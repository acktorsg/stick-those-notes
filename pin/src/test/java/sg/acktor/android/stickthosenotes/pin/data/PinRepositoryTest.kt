package sg.acktor.android.stickthosenotes.pin.data

import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItemRelation
import sg.acktor.android.stickthosenotes.pin.data.room.PinnedChecklistNote
import sg.acktor.android.stickthosenotes.pin.data.room.PinnedNotesDAO
import sg.acktor.android.stickthosenotes.pin.data.room.PinnedTextNote
import kotlin.test.Test
import kotlin.test.assertEquals

class PinRepositoryTest {
    private val dao = mockk<PinnedNotesDAO>()

    private val sut = PinRepository(dao)

    @Test
    fun `verify single note retrieved from dao`() = runTest {
        val noteId = 3L
        val expected = PinnedTextNote(
            id = noteId,
            title = "some title",
            colorPalette = ColorPalette.LIGHT_BLUE,
            content = "some content"
        )

        coEvery { dao.getPinnedNote(noteId) } returns expected

        val result = sut.getPinnedNote(noteId)
        assertEquals(expected, result)
    }

    @Test
    fun `verify note list retrieved from dao`() = runTest {
        val expected = listOf(
            PinnedTextNote(
                id = 3L,
                title = "some title",
                colorPalette = ColorPalette.LIGHT_BLUE,
                content = "some content"
            ),
            PinnedChecklistNote(
                id = 7L,
                title = "some title",
                colorPalette = ColorPalette.LIGHT_BLUE,
                content = listOf(
                    ChecklistItemRelation(
                        content = "some checklist content",
                        isChecked = true,
                    )
                )
            )
        )

        coEvery { dao.getPinnedNotes() } returns expected

        val result = sut.getAllPinnedNotes()
        assertEquals(expected, result)
    }
}
