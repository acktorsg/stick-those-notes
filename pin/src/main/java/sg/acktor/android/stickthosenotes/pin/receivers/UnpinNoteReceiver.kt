/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.pin.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import sg.acktor.android.stickthosenotes.common.KEY_NOTE_ID
import sg.acktor.android.stickthosenotes.notedetail.api.usecases.UpdatePinStateUseCase
import sg.acktor.android.stickthosenotes.pin.api.PinManager
import javax.inject.Inject

@AndroidEntryPoint
class UnpinNoteReceiver : BroadcastReceiver() {
    @Inject
    lateinit var updatePinStateUseCase: UpdatePinStateUseCase

    @Inject
    lateinit var pinManager: PinManager

    private lateinit var pendingResult: PendingResult
    private val scope = CoroutineScope(Dispatchers.Main)

    override fun onReceive(context: Context?, intent: Intent?) {
        val noteId = intent?.getIntExtra(KEY_NOTE_ID, 0) ?: 0
        if (noteId == 0) {
            return
        }

        pendingResult = goAsync()

        scope.launch {
            withContext(Dispatchers.IO) {
                updatePinStateUseCase.updatePinState(noteId.toLong(), false)
            }

            pinManager.unpinNote(noteId.toLong())
            pendingResult.finish()
        }
    }
}
