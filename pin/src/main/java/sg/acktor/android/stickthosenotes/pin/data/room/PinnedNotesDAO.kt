/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.pin.data.room

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import sg.acktor.android.stickthosenotes.common.models.ContentType

@Dao
abstract class PinnedNotesDAO {
    suspend fun getPinnedNotes(): List<PinnedNote> {
        val textNotes = getPinnedTextNotes()
        val checklistNotes = getPinnedChecklistNotes()
        return listOf(*textNotes, *checklistNotes)
    }

    @Query(
            """
            SELECT t.id, n.title, n.colorPalette, t.content 
            FROM TextNote t INNER JOIN Note n USING(id)
            WHERE isPinned = 1 AND isDeleted = 0
        """
    )
    protected abstract suspend fun getPinnedTextNotes(): Array<PinnedTextNote>

    @Transaction
    @Query(
            """
            SELECT c.id, n.title, n.colorPalette
            FROM ChecklistNote c INNER JOIN Note n USING(id)
            WHERE isPinned = 1 AND isDeleted = 0
        """
    )
    protected abstract suspend fun getPinnedChecklistNotes(): Array<PinnedChecklistNote>

    suspend fun getPinnedNote(noteId: Long) = when (getContentType(noteId)) {
        ContentType.TEXT -> getPinnedTextNote(noteId)
        ContentType.CHECKLIST -> getPinnedChecklistNote(noteId)
        else -> null
    }

    @Query(
            """
            SELECT t.id, n.title, n.colorPalette, t.content 
            FROM TextNote t INNER JOIN Note n USING(id)
            WHERE t.id = :noteId AND n.isPinned = 1
        """
    )
    protected abstract suspend fun getPinnedTextNote(noteId: Long): PinnedTextNote?

    @Transaction
    @Query(
            """
            SELECT c.id, n.title, n.colorPalette
            FROM ChecklistNote c INNER JOIN Note n USING(id)
            WHERE c.id = :noteId AND n.isPinned = 1
        """
    )
    protected abstract suspend fun getPinnedChecklistNote(noteId: Long): PinnedChecklistNote?

    private suspend fun getContentType(noteId: Long): ContentType? = when {
        isTextNote(noteId) -> ContentType.TEXT
        isChecklistNote(noteId) -> ContentType.CHECKLIST
        else -> null
    }

    @Query("SELECT COUNT(*) > 0 FROM TextNote WHERE id = :noteId")
    protected abstract suspend fun isTextNote(noteId: Long): Boolean

    @Query("SELECT COUNT(*) > 0 FROM ChecklistNote WHERE id = :noteId")
    protected abstract suspend fun isChecklistNote(noteId: Long): Boolean
}
