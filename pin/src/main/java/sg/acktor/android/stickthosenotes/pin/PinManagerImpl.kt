/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.pin

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationManagerCompat
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.Bound
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItemRelation
import sg.acktor.android.stickthosenotes.pin.api.NOTIFICATION_CHANNEL_ID_PINNED_NOTES
import sg.acktor.android.stickthosenotes.pin.api.PinManager
import sg.acktor.android.stickthosenotes.pin.data.PinRepository
import sg.acktor.android.stickthosenotes.pin.data.room.PinnedChecklistNote
import sg.acktor.android.stickthosenotes.pin.data.room.PinnedNote
import sg.acktor.android.stickthosenotes.pin.data.room.PinnedTextNote
import javax.inject.Inject
import javax.inject.Singleton

@Bound(SingletonComponent::class)
@Singleton
class PinManagerImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    private val notificationBuilder: NotificationBuilder,
    private val repo: PinRepository,
) : PinManager {
    private val notifications = mutableListOf<Int>()

    private val notificationManager get() = NotificationManagerCompat.from(context)

    override fun isSystemPinningEnabled(): Boolean {
        // Check if notifications are enabled
        if (!notificationManager.areNotificationsEnabled()) {
            return false
        }

        // Check if notification channel is enabled
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel =
                notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID_PINNED_NOTES)
            if (notificationChannel != null
                && notificationChannel.importance == NotificationManager.IMPORTANCE_NONE
            ) {
                return false
            }
        }

        return true
    }

    override suspend fun pinNote(noteId: Long) {
        val note = repo.getPinnedNote(noteId) ?: return
        performNotePin(note)
    }

    override fun unpinNote(noteId: Long) {
        val id = noteId.toInt()
        if (!notifications.contains(id)) return
        notificationManager.cancel(id)
        notifications.remove(id)
    }

    override suspend fun restorePins() {
        for (id in notifications) {
            notificationManager.cancel(id)
        }

        notifications.clear()

        val notes = repo.getAllPinnedNotes()
        for (note in notes) {
            performNotePin(note)
        }
    }

    @SuppressLint("MissingPermission")
    private fun performNotePin(note: PinnedNote) {
        val noteId = note.id.toInt()
        val (contentType, content) = when (note) {
            is PinnedTextNote -> ContentType.TEXT to note.content
            is PinnedChecklistNote -> ContentType.CHECKLIST to note.content.asString()
        }

        notificationManager.notify(
            noteId,
            notificationBuilder.constructPinnedNotification(
                context, noteId, note.title, content, contentType, note.colorPalette
            )
        )
        notifications.add(noteId)
    }
}

private fun List<ChecklistItemRelation>.asString() =
    joinToString(separator = "\n") { checklistItem ->
        "[${if (checklistItem.isChecked) "\u2713" else "   "}] ${checklistItem.content}"
    }
