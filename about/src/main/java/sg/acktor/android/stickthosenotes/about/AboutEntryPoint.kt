/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.about

import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import androidx.navigation.toRoute
import sg.acktor.aboutpage.models.AboutNavParams
import sg.acktor.aboutpage.ui.AboutScreen
import sg.acktor.android.stickthosenotes.common.LocalRootNavigator
import sg.acktor.android.stickthosenotes.common.ui.EntryPoint
import javax.inject.Inject

private const val ANIM_DURATION_MILLIS = 250

internal class AboutEntryPoint @Inject constructor() : EntryPoint {
    override fun createDestination(navGraphBuilder: NavGraphBuilder) {
        navGraphBuilder.composable<AboutNavParams>(
            enterTransition = {
                fadeIn() + slideInHorizontally(
                    animationSpec = tween(
                        durationMillis = ANIM_DURATION_MILLIS
                    ),
                    initialOffsetX = { it / 2 }
                )
            },
            exitTransition = {
                fadeOut() + slideOutHorizontally(
                    animationSpec = tween(
                        durationMillis = ANIM_DURATION_MILLIS
                    ),
                    targetOffsetX = { it / 2 }
                )
            }
        ) { backStackEntry ->
            val rootNavigator = LocalRootNavigator.current
            val navParams = backStackEntry.toRoute<AboutNavParams>()

            AboutScreen(
                navParams = navParams,
                returnToPreviousScreen = rootNavigator::returnToHomeScreen
            )
        }
    }
}
