/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.db

import androidx.room.Database
import androidx.room.RoomDatabase
import sg.acktor.android.stickthosenotes.createnote.data.room.CreateNoteDAO
import sg.acktor.android.stickthosenotes.database.api.room.DATABASE_VERSION
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItem
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistNote
import sg.acktor.android.stickthosenotes.database.api.room.entities.Note
import sg.acktor.android.stickthosenotes.database.api.room.entities.TextNote
import sg.acktor.android.stickthosenotes.home.data.room.RecycleBinDAO
import sg.acktor.android.stickthosenotes.notedetail.data.room.NoteDetailDAO
import sg.acktor.android.stickthosenotes.notelist.data.room.NoteListDAO
import sg.acktor.android.stickthosenotes.pin.data.room.PinnedNotesDAO
import sg.acktor.android.stickthosenotes.settings.backup.data.room.BackupNotesDAO

@Database(
    version = DATABASE_VERSION,
    entities = [Note::class, TextNote::class, ChecklistNote::class, ChecklistItem::class]
)
abstract class StickThoseNotesDatabase : RoomDatabase() {
    abstract fun noteListDao(): NoteListDAO

    abstract fun recycleBinDao(): RecycleBinDAO

    abstract fun noteCreateDao(): CreateNoteDAO

    abstract fun noteDetailDao(): NoteDetailDAO

    abstract fun backupNotesDao(): BackupNotesDAO

    abstract fun pinnedNotesDao(): PinnedNotesDAO
}
