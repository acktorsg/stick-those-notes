/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.SharedPreferences
import android.os.Build
import androidx.core.app.NotificationManagerCompat
import dagger.hilt.android.HiltAndroidApp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import sg.acktor.android.stickthosenotes.pin.api.NOTIFICATION_CHANNEL_ID_PINNED_NOTES
import sg.acktor.android.stickthosenotes.pin.api.PinManager
import javax.inject.Inject

@HiltAndroidApp
class StickThoseNotesApplication : Application() {
    @Inject
    lateinit var pinManager: PinManager

    @Inject
    lateinit var prefs: SharedPreferences

    private val scope = CoroutineScope(Dispatchers.IO)

    override fun onCreate() {
        super.onCreate()

        // Create notification channel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = NotificationManagerCompat.from(this)
            if (notificationManager.notificationChannels.none { it.id == NOTIFICATION_CHANNEL_ID_PINNED_NOTES }) {
                val notificationChannel = NotificationChannel(
                    NOTIFICATION_CHANNEL_ID_PINNED_NOTES,
                    getString(R.string.title_notificationChannel_pinnedNotes),
                    NotificationManager.IMPORTANCE_HIGH
                )

                notificationChannel.setShowBadge(false)
                notificationChannel.setSound(null, null)

                notificationManager.createNotificationChannel(notificationChannel)
            }
        }

        // Pin notes if they are removed when app is forced to stop
        scope.launch {
            try {
                pinManager.restorePins()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}
