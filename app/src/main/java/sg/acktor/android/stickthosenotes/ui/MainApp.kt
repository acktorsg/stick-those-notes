/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.ui

import androidx.compose.material.navigation.ModalBottomSheetLayout
import androidx.compose.material.navigation.rememberBottomSheetNavigator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navOptions
import sg.acktor.aboutpage.models.AboutNavParams
import sg.acktor.android.stickthosenotes.R
import sg.acktor.android.stickthosenotes.UiViewModel
import sg.acktor.android.stickthosenotes.common.LocalRootNavigator
import sg.acktor.android.stickthosenotes.common.RootNavigator
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.common.ui.EntryPoint
import sg.acktor.android.stickthosenotes.createnote.ui.CreateNoteDestination
import sg.acktor.android.stickthosenotes.home.ui.HomeDestination
import sg.acktor.android.stickthosenotes.models.IntentEvent
import sg.acktor.android.stickthosenotes.notedetail.ui.NoteDetailDestination
import sg.acktor.android.stickthosenotes.search.ui.SearchDestination
import sg.acktor.android.stickthosenotes.settings.ui.SettingsDestination

@Composable
internal fun MainApp(
    uiViewModel: UiViewModel,
    entryPoints: Set<EntryPoint>,
) {
    val bottomSheetNavigator = rememberBottomSheetNavigator()
    val navController = rememberNavController(bottomSheetNavigator)

    val rootNavigator = remember(navController) {
        object : RootNavigator {
            override fun showNoteDetailsScreen(
                noteId: Long,
                contentType: ContentType,
                shouldReturnToHome: Boolean
            ) {
                navController.navigate(
                    NoteDetailDestination(noteId, contentType),
                    navOptions {
                        popUpTo(HomeDestination)
                    }.takeIf { shouldReturnToHome }
                )
            }

            override fun showCreateNoteScreen() {
                navController.navigate(CreateNoteDestination)
            }

            override fun showSearchScreen() {
                navController.navigate(SearchDestination)
            }

            override fun showSettingsScreen() {
                navController.navigate(SettingsDestination)
            }

            override fun showAboutScreen() {
                navController.navigate(AboutNavParams(R.raw.licenses))
            }

            override fun returnToHomeScreen() {
                navController.popBackStack()
            }
        }
    }

    LaunchedEffect(uiViewModel, rootNavigator) {
        uiViewModel.intentEvents.collect { event ->
            when (event) {
                IntentEvent.None -> {}
                IntentEvent.NewNote -> {
                    rootNavigator.showCreateNoteScreen()
                }

                is IntentEvent.OpenNote -> {
                    rootNavigator.showNoteDetailsScreen(event.noteId, event.contentType)
                }

                IntentEvent.Search -> {
                    rootNavigator.showSearchScreen()
                }
            }
        }
    }

    CompositionLocalProvider(
        LocalRootNavigator provides rootNavigator,
    ) {
        ModalBottomSheetLayout(
            bottomSheetNavigator,
            // Let bottom sheet destinations draw their own scrim
            scrimColor = Color.Transparent,
        ) {
            NavHost(navController, startDestination = HomeDestination::class) {
                for (entryPoint in entryPoints) {
                    entryPoint.createDestination(this)
                }
            }
        }
    }
}
