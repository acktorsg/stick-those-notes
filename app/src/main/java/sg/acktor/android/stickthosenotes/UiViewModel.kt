/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import sg.acktor.android.stickthosenotes.models.IntentEvent
import javax.inject.Inject

@HiltViewModel
class UiViewModel @Inject constructor() : ViewModel() {
    private val _intentEvents = MutableStateFlow<IntentEvent>(IntentEvent.None)
    val intentEvents get() = _intentEvents.asStateFlow()

    fun triggerIntentEvent(event: IntentEvent) {
        _intentEvents.value = event
    }
}
