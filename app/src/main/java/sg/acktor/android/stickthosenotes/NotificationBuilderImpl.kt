/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.os.Build
import androidx.core.app.NotificationCompat
import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.Bound
import sg.acktor.android.stickthosenotes.common.INTENT_ACTION_OPEN_NOTE
import sg.acktor.android.stickthosenotes.common.KEY_CONTENT_TYPE
import sg.acktor.android.stickthosenotes.common.KEY_NOTE_ID
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.common.models.LightNoteThemes
import sg.acktor.android.stickthosenotes.pin.ID_UNDO_DELETE_OFFSET
import sg.acktor.android.stickthosenotes.pin.KEY_TITLE
import sg.acktor.android.stickthosenotes.pin.NotificationBuilder
import sg.acktor.android.stickthosenotes.pin.api.NOTIFICATION_CHANNEL_ID_PINNED_NOTES
import sg.acktor.android.stickthosenotes.pin.receivers.DeleteNoteReceiver
import sg.acktor.android.stickthosenotes.pin.receivers.UndoDeleteReceiver
import sg.acktor.android.stickthosenotes.pin.receivers.UnpinNoteReceiver
import sg.acktor.android.stickthosenotes.ui.MainActivity
import javax.inject.Inject
import sg.acktor.android.stickthosenotes.common.R as R_common
import sg.acktor.android.stickthosenotes.pin.api.R as R_pin_api

@Bound(SingletonComponent::class)
class NotificationBuilderImpl @Inject constructor(
    private val noteThemes: LightNoteThemes,
) : NotificationBuilder {
    private val pendingIntentFlags by lazy {
        PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
    }

    override fun constructPinnedNotification(
        context: Context,
        noteId: Int,
        title: String,
        content: String,
        contentType: ContentType,
        colorPalette: ColorPalette
    ): Notification {
        val theme = noteThemes[colorPalette]
        val unpinIntent = PendingIntent.getBroadcast(
            context, noteId,
            Intent(context, UnpinNoteReceiver::class.java)
                .putExtra(KEY_NOTE_ID, noteId),
            pendingIntentFlags
        )


        val notificationBuilder =
            NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID_PINNED_NOTES)
                .setSmallIcon(sg.acktor.android.stickthosenotes.pin.R.drawable.ic_notification)
                .setContentTitle(title)
                .setContentText(content)
                .setTicker(title)
                .setColor(theme.darkBgArgb)
                .setOngoing(true)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setSound(null, AudioManager.STREAM_NOTIFICATION)
                .setStyle(
                    NotificationCompat.BigTextStyle()
                        .setBigContentTitle(title)
                        .bigText(content)
                )
                .setContentIntent(
                    PendingIntent.getActivity(
                        context, noteId,
                        Intent(context.applicationContext, MainActivity::class.java)
                            .setAction(INTENT_ACTION_OPEN_NOTE)
                            .putExtra(KEY_NOTE_ID, noteId)
                            .putExtra(KEY_CONTENT_TYPE, contentType.name),
                        pendingIntentFlags
                    )
                )
                .addAction(
                    R_pin_api.drawable.ic_unpin,
                    context.getString(sg.acktor.android.stickthosenotes.pin.R.string.title_button_unpin),
                    unpinIntent
                )
                .addAction(
                    R_common.drawable.ic_delete,
                    context.getString(sg.acktor.android.stickthosenotes.pin.R.string.title_button_delete),
                    PendingIntent.getBroadcast(
                        context, noteId,
                        Intent(context, DeleteNoteReceiver::class.java)
                            .putExtra(KEY_NOTE_ID, noteId)
                            .putExtra(KEY_TITLE, title),
                        pendingIntentFlags
                    )
                )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            notificationBuilder.setDeleteIntent(unpinIntent)
        }

        return notificationBuilder.build()
    }

    override fun constructDeleteNotification(
        context: Context, noteId: Int, noteTitle: String
    ): Notification {
        return NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID_PINNED_NOTES)
            .setSmallIcon(sg.acktor.android.stickthosenotes.pin.R.drawable.ic_notification)
            .setContentTitle(context.getString(R_common.string.message_success_deleteNote))
            .setContentText(
                context.getString(
                    sg.acktor.android.stickthosenotes.pin.R.string.template_notification_deletedNoteDetails,
                    noteTitle
                )
            )
            .setTicker(context.getString(R_common.string.message_success_deleteNote))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setSound(null, AudioManager.STREAM_NOTIFICATION)
            .addAction(
                sg.acktor.android.stickthosenotes.pin.R.drawable.ic_undo,
                context.getString(sg.acktor.android.stickthosenotes.pin.R.string.title_button_undoDelete),
                PendingIntent.getBroadcast(
                    context, noteId + ID_UNDO_DELETE_OFFSET,
                    Intent(context, UndoDeleteReceiver::class.java)
                        .putExtra(KEY_NOTE_ID, noteId),
                    pendingIntentFlags
                )
            )
            .build()
    }
}
