/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.ui

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.activity.SystemBarStyle
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.StateFlow
import sg.acktor.android.stickthosenotes.UiViewModel
import sg.acktor.android.stickthosenotes.common.INTENT_ACTION_OPEN_NOTE
import sg.acktor.android.stickthosenotes.common.KEY_CONTENT_TYPE
import sg.acktor.android.stickthosenotes.common.KEY_NOTE_ID
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.common.ui.EntryPoint
import sg.acktor.android.stickthosenotes.models.IntentEvent
import sg.acktor.android.stickthosenotes.settings.customization.domain.ThemeProvider
import sg.acktor.android.stickthosenotes.settings.models.AppThemeMode
import javax.inject.Inject

private const val INTENT_ACTION_NEW_NOTE = "action_new_note"
private const val INTENT_ACTION_SEARCH_NOTES = "action_search_notes"

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val uiViewModel by viewModels<UiViewModel>()
    private val newIntentListener = this::processIntent

    @Inject
    lateinit var entryPoints: Set<@JvmSuppressWildcards EntryPoint>

    @Inject
    lateinit var themeProvider: ThemeProvider

    private lateinit var themeModeState: StateFlow<AppThemeMode>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        themeModeState = themeProvider.themeState

        setContent {
            val themeMode by themeModeState.collectAsStateWithLifecycle()
            val isDarkTheme = remember(themeMode) {
                when (themeMode) {
                    AppThemeMode.LIGHT -> false
                    AppThemeMode.DARK -> true
                    AppThemeMode.SYSTEM -> null
                }
            } ?: isSystemInDarkTheme()

            LaunchedEffect(isDarkTheme) {
                enableEdgeToEdge(
                    statusBarStyle = if (isDarkTheme) {
                        SystemBarStyle.dark(Color.TRANSPARENT)
                    } else {
                        SystemBarStyle.light(
                            scrim = Color.TRANSPARENT,
                            darkScrim = Color.TRANSPARENT,
                        )
                    },
                    navigationBarStyle = if (isDarkTheme) {
                        SystemBarStyle.dark(Color.argb(0x80, 0x1b, 0x1b, 0x1b))
                    } else {
                        SystemBarStyle.light(
                            scrim = Color.argb(0xe6, 0xFF, 0xFF, 0xFF),
                            darkScrim = Color.argb(0x80, 0x1b, 0x1b, 0x1b),
                        )
                    },
                )
            }

            StickThoseNotesTheme(isDarkTheme = isDarkTheme) {
                MainApp(uiViewModel, entryPoints)
            }
        }

        addOnNewIntentListener(newIntentListener)
        processIntent(intent)
    }

    override fun onDestroy() {
        removeOnNewIntentListener(newIntentListener)
        super.onDestroy()
    }

    private fun processIntent(intent: Intent?) {
        when (intent?.action) {
            INTENT_ACTION_NEW_NOTE -> uiViewModel.triggerIntentEvent(IntentEvent.NewNote)
            INTENT_ACTION_SEARCH_NOTES -> uiViewModel.triggerIntentEvent(IntentEvent.Search)
            INTENT_ACTION_OPEN_NOTE -> {
                val noteId = intent.getIntExtra(KEY_NOTE_ID, 0)
                if (noteId == 0) return

                val contentType = intent.getStringExtra(KEY_CONTENT_TYPE)
                if (contentType == null
                    || contentType !in ContentType.entries.map(ContentType::name)
                ) {
                    return
                }

                uiViewModel.triggerIntentEvent(
                    IntentEvent.OpenNote(noteId.toLong(), ContentType.valueOf(contentType))
                )
            }
        }
    }
}
