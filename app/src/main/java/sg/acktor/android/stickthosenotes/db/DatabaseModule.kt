/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.db

import android.content.Context
import androidx.room.Room
import androidx.room.migration.Migration
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import sg.acktor.android.stickthosenotes.createnote.data.room.CreateNoteDAO
import sg.acktor.android.stickthosenotes.home.data.room.RecycleBinDAO
import sg.acktor.android.stickthosenotes.notedetail.data.room.NoteDetailDAO
import sg.acktor.android.stickthosenotes.notelist.data.room.NoteListDAO
import sg.acktor.android.stickthosenotes.pin.data.room.PinnedNotesDAO
import sg.acktor.android.stickthosenotes.settings.backup.data.room.BackupNotesDAO
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    private const val DATABASE_NAME = "stick_those_notes.db"

    @Provides
    @Singleton
    fun provideDatabase(
        @ApplicationContext context: Context,
        migrations: Set<@JvmSuppressWildcards Migration>,
    ) = Room.databaseBuilder(context, StickThoseNotesDatabase::class.java, DATABASE_NAME)
        .addMigrations(*migrations.toTypedArray())
        .build()

    @Provides
    fun provideCreateNoteDao(db: StickThoseNotesDatabase): CreateNoteDAO = db.noteCreateDao()

    @Provides
    fun provideNoteListDao(db: StickThoseNotesDatabase): NoteListDAO = db.noteListDao()

    @Provides
    fun provideRecycleBinDao(db: StickThoseNotesDatabase): RecycleBinDAO = db.recycleBinDao()

    @Provides
    fun provideNoteViewDao(db: StickThoseNotesDatabase): NoteDetailDAO = db.noteDetailDao()

    @Provides
    fun providePinnedNotesDao(db: StickThoseNotesDatabase): PinnedNotesDAO = db.pinnedNotesDao()

    @Provides
    fun provideBackupNotesDao(db: StickThoseNotesDatabase): BackupNotesDAO = db.backupNotesDao()
}
