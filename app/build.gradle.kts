/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

import java.util.Properties

plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.hilt)
    alias(libs.plugins.ksp)
    alias(libs.plugins.composeCompiler)
}

val versionMajor = 3
val versionMinor = 4
val versionPatch = 2

val keystoreProperties = Properties()
file("../keystore.properties").inputStream().use { keystoreProperties.load(it) }

android {
    namespace = "sg.acktor.android.stickthosenotes"

    compileSdk = libs.versions.sdk.max.get().toInt()
    buildToolsVersion = libs.versions.buildTools.get()

    defaultConfig {
        applicationId = "sg.acktor.android.stickthosenotes"

        minSdk = libs.versions.sdk.min.get().toInt()
        targetSdk = libs.versions.sdk.max.get().toInt()

        versionCode = versionMajor * 1000000 + versionMinor * 1000 + versionPatch
        versionName = "$versionMajor.$versionMinor.$versionPatch"
    }

    signingConfigs {
        create("release") {
            keyAlias = "stick-those-notes"
            keyPassword = keystoreProperties.getProperty("keyPassword")
            storeFile = file("../keystore.jks")
            storePassword = keystoreProperties.getProperty("storePassword")
        }
    }

    buildTypes {
        debug {
            isMinifyEnabled = false

            versionNameSuffix = "-debug"
            applicationIdSuffix = ".debug"
        }

        release {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            signingConfig = signingConfigs.getByName("release")
        }
    }

    kotlin {
        jvmToolchain(libs.versions.jvm.get().toInt())
    }

    buildFeatures {
        compose = true
    }

    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }

    compileOptions {
        isCoreLibraryDesugaringEnabled = true
    }

    ksp {
        arg("room.schemaLocation", "$projectDir/schemas")
    }
}

dependencies {
    coreLibraryDesugaring(libs.desugar)

    // Modules
    api(project(":common"))
    api(project(":database"))
    api(project(":settings"))
    api(project(":pin"))
    api(project(":create-note"))
    api(project(":note-list"))
    api(project(":search"))
    api(project(":note-detail"))
    api(project(":home"))
    api(project(":about"))

    // About Page library
    implementation(libs.acktorsdk)
    implementation(libs.aboutPage)

    // Android support libraries
    implementation(libs.appcompat)
    implementation(libs.activity.compose)
    implementation(libs.activity.main)
    implementation(libs.kotlin.serialization.json)
    implementation(libs.bundles.lifecycle.ktx)
    implementation(libs.bundles.navigation)
    implementation(libs.preference)
    implementation(libs.bundles.room)
    ksp(libs.compiler.room)

    // Jetpack Compose
    implementation(platform(libs.compose.bom))
    implementation(libs.bundles.compose)

    // Hilt
    implementation(libs.hilt.android)
    ksp(libs.compiler.hilt.android)
    implementation(libs.hilt.extensions)
    ksp(libs.compiler.hilt.extensions)
}
