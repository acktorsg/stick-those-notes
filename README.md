# Stick Those Notes!

A note-taking app that pins your notes to the notification area

[Play Store link](https://play.google.com/store/apps/details?id=sg.acktor.android.stickthosenotes)

## Phone screenshots
![Phone Screenshot 1](art/screenshot_phone_repo_1.png)
![Phone Screenshot 2](art/screenshot_phone_repo_2.png)
![Phone Screenshot 3](art/screenshot_phone_repo_3.png)
![Phone Screenshot 4](art/screenshot_phone_repo_4.png)
![Phone Screenshot 5](art/screenshot_phone_repo_5.png)
![Phone Screenshot 6](art/screenshot_phone_repo_6.png)
![Phone Screenshot 7](art/screenshot_phone_repo_7.png)
![Phone Screenshot 8](art/screenshot_phone_repo_8.png)

## 7-inch tablet screenshots
![7 Inch Tablet Screenshot 1](art/screenshot_small-tablet_repo_1.png)
![7 Inch Tablet Screenshot 2](art/screenshot_small-tablet_repo_2.png)
![7 Inch Tablet Screenshot 3](art/screenshot_small-tablet_repo_3.png)
![7 Inch Tablet Screenshot 4](art/screenshot_small-tablet_repo_4.png)
![7 Inch Tablet Screenshot 5](art/screenshot_small-tablet_repo_5.png)
![7 Inch Tablet Screenshot 6](art/screenshot_small-tablet_repo_6.png)
![7 Inch Tablet Screenshot 7](art/screenshot_small-tablet_repo_7.png)
![7 Inch Tablet Screenshot 8](art/screenshot_small-tablet_repo_8.png)

## 10-inch tablet screenshots
![10 Inch Tablet Screenshot 1](art/screenshot_large-tablet_repo_1.png)
![10 Inch Tablet Screenshot 2](art/screenshot_large-tablet_repo_2.png)
![10 Inch Tablet Screenshot 3](art/screenshot_large-tablet_repo_3.png)
![10 Inch Tablet Screenshot 4](art/screenshot_large-tablet_repo_4.png)
![10 Inch Tablet Screenshot 5](art/screenshot_large-tablet_repo_5.png)
![10 Inch Tablet Screenshot 6](art/screenshot_large-tablet_repo_6.png)
![10 Inch Tablet Screenshot 7](art/screenshot_large-tablet_repo_7.png)
![10 Inch Tablet Screenshot 8](art/screenshot_large-tablet_repo_8.png)

# License

~~~~
Copyright 2019-2024 Khairuddin Bin Ali

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
~~~~
