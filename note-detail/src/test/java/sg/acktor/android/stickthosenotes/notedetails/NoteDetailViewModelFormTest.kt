package sg.acktor.android.stickthosenotes.notedetails

import android.os.Build
import app.cash.turbine.test
import io.mockk.Ordering
import io.mockk.coVerify
import io.mockk.every
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.home.api.HomeMessage
import sg.acktor.android.stickthosenotes.home.api.HomeMessageAction
import sg.acktor.android.stickthosenotes.notedetail.R
import sg.acktor.android.stickthosenotes.notedetail.ui.MessageType
import sg.acktor.android.stickthosenotes.notedetail.ui.NoteDetailUiAction
import sg.acktor.android.stickthosenotes.notedetail.ui.NoteDetailUiEvent
import sg.acktor.android.stickthosenotes.notedetail.ui.NoteFormUpdate
import sg.acktor.android.stickthosenotes.notedetail.ui.UserMessage
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteContentUiModel
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteStateUpdate
import sg.acktor.android.stickthosenotes.notedetails.utils.mockEmptyChecklistContent
import sg.acktor.android.stickthosenotes.notedetails.utils.mockNoteDetail
import sg.acktor.android.stickthosenotes.notedetails.utils.mockTextContent
import kotlin.test.Test
import kotlin.test.assertEquals
import sg.acktor.android.stickthosenotes.common.R as R_common

// TODO: Figure out how to verify repo updates
@OptIn(ExperimentalCoroutinesApi::class)
internal class NoteDetailViewModelFormTest : BaseNoteDetailViewModelTest() {
    @Test
    fun `verify title update`() = runTest {
        val expected = "new title"

        sut.uiState.test {
            initNoteDetails()
            skipItems(2)

            sut.processUiAction(NoteFormUpdate.Title(expected))

            val cardUiState = awaitItem()?.cardUiState
            assertEquals(expected, cardUiState?.title)
        }
    }

    @Test
    fun `verify colour palette update`() = runTest {
        val expected = ColorPalette.DEEP_PURPLE

        sut.uiState.test {
            initNoteDetails()
            skipItems(2)

            sut.processUiAction(NoteFormUpdate.Color(expected))

            val uiState = awaitItem()
            assertEquals(expected, uiState?.colorPalette)
        }
    }

    @Test
    fun `verify show error message when title and text content are blank`() = runTest {
        val message = "You are not allowed to pin an empty note"
        val expected = UserMessage(MessageType.ERROR, message)

        every { context.getString(R.string.message_error_pinEmptyNote) } returns message

        sut.uiState.test {
            initNoteDetails(
                details = mockNoteDetail(title = ""),
                content = mockTextContent(content = "")
            )
            skipItems(2)

            sut.processUiAction(NoteFormUpdate.Pin(true))

            val state = awaitItem()
            assertEquals(expected, state?.userMessage)
        }
    }

    @Test
    fun `verify show error message when title and checklist content are blank`() = runTest {
        val message = "You are not allowed to pin an empty note"
        val expected = UserMessage(MessageType.ERROR, message)

        every { context.getString(R.string.message_error_pinEmptyNote) } returns message

        sut.uiState.test {
            initNoteDetails(
                details = mockNoteDetail(title = ""),
                content = mockEmptyChecklistContent()
            )
            skipItems(2)

            sut.processUiAction(NoteFormUpdate.Pin(true))

            val state = awaitItem()
            assertEquals(expected, state?.userMessage)
        }
    }

    @Test
    fun `verify set pin to true for API 32 & below`() = runTest {
        val expected = true

        sut.uiState.test {
            initNoteDetails()
            skipItems(2)

            sut.processUiAction(NoteFormUpdate.Pin(true))

            val result = awaitItem()?.topBarUiState?.isPinEnabled
            assertEquals(expected, result)
        }
    }

    @Test
    fun `verify set pin to true for API 33 & above`() = runTest {
        val expected = true

        every { buildDetailProvider.getDeviceApiVersion() } returns Build.VERSION_CODES.TIRAMISU

        sut.uiState.test {
            initNoteDetails()
            skipItems(2)

            sut.processUiAction(NoteDetailUiAction.NotificationPermissionRequested(true))

            val result = awaitItem()?.topBarUiState?.isPinEnabled
            assertEquals(expected, result)
        }
    }

    @Test
    fun `verify set pin to false`() = runTest {
        val expected = false

        sut.uiState.test {
            initNoteDetails(mockNoteDetail(isPinned = true))
            skipItems(2)

            sut.processUiAction(NoteFormUpdate.Pin(false))

            val result = awaitItem()?.topBarUiState?.isPinChecked
            assertEquals(expected, result)
        }
    }

    @Test
    fun `verify mark note as deleted`() = runTest {
        val message = "Note moved to Recycle Bin"
        val actionLabel = "Undo"
        val expected = NoteDetailUiEvent.ReturnToPreviousScreen

        every { context.getString(R_common.string.message_success_deleteNote) } returns message
        every { context.getString(R.string.title_button_undo) } returns actionLabel

        sut.uiState.test {
            initNoteDetails(mockNoteDetail())
            skipItems(2)

            sut.processUiAction(NoteDetailUiAction.ClickDeleteButton)
            advanceUntilIdle()

            val result = sut.uiEventChannel.first()
            assertEquals(expected, result)

            coVerify(Ordering.ORDERED) {
                deleteNoteUseCase.setDeleteState(DEFAULT_NOTE_ID, true)
                pinManager.unpinNote(DEFAULT_NOTE_ID)

                homeMessageSender.sendMessage(
                    HomeMessage(
                        message, actionLabel, HomeMessageAction.RestoreNote(DEFAULT_NOTE_ID)
                    )
                )
            }
        }
    }

    @Test
    fun `verify text content update`() = runTest {
        val expected = NoteContentUiModel.Text("new content")

        sut.uiState.test {
            initNoteDetails()
            skipItems(2)

            sut.processUiAction(NoteFormUpdate.TextContent(expected.content))

            val result = awaitItem()?.cardUiState?.content
            assertEquals(expected, result)
        }
    }

    @Test
    fun `verify last updated state update received`() = runTest {
        val expected = "2"

        val noteUpdateChannel = Channel<NoteStateUpdate>()

        every { getNoteUseCase.getNoteStateUpdates(DEFAULT_NOTE_ID) } returns noteUpdateChannel.receiveAsFlow()

        sut.uiState.test {
            initNoteDetails()
            skipItems(2)

            noteUpdateChannel.send(NoteStateUpdate(expected, false, false))

            val footerUiState = awaitItem()?.footerUiState
            assertEquals(expected, footerUiState?.lastUpdated)
        }
    }

    @Test
    fun `verify delete state update received`() = runTest {
        val expected = NoteDetailUiEvent.ReturnToPreviousScreen

        val noteUpdateChannel = Channel<NoteStateUpdate>()

        every { getNoteUseCase.getNoteStateUpdates(DEFAULT_NOTE_ID) } returns noteUpdateChannel.receiveAsFlow()

        sut.uiState.test {
            initNoteDetails()
            skipItems(2)

            noteUpdateChannel.send(NoteStateUpdate("", false, true))

            val result = sut.uiEventChannel.first()
            assertEquals(expected, result)
        }
    }
}
