/*
 * Copyright (c) 2023. Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetails.mappers

import sg.acktor.android.stickthosenotes.notedetail.mappers.LastUpdatedTextMapper
import sg.acktor.android.stickthosenotes.notedetails.DEFAULT_CLOCK
import sg.acktor.android.stickthosenotes.notedetails.DEFAULT_TIME_MILLIS
import java.util.concurrent.TimeUnit
import kotlin.test.Test
import kotlin.test.assertEquals

class LastUpdatedTextMapperTest {
    private val mapper = LastUpdatedTextMapper(DEFAULT_CLOCK)

    @Test
    fun `1 second`() {
        val lastUpdated = 1L
        verifyMapping(
            expectedText = "$lastUpdated second ago",
            durationToSubtract = lastUpdated,
            timeUnit = TimeUnit.SECONDS,
        )
    }

    @Test
    fun `between 2 seconds and 59 seconds`() {
        val lastUpdated = 12L
        verifyMapping(
            expectedText = "$lastUpdated seconds ago",
            durationToSubtract = lastUpdated,
            timeUnit = TimeUnit.SECONDS,
        )
    }

    @Test
    fun `1 minute`() {
        val lastUpdated = 1L
        verifyMapping(
            expectedText = "$lastUpdated minute ago",
            durationToSubtract = lastUpdated,
            timeUnit = TimeUnit.MINUTES,
        )
    }

    @Test
    fun `between 2 minutes and 59 minutes`() {
        val lastUpdated = 26L
        verifyMapping(
            expectedText = "$lastUpdated minutes ago",
            durationToSubtract = lastUpdated,
            timeUnit = TimeUnit.MINUTES,
        )
    }

    @Test
    fun `beyond 1 hour`() {
        verifyMapping(
            expectedText = "on 11 September at 7:04 am",
            durationToSubtract = 2L,
            timeUnit = TimeUnit.HOURS,
        )
    }

    private fun verifyMapping(
        expectedText: String,
        durationToSubtract: Long,
        timeUnit: TimeUnit,
    ) {
        val time = DEFAULT_TIME_MILLIS - timeUnit.toMillis(durationToSubtract)
        val result = mapper.map(time)
        assertEquals(expectedText, result)
    }
}
