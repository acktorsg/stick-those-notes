package sg.acktor.android.stickthosenotes.notedetails

import app.cash.turbine.test
import io.mockk.coEvery
import io.mockk.coVerify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.notedetail.ui.ChecklistFormUpdate
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteContentUiModel
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs

private const val DEFAULT_ITEM_ID = 1L

@OptIn(ExperimentalCoroutinesApi::class)
internal class NoteDetailViewModelChecklistTest : BaseNoteDetailViewModelTest() {
    @Test
    fun `verify checklist content update`() = runTest {
        val expected = "new content"

        sut.uiState.test {
            initNoteDetails(content = mockPopulatedChecklistContent())
            skipItems(1)

            sut.processUiAction(ChecklistFormUpdate.Content(DEFAULT_ITEM_ID, 0, expected))

            val content = awaitItem()?.cardUiState?.content
            assertIs<NoteContentUiModel.Checklist>(content)
            assert(content.items.isNotEmpty())
            assertEquals(expected, content.items[0].content)
        }
    }

    @Test
    fun `verify checklist checked state update`() = runTest {
        val expected = true

        sut.uiState.test {
            initNoteDetails(content = mockPopulatedChecklistContent())
            skipItems(1)

            sut.processUiAction(ChecklistFormUpdate.CheckState(DEFAULT_ITEM_ID, 0, expected))

            val content = awaitItem()?.cardUiState?.content
            assertIs<NoteContentUiModel.Checklist>(content)
            assert(content.items.isNotEmpty())
            assertEquals(expected, content.items[0].isChecked)
        }

        coVerify {
            updateChecklistUseCase.updateCheckedState(DEFAULT_NOTE_ID, DEFAULT_ITEM_ID, expected)
        }
    }

    @Test
    fun `verify delete checklist item update`() = runTest {
        val expected = 2

        sut.uiState.test {
            initNoteDetails(content = mockPopulatedChecklistContent())
            skipItems(2)

            sut.processUiAction(ChecklistFormUpdate.Delete(DEFAULT_ITEM_ID, 0))

            val content = awaitItem()?.cardUiState?.content
            assertIs<NoteContentUiModel.Checklist>(content)
            assertEquals(expected, content.items.size)
        }

        coVerify {
            updateChecklistUseCase.deleteChecklistItem(DEFAULT_NOTE_ID, DEFAULT_ITEM_ID)
        }
    }

    @Test
    fun `verify new checklist item update`() = runTest {
        val expectedCount = 4
        val expectedId = 4L

        coEvery { updateChecklistUseCase.addChecklistItem(DEFAULT_NOTE_ID) } returns expectedId

        sut.uiState.test {
            initNoteDetails(content = mockPopulatedChecklistContent())
            skipItems(2)

            sut.processUiAction(ChecklistFormUpdate.NewItem)

            val content = awaitItem()?.cardUiState?.content
            assertIs<NoteContentUiModel.Checklist>(content)
            assertEquals(expectedCount, content.items.size)
            assertEquals(expectedId, content.items.last().id)
        }

        coVerify {
            updateChecklistUseCase.addChecklistItem(DEFAULT_NOTE_ID)
        }
    }

    private fun mockPopulatedChecklistContent() = NoteContentUiModel.Checklist(
        listOf(
            NoteContentUiModel.Checklist.Item(
                id = DEFAULT_ITEM_ID,
                content = "First item",
                isChecked = false
            ),
            NoteContentUiModel.Checklist.Item(
                id = 2,
                content = "Second item",
                isChecked = true
            ),
            NoteContentUiModel.Checklist.Item(
                id = 3,
                content = "Third item",
                isChecked = false
            ),
        )
    )
}
