package sg.acktor.android.stickthosenotes.notedetails

import android.os.Build
import app.cash.turbine.test
import io.mockk.Ordering
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.home.api.HomeMessage
import sg.acktor.android.stickthosenotes.notedetail.R
import sg.acktor.android.stickthosenotes.notedetail.models.ui.NoteScreenUiModel
import sg.acktor.android.stickthosenotes.notedetail.ui.MessageType
import sg.acktor.android.stickthosenotes.notedetail.ui.NoteDetailUiAction
import sg.acktor.android.stickthosenotes.notedetail.ui.NoteDetailUiEvent
import sg.acktor.android.stickthosenotes.notedetail.ui.NoteFormUpdate
import sg.acktor.android.stickthosenotes.notedetail.ui.UserMessage
import sg.acktor.android.stickthosenotes.notedetails.utils.mockEmptyChecklistContent
import sg.acktor.android.stickthosenotes.notedetails.utils.mockNoteDetail
import java.io.IOException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

@OptIn(ExperimentalCoroutinesApi::class)
internal class NoteDetailViewModelTest : BaseNoteDetailViewModelTest() {
    @Test
    fun `verify message shown and screen closed if cannot fetch details`() = runTest {
        val expected = NoteDetailUiEvent.ReturnToPreviousScreen
        val errorMessage = "Failed to load note details"
        every { context.getString(R.string.message_error_loadNoteDetails) } returns errorMessage

        initNoteDetails(null, null)
        val result = sut.uiEventChannel.first()
        assertEquals(expected, result)

        verify {
            homeMessageSender.sendMessage(HomeMessage(errorMessage))
        }
    }

    @Test
    fun `verify note successfully loads`() = runTest {
        val noteDetails = mockNoteDetail()
        val noteContent = mockEmptyChecklistContent()
        val expected = noteUiStateMapper.map(noteDetails, noteContent, NoteScreenUiModel())

        sut.uiState.test {
            skipItems(1)
            initNoteDetails(noteDetails, noteContent)

            val result = awaitItem()
            assertEquals(expected, result)
        }
    }

    @Test
    fun `verify close note`() = runTest {
        val expected = NoteDetailUiEvent.ReturnToPreviousScreen
        initNoteDetails()
        sut.processUiAction(NoteDetailUiAction.CloseNote)
        val result = sut.uiEventChannel.first()
        assertEquals(expected, result)
    }

    @Test
    fun `verify confirmation shown when permanently deleting note`() = runTest {
        val noteDetails = mockNoteDetail(isDeleted = true)

        sut.uiState.test {
            initNoteDetails(noteDetails)
            skipItems(2)

            sut.processUiAction(NoteDetailUiAction.ClickDeleteButton)

            val uiState = awaitItem()
            assertEquals(true, uiState?.showPermanentDeleteConfirmation)

            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun `verify dismiss delete confirmation`() = runTest {
        val noteDetails = mockNoteDetail(isDeleted = true)

        sut.uiState.test {
            initNoteDetails(noteDetails)
            skipItems(2)

            sut.processUiAction(NoteDetailUiAction.ClickDeleteButton)
            skipItems(1)

            sut.processUiAction(NoteDetailUiAction.DismissDeleteConfirmation)
            advanceUntilIdle()

            val uiState = awaitItem()
            assertEquals(false, uiState?.showPermanentDeleteConfirmation)

            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun `verify return to previous screen after permanent delete operation`() = runTest {
        val expected = NoteDetailUiEvent.ReturnToPreviousScreen

        val noteDetails = mockNoteDetail(isDeleted = true)
        initNoteDetails(noteDetails)
        sut.processUiAction(NoteDetailUiAction.ConfirmPermanentDelete)
        advanceUntilIdle()
        val result = sut.uiEventChannel.first()
        assertEquals(expected, result)

        coVerify(Ordering.ORDERED) {
            deleteNoteUseCase.permanentlyDelete(DEFAULT_NOTE_ID)
        }
    }

    @Test
    fun `verify restore note`() = runTest {
        val noteDetails = mockNoteDetail(isDeleted = true)

        val restoreMessage = "Note restored"
        every { context.getString(R.string.message_success_restoreNote) } returns restoreMessage

        initNoteDetails(noteDetails)
        sut.processUiAction(NoteDetailUiAction.ClickRestoreButton)
        advanceUntilIdle()

        coVerify(Ordering.ORDERED) {
            deleteNoteUseCase.setDeleteState(DEFAULT_NOTE_ID, false)
            pinManager.pinNote(DEFAULT_NOTE_ID)
            homeMessageSender.sendMessage(HomeMessage(restoreMessage))
        }
    }

    @Test
    fun `verify show user message`() = runTest {
        val expected = UserMessage(MessageType.ERROR, "Failed to restore note")

        val noteDetails = mockNoteDetail(isDeleted = true)
        every { context.getString(R.string.message_error_restoreNote) } returns expected.text
        coEvery {
            deleteNoteUseCase.setDeleteState(
                DEFAULT_NOTE_ID,
                false
            )
        } throws IOException("note detail fail")

        sut.uiState.test {
            initNoteDetails(noteDetails)
            skipItems(2)

            sut.processUiAction(NoteDetailUiAction.ClickRestoreButton)

            val uiState = awaitItem()
            assertEquals(expected, uiState?.userMessage)
        }
    }

    @Test
    fun `verify dismiss user message`() = runTest {
        val noteDetails = mockNoteDetail(isDeleted = true)
        every { context.getString(R.string.message_error_restoreNote) } returns "Failed to restore note"
        coEvery {
            deleteNoteUseCase.setDeleteState(
                DEFAULT_NOTE_ID,
                false
            )
        } throws IOException("note detail fail")

        sut.uiState.test {
            initNoteDetails(noteDetails)
            skipItems(2)

            sut.processUiAction(NoteDetailUiAction.ClickRestoreButton)
            skipItems(1)

            sut.processUiAction(NoteDetailUiAction.DismissUserMessage)

            val uiState = awaitItem()
            assertNull(uiState?.userMessage)
        }
    }

    @Test
    fun `verify notification permission request`() = runTest {
        val expected = NoteDetailUiEvent.RequestNotificationPermission

        every { buildDetailProvider.getDeviceApiVersion() } returns Build.VERSION_CODES.TIRAMISU

        initNoteDetails()
        sut.processUiAction(NoteFormUpdate.Pin(true))
        advanceUntilIdle()

        val result = sut.uiEventChannel.first()
        assertEquals(expected, result)
    }

    @Test
    fun `verify show notification rationale if permission not granted`() = runTest {
        sut.uiState.test {
            initNoteDetails()
            skipItems(2)

            sut.processUiAction(NoteDetailUiAction.NotificationPermissionRequested(false))

            val uiState = awaitItem()
            assertEquals(true, uiState?.showNotificationRationale)
        }
    }

    @Test
    fun `verify show notification rationale if notification are disabled`() = runTest {
        every { pinManager.isSystemPinningEnabled() } returns false

        sut.uiState.test {
            initNoteDetails()
            skipItems(2)

            sut.processUiAction(NoteDetailUiAction.NotificationPermissionRequested(true))

            val uiState = awaitItem()
            assertEquals(true, uiState?.showNotificationRationale)
        }
    }

    @Test
    fun `verify dismiss notification rationale`() = runTest {
        sut.uiState.test {
            initNoteDetails()
            skipItems(2)

            sut.processUiAction(NoteDetailUiAction.NotificationPermissionRequested(false))
            skipItems(1)

            sut.processUiAction(NoteDetailUiAction.DismissNotificationRationale)

            val uiState = awaitItem()
            assertEquals(false, uiState?.showNotificationRationale)
        }
    }
}
