/*
 * Copyright (c) 2023. Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetails

import java.time.Clock
import java.time.Instant
import java.time.ZoneId

const val DEFAULT_NOTE_ID = 4L

// Tue Sep 11 2001 09:04:00 GMT+8
const val DEFAULT_TIME_MILLIS = 1000170240000
val DEFAULT_CLOCK: Clock = Clock.fixed(
        Instant.ofEpochMilli(DEFAULT_TIME_MILLIS), ZoneId.of("GMT+8")
)
