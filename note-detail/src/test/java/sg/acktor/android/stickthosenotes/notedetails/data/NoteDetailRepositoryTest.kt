/*
 * Copyright (c) 2023. Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetails.data

import io.mockk.Ordering
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.database.api.room.entities.EntityId
import sg.acktor.android.stickthosenotes.database.api.room.entities.NotePosition
import sg.acktor.android.stickthosenotes.database.api.room.entities.TextNote
import sg.acktor.android.stickthosenotes.notedetail.data.NoteDetailRepository
import sg.acktor.android.stickthosenotes.notedetail.data.models.ChecklistItemRoom
import sg.acktor.android.stickthosenotes.notedetail.data.models.ChecklistRoom
import sg.acktor.android.stickthosenotes.notedetail.data.models.ContentApiModel
import sg.acktor.android.stickthosenotes.notedetail.data.models.NoteApiModel
import sg.acktor.android.stickthosenotes.notedetail.data.models.NoteRoom
import sg.acktor.android.stickthosenotes.notedetail.data.models.NoteStateUpdateApiModel
import sg.acktor.android.stickthosenotes.notedetail.data.models.NoteStateUpdateRoomModel
import sg.acktor.android.stickthosenotes.notedetail.data.room.ChecklistCheckUpdate
import sg.acktor.android.stickthosenotes.notedetail.data.room.ChecklistContentUpdate
import sg.acktor.android.stickthosenotes.notedetail.data.room.ChecklistItemDelete
import sg.acktor.android.stickthosenotes.notedetail.data.room.NoteColorPaletteUpdate
import sg.acktor.android.stickthosenotes.notedetail.data.room.NoteDeleteStatus
import sg.acktor.android.stickthosenotes.notedetail.data.room.NoteDetailDAO
import sg.acktor.android.stickthosenotes.notedetail.data.room.NoteLastUpdate
import sg.acktor.android.stickthosenotes.notedetail.data.room.NotePinStateUpdate
import sg.acktor.android.stickthosenotes.notedetail.data.room.NoteTitleUpdate
import sg.acktor.android.stickthosenotes.notedetails.DEFAULT_NOTE_ID
import kotlin.test.Test
import kotlin.test.assertEquals

private const val CHECKLIST_ITEM_ID = 8L

class NoteDetailRepositoryTest {
    private val noteDetailDao = mockk<NoteDetailDAO>(relaxUnitFun = true)

    private val sut = NoteDetailRepository(noteDetailDao)

    @Test
    fun `create item from existing list`() {
        val expected = 2L

        coEvery { noteDetailDao.generateChecklistItemId(DEFAULT_NOTE_ID) } returns expected

        runTest {
            val result = sut.addChecklistItem(DEFAULT_NOTE_ID)
            assertEquals(result, expected)
        }
    }

    @Test
    fun `create item from empty list`() {
        val expected = 1L

        coEvery { noteDetailDao.generateChecklistItemId(DEFAULT_NOTE_ID) } returns null

        runTest {
            val result = sut.addChecklistItem(DEFAULT_NOTE_ID)
            assertEquals(result, expected)
        }
    }

    @Test
    fun `verify get note detail`() = runTest {
        val expected = NoteApiModel(
            title = "title",
            colorPalette = ColorPalette.BLUE,
            isPinned = false,
            lastUpdated = 4L,
            isDeleted = false,
        )

        coEvery { noteDetailDao.getNote(DEFAULT_NOTE_ID) } returns NoteRoom(
            expected.title,
            expected.colorPalette,
            expected.isPinned,
            expected.lastUpdated,
            expected.isDeleted,
        )

        val result = sut.getNote(DEFAULT_NOTE_ID)
        assertEquals(expected, result)
    }

    @Test
    fun `verify get note detail updates`() = runTest {
        val expected = NoteStateUpdateApiModel(
            isPinned = false,
            lastUpdated = 4L,
            isDeleted = false,
        )

        coEvery { noteDetailDao.getNoteStateUpdates(DEFAULT_NOTE_ID) } returns flowOf(
            NoteStateUpdateRoomModel(
                expected.isPinned,
                expected.isDeleted,
                expected.lastUpdated,
            )
        )

        val result = sut.getNoteDetailUpdates(DEFAULT_NOTE_ID).first()
        assertEquals(expected, result)
    }

    @Test
    fun `verify get text content`() = runTest {
        val expected = ContentApiModel.Text(content = "content")

        coEvery { noteDetailDao.getTextContent(DEFAULT_NOTE_ID) } returns expected.content

        val result = sut.getTextContent(DEFAULT_NOTE_ID)
        assertEquals(expected, result)
    }

    @Test
    fun `verify get checklist content`() = runTest {
        val expected = ContentApiModel.Checklist(
            listOf(ContentApiModel.Checklist.Item(2L, "content", true))
        )

        coEvery { noteDetailDao.getChecklistItems(DEFAULT_NOTE_ID) } returns ChecklistRoom(
            DEFAULT_NOTE_ID, expected.items.map {
                ChecklistItemRoom(it.id, it.content, it.isChecked)
            }
        )

        val result = sut.getChecklistContent(DEFAULT_NOTE_ID)
        assertEquals(expected, result)
    }

    @Test
    fun `verify text content update`() = runTest {
        val content = "content"
        sut.updateTextContent(DEFAULT_NOTE_ID, content)
        coVerify {
            noteDetailDao.updateTextContent(TextNote(DEFAULT_NOTE_ID, content))
        }
    }

    @Test
    fun `verify checklist content update`() = runTest {
        val content = "content"
        sut.updateChecklistItemContent(CHECKLIST_ITEM_ID, DEFAULT_NOTE_ID, content)
        coVerify {
            noteDetailDao.updateChecklistContent(
                ChecklistContentUpdate(CHECKLIST_ITEM_ID, DEFAULT_NOTE_ID, content)
            )
        }
    }

    @Test
    fun `verify checklist checked update`() = runTest {
        val isChecked = true
        sut.updateChecklistItemCheckedState(CHECKLIST_ITEM_ID, DEFAULT_NOTE_ID, isChecked)
        coVerify {
            noteDetailDao.updateChecklistCheckedState(
                ChecklistCheckUpdate(CHECKLIST_ITEM_ID, DEFAULT_NOTE_ID, isChecked)
            )
        }
    }

    @Test
    fun `verify delete checklist item`() = runTest {
        sut.deleteChecklistItem(CHECKLIST_ITEM_ID, DEFAULT_NOTE_ID)
        coVerify {
            noteDetailDao.deleteChecklistItem(
                ChecklistItemDelete(CHECKLIST_ITEM_ID, DEFAULT_NOTE_ID)
            )
        }
    }

    @Test
    fun `verify mark note as deleted`() = runTest {
        val isDeleted = true
        sut.setDeleteStatus(DEFAULT_NOTE_ID, isDeleted)
        coVerify(Ordering.ALL) {
            noteDetailDao.updateDeleteStatus(NoteDeleteStatus(DEFAULT_NOTE_ID, isDeleted))
        }
    }

    @Test
    fun `verify mark note as not deleted`() = runTest {
        val isDeleted = false
        val position = 4
        coEvery { noteDetailDao.generateNextPosition() } returns position

        sut.setDeleteStatus(DEFAULT_NOTE_ID, isDeleted)
        coVerify(Ordering.SEQUENCE) {
            noteDetailDao.updateDeleteStatus(NoteDeleteStatus(DEFAULT_NOTE_ID, isDeleted))
            noteDetailDao.generateNextPosition()
            noteDetailDao.updateNotePosition(NotePosition(DEFAULT_NOTE_ID, position))
        }
    }

    @Test
    fun `verify permanent delete note`() = runTest {
        sut.permanentlyDeleteNote(DEFAULT_NOTE_ID)
        coVerify {
            noteDetailDao.permanentlyDeleteNote(EntityId(DEFAULT_NOTE_ID))
        }
    }

    @Test
    fun `verify update title`() = runTest {
        val title = "title"
        val lastUpdated = 43L
        sut.updateTitle(DEFAULT_NOTE_ID, title, lastUpdated)
        coVerify {
            noteDetailDao.updateNoteTitle(NoteTitleUpdate(DEFAULT_NOTE_ID, title, lastUpdated))
        }
    }

    @Test
    fun `verify update color palette`() = runTest {
        val colorPalette = ColorPalette.DEEP_PURPLE
        val lastUpdated = 43L
        sut.updateColorPalette(DEFAULT_NOTE_ID, colorPalette, lastUpdated)
        coVerify {
            noteDetailDao.updateNoteColorPalette(
                NoteColorPaletteUpdate(DEFAULT_NOTE_ID, colorPalette, lastUpdated)
            )
        }
    }

    @Test
    fun `verify update pin state`() = runTest {
        val isPinned = false
        val lastUpdated = 43L
        sut.updatePinState(DEFAULT_NOTE_ID, isPinned, lastUpdated)
        coVerify {
            noteDetailDao.updateNotePinState(
                NotePinStateUpdate(DEFAULT_NOTE_ID, isPinned, lastUpdated)
            )
        }
    }

    @Test
    fun `verify update last updated`() = runTest {
        val lastUpdated = 68L
        sut.updateLastUpdated(DEFAULT_NOTE_ID, lastUpdated)
        coVerify {
            noteDetailDao.updateLastUpdated(
                NoteLastUpdate(DEFAULT_NOTE_ID, lastUpdated)
            )
        }
    }
}
