/*
 * Copyright (c) 2023. Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetails.usecases

import io.mockk.coJustRun
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.notedetail.data.NoteDetailRepository
import sg.acktor.android.stickthosenotes.notedetail.usecases.DeleteNoteUseCaseImpl
import sg.acktor.android.stickthosenotes.notedetails.DEFAULT_NOTE_ID
import kotlin.test.Test

class DeleteNoteUseCaseTest {
    private val repo = mockk<NoteDetailRepository> {
        coJustRun { setDeleteStatus(any(), true) }
        coJustRun { permanentlyDeleteNote(any()) }
    }

    private val sut = DeleteNoteUseCaseImpl(repo)

    @Test
    fun `note is permanently deleted`() {
        runTest {
            sut.permanentlyDelete(DEFAULT_NOTE_ID)
        }

        coVerify {
            repo.permanentlyDeleteNote(any())
        }
    }

    @Test
    fun `verify delete state update`() = runTest {
        sut.setDeleteState(DEFAULT_NOTE_ID, true)

        coVerify {
            repo.setDeleteStatus(DEFAULT_NOTE_ID, true)
        }
    }
}
