package sg.acktor.android.stickthosenotes.notedetails

import android.content.Context
import android.os.Build
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import sg.acktor.android.stickthosenotes.common.BuildDetailProvider
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.home.api.HomeMessageSender
import sg.acktor.android.stickthosenotes.notedetail.NoteDetailViewModel
import sg.acktor.android.stickthosenotes.notedetail.mappers.NoteUiStateMapper
import sg.acktor.android.stickthosenotes.notedetail.ui.NavParams
import sg.acktor.android.stickthosenotes.notedetail.usecases.DeleteNoteUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.GetNoteUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.UpdateChecklistUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.UpdateNoteUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.UpdateTextContentUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteContentUiModel
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteDetailUiModel
import sg.acktor.android.stickthosenotes.notedetails.utils.mockNoteDetail
import sg.acktor.android.stickthosenotes.notedetails.utils.mockTextContent
import sg.acktor.android.stickthosenotes.pin.api.PinManager
import java.io.IOException

@OptIn(ExperimentalCoroutinesApi::class)
internal abstract class BaseNoteDetailViewModelTest {
    companion object {
        @BeforeAll
        @JvmStatic
        fun setUpAll() {
            Dispatchers.setMain(StandardTestDispatcher())
        }

        @AfterAll
        @JvmStatic
        fun tearDownAll() {
            Dispatchers.resetMain()
        }
    }

    protected val context = mockk<Context>()
    protected val buildDetailProvider = mockk<BuildDetailProvider> {
        every { getDeviceApiVersion() } returns Build.VERSION_CODES.R
    }
    protected val noteUiStateMapper = NoteUiStateMapper()
    protected val getNoteUseCase = mockk<GetNoteUseCase> {
        every { getNoteStateUpdates(DEFAULT_NOTE_ID) } returns flowOf()
    }
    protected val pinManager = mockk<PinManager>(relaxUnitFun = true) {
        every { isSystemPinningEnabled() } returns true
    }
    protected val updateNoteUseCase = mockk<UpdateNoteUseCase>(relaxUnitFun = true)
    protected val updateTextContentUseCase = mockk<UpdateTextContentUseCase>(relaxUnitFun = true)
    protected val updateChecklistUseCase = mockk<UpdateChecklistUseCase>(relaxUnitFun = true)
    protected val deleteNoteUseCase = mockk<DeleteNoteUseCase>(relaxUnitFun = true)
    protected val homeMessageSender = mockk<HomeMessageSender>(relaxUnitFun = true)

    protected val sut = NoteDetailViewModel(
        context = context,
        buildDetailProvider = buildDetailProvider,
        uiStateMapper = noteUiStateMapper,
        getNoteUseCase = getNoteUseCase,
        pinManager = pinManager,
        updateNoteUseCase = updateNoteUseCase,
        updateTextContentUseCase = updateTextContentUseCase,
        updateChecklistUseCase = updateChecklistUseCase,
        deleteNoteUseCase = deleteNoteUseCase,
        homeMessageSender = homeMessageSender,
    )

    protected fun TestScope.initNoteDetails(
        details: NoteDetailUiModel? = mockNoteDetail(),
        content: NoteContentUiModel? = mockTextContent(),
    ) {
        val contentType = when (content) {
            is NoteContentUiModel.Checklist -> ContentType.CHECKLIST
            else -> ContentType.TEXT
        }

        val getDetailsCall = coEvery { getNoteUseCase.getDetails(DEFAULT_NOTE_ID) }
        if (details == null) {
            getDetailsCall throws IOException("get details fail")
        } else {
            getDetailsCall returns details
        }

        val getContentCall = coEvery { getNoteUseCase.getContent(DEFAULT_NOTE_ID, contentType) }
        if (content == null) {
            getContentCall throws IOException("get content fail")
        } else {
            getContentCall returns content
        }

        sut.loadNote(NavParams(DEFAULT_NOTE_ID, contentType))
        advanceUntilIdle()
    }
}
