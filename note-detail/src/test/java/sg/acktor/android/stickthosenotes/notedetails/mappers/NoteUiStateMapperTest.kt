/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetails.mappers

import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.notedetail.mappers.NoteUiStateMapper
import sg.acktor.android.stickthosenotes.notedetail.models.ui.NoteScreenUiModel
import sg.acktor.android.stickthosenotes.notedetail.ui.CardUiState
import sg.acktor.android.stickthosenotes.notedetail.ui.FooterUiState
import sg.acktor.android.stickthosenotes.notedetail.ui.MessageType
import sg.acktor.android.stickthosenotes.notedetail.ui.NoteUiState
import sg.acktor.android.stickthosenotes.notedetail.ui.TopBarUiState
import sg.acktor.android.stickthosenotes.notedetail.ui.UserMessage
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteContentUiModel
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteDetailUiModel
import kotlin.test.Test
import kotlin.test.assertEquals

class NoteUiStateMapperTest {
    private val sut = NoteUiStateMapper()

    @Test
    fun `verify mapping`() {
        val expected = NoteUiState(
            topBarUiState = TopBarUiState(
                isPinChecked = true,
                isPinEnabled = true,
                showRestoreButton = false,
            ),
            cardUiState = CardUiState(
                isEnabled = true,
                title = "some title",
                content = NoteContentUiModel.Text("some content"),
            ),
            footerUiState = FooterUiState(
                lastUpdated = "last updated",
                showColorPicker = true,
            ),
            userMessage = UserMessage(MessageType.ERROR, "have a good day"),
            colorPalette = ColorPalette.BLUE_GREY,
            showNotificationRationale = false,
            showPermanentDeleteConfirmation = false,
        )

        val noteDetail = NoteDetailUiModel(
            title = "some title",
            colorPalette = ColorPalette.BLUE_GREY,
            isPinned = true,
            lastUpdated = "last updated",
            isDeleted = false,
        )
        val noteContent = NoteContentUiModel.Text("some content")
        val screenUiModel = NoteScreenUiModel(
            userMessage = UserMessage(MessageType.ERROR, "have a good day"),
            showNotificationRationale = false,
            showPermanentDeleteConfirmation = false,
        )

        val result = sut.map(noteDetail, noteContent, screenUiModel)
        assertEquals(expected, result)
    }
}
