package sg.acktor.android.stickthosenotes.notedetails.usecases

import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Test
import sg.acktor.android.stickthosenotes.notedetail.data.NoteDetailRepository
import sg.acktor.android.stickthosenotes.notedetail.usecases.UpdateTextContentUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.UpdateTextContentUseCaseImpl
import sg.acktor.android.stickthosenotes.notedetails.DEFAULT_NOTE_ID

class UpdateTextContentUseCaseTest {
    private val repo = mockk<NoteDetailRepository>(relaxUnitFun = true)
    private val sut: UpdateTextContentUseCase = UpdateTextContentUseCaseImpl(repo)

    @Test
    fun `verify text content update`() = runTest {
        val content = "content"
        sut.run(DEFAULT_NOTE_ID, content)
        coVerify {
            repo.updateTextContent(DEFAULT_NOTE_ID, content)
        }
    }
}
