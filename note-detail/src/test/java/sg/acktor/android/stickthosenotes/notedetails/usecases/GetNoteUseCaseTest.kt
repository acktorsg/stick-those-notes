package sg.acktor.android.stickthosenotes.notedetails.usecases

import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Test
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.notedetail.data.NoteDetailRepository
import sg.acktor.android.stickthosenotes.notedetail.data.models.ContentApiModel
import sg.acktor.android.stickthosenotes.notedetail.data.models.NoteApiModel
import sg.acktor.android.stickthosenotes.notedetail.data.models.NoteStateUpdateApiModel
import sg.acktor.android.stickthosenotes.notedetail.mappers.LastUpdatedTextMapper
import sg.acktor.android.stickthosenotes.notedetail.usecases.GetNoteUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.GetNoteUseCaseImpl
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteContentUiModel
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteDetailUiModel
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteStateUpdate
import sg.acktor.android.stickthosenotes.notedetails.DEFAULT_NOTE_ID
import kotlin.test.assertEquals

class GetNoteUseCaseTest {
    private val repo = mockk<NoteDetailRepository>()
    private val lastUpdatedTextMapper = mockk<LastUpdatedTextMapper>()
    private val sut: GetNoteUseCase = GetNoteUseCaseImpl(repo, lastUpdatedTextMapper)

    @Test
    fun `verify note state updates`() = runTest {
        val lastUpdated = 2L
        val expected = NoteStateUpdate("last update", true, false)

        val repoFlow = flowOf(
            NoteStateUpdateApiModel(lastUpdated, expected.isPinned, expected.isDeleted)
        )
        every { repo.getNoteDetailUpdates(DEFAULT_NOTE_ID) } returns repoFlow
        every { lastUpdatedTextMapper.map(lastUpdated) } returns expected.lastUpdated

        val result = sut.getNoteStateUpdates(DEFAULT_NOTE_ID).first()
        assertEquals(expected, result)
    }

    @Test
    fun `verify get note details`() = runTest {
        val expected = NoteDetailUiModel(
            "title",
            ColorPalette.BLUE_GREY,
            true,
            "last update",
            false,
        )

        val lastUpdated = 3L
        coEvery { repo.getNote(DEFAULT_NOTE_ID) } returns NoteApiModel(
            expected.title,
            expected.colorPalette,
            expected.isPinned,
            lastUpdated,
            expected.isDeleted,
        )
        every { lastUpdatedTextMapper.map(lastUpdated) } returns expected.lastUpdated

        val result = sut.getDetails(DEFAULT_NOTE_ID)
        assertEquals(expected, result)
    }

    @Test
    fun `verify get text content`() = runTest {
        val expected = NoteContentUiModel.Text("content")
        coEvery { repo.getTextContent(DEFAULT_NOTE_ID) } returns ContentApiModel.Text(expected.content)

        val result = sut.getContent(DEFAULT_NOTE_ID, ContentType.TEXT)
        assertEquals(expected, result)
    }

    @Test
    fun `verify get checklist content`() = runTest {
        val expected = NoteContentUiModel.Checklist(
            listOf(NoteContentUiModel.Checklist.Item(4L, "content", true))
        )

        coEvery { repo.getChecklistContent(DEFAULT_NOTE_ID) } returns ContentApiModel.Checklist(
            expected.items.map { ContentApiModel.Checklist.Item(it.id, it.content, it.isChecked) }
        )

        val result = sut.getContent(DEFAULT_NOTE_ID, ContentType.CHECKLIST)
        assertEquals(expected, result)
    }
}
