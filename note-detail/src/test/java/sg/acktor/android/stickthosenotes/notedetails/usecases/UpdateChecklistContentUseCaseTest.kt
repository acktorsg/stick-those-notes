package sg.acktor.android.stickthosenotes.notedetails.usecases

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.notedetail.data.NoteDetailRepository
import sg.acktor.android.stickthosenotes.notedetail.usecases.UpdateChecklistUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.UpdateChecklistUseCaseImpl
import sg.acktor.android.stickthosenotes.notedetails.DEFAULT_NOTE_ID
import kotlin.test.Test
import kotlin.test.assertEquals

private const val CHECKLIST_ITEM_ID = 276L

class UpdateChecklistContentUseCaseTest {
    private val repo = mockk<NoteDetailRepository>(relaxUnitFun = true)
    private val sut: UpdateChecklistUseCase = UpdateChecklistUseCaseImpl(repo)

    @Test
    fun `verify content update`() = runTest {
        val content = "content"
        sut.updateContent(DEFAULT_NOTE_ID, CHECKLIST_ITEM_ID, content)
        coVerify {
            repo.updateChecklistItemContent(CHECKLIST_ITEM_ID, DEFAULT_NOTE_ID, content)
        }
    }

    @Test
    fun `verify check state update`() = runTest {
        val isChecked = true
        sut.updateCheckedState(DEFAULT_NOTE_ID, CHECKLIST_ITEM_ID, isChecked)
        coVerify {
            repo.updateChecklistItemCheckedState(CHECKLIST_ITEM_ID, DEFAULT_NOTE_ID, isChecked)
        }
    }

    @Test
    fun `verify add checklist item`() = runTest {
        val expected = 13L
        coEvery { repo.addChecklistItem(DEFAULT_NOTE_ID) } returns expected

        val result = sut.addChecklistItem(DEFAULT_NOTE_ID)
        assertEquals(expected, result)
        coVerify {
            repo.addChecklistItem(DEFAULT_NOTE_ID)
        }
    }

    @Test
    fun `verify delete checklist item`() = runTest {
        sut.deleteChecklistItem(DEFAULT_NOTE_ID, CHECKLIST_ITEM_ID)
        coVerify {
            repo.deleteChecklistItem(CHECKLIST_ITEM_ID, DEFAULT_NOTE_ID)
        }
    }
}
