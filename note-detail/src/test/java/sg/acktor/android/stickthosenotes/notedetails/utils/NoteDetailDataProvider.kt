package sg.acktor.android.stickthosenotes.notedetails.utils

import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteContentUiModel
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteDetailUiModel

fun mockNoteDetail(
    title: String = "mock title",
    isPinned: Boolean = false,
    isDeleted: Boolean = false,
) = NoteDetailUiModel(
    title = title,
    colorPalette = ColorPalette.WHITE,
    isPinned = isPinned,
    lastUpdated = "",
    isDeleted = isDeleted,
)

fun mockTextContent(content: String = "mock content") = NoteContentUiModel.Text(content)

fun mockEmptyChecklistContent() = NoteContentUiModel.Checklist(emptyList())
