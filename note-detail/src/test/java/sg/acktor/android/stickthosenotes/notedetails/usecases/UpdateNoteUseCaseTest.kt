package sg.acktor.android.stickthosenotes.notedetails.usecases

import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Test
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.notedetail.data.NoteDetailRepository
import sg.acktor.android.stickthosenotes.notedetail.usecases.UpdateNoteUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.UpdateNoteUseCaseImpl
import sg.acktor.android.stickthosenotes.notedetails.DEFAULT_CLOCK
import sg.acktor.android.stickthosenotes.notedetails.DEFAULT_NOTE_ID
import sg.acktor.android.stickthosenotes.notedetails.DEFAULT_TIME_MILLIS

class UpdateNoteUseCaseTest {
    private val repo = mockk<NoteDetailRepository>(relaxUnitFun = true)
    private val sut: UpdateNoteUseCase = UpdateNoteUseCaseImpl(repo, DEFAULT_CLOCK)

    @Test
    fun `verify title update`() = runTest {
        val title = "title"
        sut.updateTitle(DEFAULT_NOTE_ID, title)
        coVerify {
            repo.updateTitle(DEFAULT_NOTE_ID, title, DEFAULT_TIME_MILLIS)
        }
    }

    @Test
    fun `verify color palette update`() = runTest {
        val colorPalette = ColorPalette.BLACK
        sut.updateColorPalette(DEFAULT_NOTE_ID, colorPalette)
        coVerify {
            repo.updateColorPalette(DEFAULT_NOTE_ID, colorPalette, DEFAULT_TIME_MILLIS)
        }
    }

    @Test
    fun `verify pin state update`() = runTest {
        val isPinned = true
        sut.updatePinState(DEFAULT_NOTE_ID, isPinned)
        coVerify {
            repo.updatePinState(DEFAULT_NOTE_ID, isPinned, DEFAULT_TIME_MILLIS)
        }
    }

    @Test
        fun `verify last updated update`() = runTest {
        sut.updateLastUpdated(DEFAULT_NOTE_ID)
        coVerify {
            repo.updateLastUpdated(DEFAULT_NOTE_ID, DEFAULT_TIME_MILLIS)
        }
    }
}
