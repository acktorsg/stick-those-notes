/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material.icons.rounded.Clear
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.tooling.preview.Preview
import sg.acktor.acktorsdk.ui.compose.Dimensions
import sg.acktor.android.stickthosenotes.common.LocalNoteThemes
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.common.models.NoteTheme
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.notedetail.R
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteContentUiModel

@Composable
internal fun ContentCard(
    content: NoteContentUiModel,
    colorPalette: ColorPalette,
    isEnabled: Boolean,
    sendFormUpdate: (NoteFormUpdate) -> Unit,
) {
    val contentScrollState = rememberScrollState()

    val themes = LocalNoteThemes.current
    val theme = themes[colorPalette]

    Card(
        colors = CardDefaults.cardColors(
            containerColor = theme.lightBackground,
            contentColor = theme.onLightBackground,
        )
    ) {
        when (content) {
            is NoteContentUiModel.Text -> TextContent(
                modifier = Modifier.verticalScroll(contentScrollState),
                content = content.content,
                enabled = isEnabled,
                theme = theme,
                onTextChanged = { sendFormUpdate(NoteFormUpdate.TextContent(it)) },
            )

            is NoteContentUiModel.Checklist -> ChecklistContent(
                modifier = Modifier
                    .padding(Dimensions.spacingSmall)
                    .verticalScroll(contentScrollState),
                content = content.items,
                enabled = isEnabled,
                theme = theme,
                sendUpdate = sendFormUpdate,
            )
        }
    }
}

@Composable
internal fun TextContent(
    modifier: Modifier = Modifier,
    content: String,
    enabled: Boolean,
    theme: NoteTheme,
    onTextChanged: (String) -> Unit,
) {
    val textColor = theme.onLightBackground
    val containerColor = Color.Transparent

    TextField(
        modifier = modifier.fillMaxWidth(),
        value = content,
        onValueChange = onTextChanged,
        enabled = enabled,
        placeholder = { Text(stringResource(R.string.hint_editText_content)) },
        minLines = 3,
        keyboardOptions = KeyboardOptions(capitalization = KeyboardCapitalization.Sentences),
        colors = TextFieldDefaults.colors(
            unfocusedTextColor = textColor,
            focusedTextColor = textColor,
            disabledTextColor = textColor,
            cursorColor = textColor,
            unfocusedContainerColor = containerColor,
            focusedContainerColor = containerColor,
            disabledContainerColor = containerColor,
            unfocusedIndicatorColor = containerColor,
            focusedIndicatorColor = containerColor,
            disabledIndicatorColor = containerColor,
        )
    )
}

@Composable
internal fun ChecklistContent(
    modifier: Modifier = Modifier,
    content: List<NoteContentUiModel.Checklist.Item>,
    enabled: Boolean,
    theme: NoteTheme,
    sendUpdate: (ChecklistFormUpdate) -> Unit,
) {
    val focusRequester = remember { FocusRequester() }
    var shouldChangeFocus by remember { mutableStateOf(false) }
    LaunchedEffect(content) {
        if (shouldChangeFocus) {
            focusRequester.requestFocus()
            shouldChangeFocus = false
        }
    }

    val createNewItem = {
        shouldChangeFocus = true
        sendUpdate(ChecklistFormUpdate.NewItem)
    }

    Column(modifier = modifier) {
        val contentColor = theme.onLightBackground

        content.mapIndexed { i, checklistItem ->
            ChecklistItemRow(
                checklistItem = checklistItem,
                pos = i,
                backgroundColor = theme.lightBackground,
                contentColor = contentColor,
                enabled = enabled,
                textFieldFocusRequester = focusRequester.takeIf { i == content.lastIndex },
                sendFormUpdate = sendUpdate,
                createNewItem = createNewItem,
            )
        }

        if (enabled) {
            TextButton(
                onClick = createNewItem,
                colors = ButtonDefaults.textButtonColors(
                    contentColor = contentColor
                )
            ) {
                Icon(Icons.Rounded.Add, contentDescription = "Add new checklist item")
                Spacer(Modifier.width(Dimensions.spacingSmall))
                Text(stringResource(R.string.title_button_addRow))
            }
        }
    }
}

@Composable
private fun ChecklistItemRow(
    checklistItem: NoteContentUiModel.Checklist.Item,
    pos: Int,
    backgroundColor: Color,
    contentColor: Color,
    enabled: Boolean,
    textFieldFocusRequester: FocusRequester?,
    sendFormUpdate: (ChecklistFormUpdate) -> Unit,
    createNewItem: () -> Unit,
) {
    var content by remember { mutableStateOf(checklistItem.content) }
    var isChecked by remember { mutableStateOf(checklistItem.isChecked) }

    Row(
        Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Checkbox(
            checked = isChecked,
            onCheckedChange = {
                sendFormUpdate(ChecklistFormUpdate.CheckState(checklistItem.id, pos, it))
                isChecked = it
            },
            enabled = enabled,
            colors = CheckboxDefaults.colors(
                checkedColor = contentColor,
                uncheckedColor = contentColor,
                checkmarkColor = backgroundColor,
                disabledCheckedColor = contentColor,
                disabledUncheckedColor = contentColor,
            )
        )
        TextField(
            modifier = Modifier.weight(1f)
                .then(
                    textFieldFocusRequester?.let {
                        Modifier.focusRequester(it)
                    } ?: Modifier
                ),
            value = content,
            onValueChange = {
                sendFormUpdate(ChecklistFormUpdate.Content(checklistItem.id, pos, it))
                content = it
            },
            enabled = enabled,
            colors = TextFieldDefaults.colors(
                focusedTextColor = contentColor,
                unfocusedTextColor = contentColor,
                disabledTextColor = contentColor,
                unfocusedContainerColor = Color.Transparent,
                focusedContainerColor = Color.Transparent,
                disabledContainerColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent,
            ),
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Next
            ),
            keyboardActions = KeyboardActions {
                createNewItem()
            },
        )

        if (enabled) {
            IconButton(
                onClick = {
                    sendFormUpdate(ChecklistFormUpdate.Delete(checklistItem.id, pos))
                },
                colors = IconButtonDefaults.iconButtonColors(
                    contentColor = contentColor
                )
            ) {
                Icon(Icons.Rounded.Clear, contentDescription = "Delete checklist item")
            }
        }
    }
}

@Preview(showBackground = true, backgroundColor = 0xffFFE082)
@Composable
private fun ChecklistItemRowPreview() {
    val checklistItem = NoteContentUiModel.Checklist.Item(
        id = 0,
        content = "This is some long checklist item content",
        isChecked = true
    )

    StickThoseNotesTheme {
        val theme = LocalNoteThemes.current[ColorPalette.AMBER]

        ChecklistItemRow(
            checklistItem = checklistItem,
            pos = 1,
            backgroundColor = theme.lightBackground,
            contentColor = theme.onLightBackground,
            enabled = true,
            textFieldFocusRequester = null,
            sendFormUpdate = {},
            createNewItem = {},
        )
    }
}
