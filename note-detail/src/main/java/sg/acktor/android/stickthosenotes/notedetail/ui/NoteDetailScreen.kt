/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.ui

import android.Manifest
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.displayCutoutPadding
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import sg.acktor.acktorsdk.ui.compose.Dimensions
import sg.acktor.android.stickthosenotes.common.LocalRootNavigator
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.notedetail.NoteDetailViewModel
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteContentUiModel

private val cardCornerRadius = 12.dp

@Composable
internal fun NoteDetailsScreen(
    noteDetailViewModel: NoteDetailViewModel = hiltViewModel(),
    navParams: NavParams,
) {
    val rootNavigator = LocalRootNavigator.current

    val permissionLauncher = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        noteDetailViewModel.processUiAction(
            NoteDetailUiAction.NotificationPermissionRequested(isGranted)
        )
    }

    val uiState by noteDetailViewModel.uiState.collectAsStateWithLifecycle()
    LaunchedEffect(navParams) {
        noteDetailViewModel.loadNote(navParams)
    }

    LaunchedEffect(noteDetailViewModel) {
        noteDetailViewModel.uiEventChannel.collect { event ->
            when (event) {
                NoteDetailUiEvent.ReturnToPreviousScreen -> rootNavigator.returnToHomeScreen()
                NoteDetailUiEvent.RequestNotificationPermission -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                        permissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
                    }
                }
            }
        }
    }

    val state = uiState
    if (state == null) {
        LoadingScreen()
    } else {
        NoteDetailContent(
            uiState = state,
            sendUiAction = noteDetailViewModel::processUiAction,
        )
    }
}

@Composable
private fun LoadingScreen() {
    CircularProgressIndicator(
        modifier = Modifier.padding(Dimensions.spacingDefault)
    )
}

@Composable
private fun NoteDetailContent(
    uiState: NoteUiState,
    sendUiAction: (NoteDetailUiAction) -> Unit,
) {
    val config = LocalConfiguration.current
    val context = LocalContext.current

    Column(
        modifier = Modifier
            .systemBarsPadding()
            .displayCutoutPadding()
            .imePadding()
            .padding(horizontal = Dimensions.spacingDefault)
            .then(
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.VANILLA_ICE_CREAM) {
                    if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                        val maxWidth = (config.screenWidthDp * (4.0 / 5)).dp
                        Modifier.widthIn(max = maxWidth)
                    } else {
                        val maxHeight = (config.screenHeightDp * (4.0 / 5)).dp
                        Modifier.heightIn(max = maxHeight)
                    }
                } else {
                    Modifier
                }
            ),
        verticalArrangement = Arrangement.Bottom,
    ) {
        var showColorSelectionMenu by remember { mutableStateOf(false) }

        NoteTopBar(
            uiState = uiState.topBarUiState,
            sendUiAction = sendUiAction,
        )
        Spacer(Modifier.height(Dimensions.spacingSmall))

        NoteDetailCard(
            modifier = Modifier
                .weight(1f, fill = false)
                .zIndex(1f),
            uiState = uiState.cardUiState,
            colorPalette = uiState.colorPalette,
            cornerRadius = cardCornerRadius,
            sendFormUpdate = sendUiAction,
        )

        AnimatedContent(
            modifier = Modifier.offset(y = -cardCornerRadius),
            targetState = uiState.userMessage,
            label = "User message animation",
            transitionSpec = {
                val enterTransition = if (targetState != null) {
                    slideInVertically(initialOffsetY = { -it })
                } else EnterTransition.None
                val exitTransition = if (initialState != null) {
                    slideOutVertically(targetOffsetY = { -it })
                } else ExitTransition.None

                enterTransition togetherWith exitTransition
            },
            contentKey = { it == null },
        ) { message ->
            Column {
                val containerColor = when (message?.type) {
                    MessageType.NORMAL -> MaterialTheme.colorScheme.primaryContainer
                    MessageType.ERROR -> MaterialTheme.colorScheme.errorContainer
                    null -> Color.Transparent
                }

                Spacer(
                    Modifier
                        .fillMaxWidth()
                        .height(cardCornerRadius)
                        .background(containerColor)
                )
                if (message != null) {
                    UserMessageBar(
                        userMessage = message,
                        containerShape = RoundedCornerShape(
                            bottomStart = cardCornerRadius,
                            bottomEnd = cardCornerRadius,
                        ),
                        containerColor = containerColor,
                        clearMessage = { sendUiAction(NoteDetailUiAction.DismissUserMessage) }
                    )
                }
            }
        }

        NoteFooter(
            uiState = uiState.footerUiState,
            showColorSelectionMenu = { showColorSelectionMenu = true },
            closeNote = { sendUiAction(NoteDetailUiAction.CloseNote) }
        )
        Spacer(Modifier.height(Dimensions.spacingSmall))

        if (showColorSelectionMenu) {
            ColorSelectionMenu(
                currentPalette = uiState.colorPalette,
                selectColor = {
                    sendUiAction(NoteFormUpdate.Color(it))
                    showColorSelectionMenu = false
                },
                dismissMenu = {
                    showColorSelectionMenu = false
                }
            )
        }
    }

    if (uiState.showNotificationRationale) {
        NotifRationaleDialog(
            openSettingsApp = {
                val packageName = context.packageName
                context.startActivity(
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        // Cannot request permission using ActivityResultLauncher. It stops
                        // working when app is closed from recent apps or receives an update
                        Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .putExtra(Settings.EXTRA_APP_PACKAGE, packageName)
                    } else {
                        // Cannot open permissions page directly before Oreo. Open app
                        // details page instead
                        Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            .setData(Uri.fromParts("package", packageName, null))
                    }
                )
                sendUiAction(NoteDetailUiAction.DismissNotificationRationale)
            },
            dismissDialog = { sendUiAction(NoteDetailUiAction.DismissNotificationRationale) }
        )
    }

    if (uiState.showPermanentDeleteConfirmation) {
        PermanentDeleteDialog(
            permanentlyDeleteNote = { sendUiAction(NoteDetailUiAction.ConfirmPermanentDelete) },
            dismissDialog = {
                sendUiAction(NoteDetailUiAction.DismissDeleteConfirmation)
            }
        )
    }
}

@Preview
@Composable
private fun NoteDetailScreenPreview() {
    StickThoseNotesTheme {
        NoteDetailContent(
            uiState = NoteUiState(
                topBarUiState = TopBarUiState(
                    isPinChecked = false,
                    isPinEnabled = false,
                    showRestoreButton = true,
                ),
                cardUiState = CardUiState(
                    isEnabled = false,
                    title = "title",
                    content = NoteContentUiModel.Checklist(
                        listOf(
                            NoteContentUiModel.Checklist.Item(
                                id = 2L,
                                content = "content",
                                isChecked = false,
                            ),
                            NoteContentUiModel.Checklist.Item(
                                id = 3L,
                                content = "content 2",
                                isChecked = true,
                            ),
                        )
                    ),
                ),
                footerUiState = FooterUiState(
                    lastUpdated = "Last updated 3 decades ago",
                    showColorPicker = true,
                ),
                userMessage = null,
                colorPalette = ColorPalette.LIME,
                showNotificationRationale = false,
                showPermanentDeleteConfirmation = false,
            ),
            sendUiAction = {},
        )
    }
}

@Preview
@Composable
private fun MessageAnimationPreview() {
    StickThoseNotesTheme {
        var userMessage by remember {
            mutableStateOf<UserMessage?>(UserMessage(MessageType.ERROR, "Failed to save note"))
        }

        NoteDetailContent(
            uiState = NoteUiState(
                topBarUiState = TopBarUiState(
                    isPinChecked = true,
                    isPinEnabled = true,
                    showRestoreButton = false,
                ),
                cardUiState = CardUiState(
                    isEnabled = false,
                    title = "title",
                    content = NoteContentUiModel.Text("content"),
                ),
                footerUiState = FooterUiState(
                    lastUpdated = "Last updated 3 decades ago",
                    showColorPicker = true,
                ),
                userMessage = null,
                colorPalette = ColorPalette.LIME,
                showNotificationRationale = false,
                showPermanentDeleteConfirmation = false,
            ),
            sendUiAction = {
                userMessage = if (it == NoteDetailUiAction.DismissUserMessage) {
                    null
                } else {
                    UserMessage(MessageType.ERROR, "Failed to save note")
                }
            },
        )
    }
}
