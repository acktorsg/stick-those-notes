/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.usecases.models

import sg.acktor.android.stickthosenotes.common.models.ColorPalette

data class NoteDetailUiModel(
    val title: String,
    val colorPalette: ColorPalette,
    val isPinned: Boolean,
    val lastUpdated: String,
    val isDeleted: Boolean,
)

sealed interface NoteContentUiModel {
    data class Text(val content: String) : NoteContentUiModel

    data class Checklist(val items: List<Item>) : NoteContentUiModel {
        data class Item(
            val id: Long,
            var content: String = "",
            var isChecked: Boolean = false
        )
    }
}

data class NoteStateUpdate(
    val lastUpdated: String,
    val isPinned: Boolean,
    val isDeleted: Boolean,
)
