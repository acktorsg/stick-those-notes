/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.ui

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import sg.acktor.android.stickthosenotes.common.LocalNoteThemes
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.notedetail.R

@Composable
internal fun NoteDetailCard(
    modifier: Modifier = Modifier,
    uiState: CardUiState,
    colorPalette: ColorPalette,
    cornerRadius: Dp,
    sendFormUpdate: (NoteFormUpdate) -> Unit,
) {
    val themes = LocalNoteThemes.current
    val theme = themes[colorPalette]

    ElevatedCard(
        modifier.fillMaxWidth(),
        shape = RoundedCornerShape(cornerRadius),
        colors = CardDefaults.cardColors(
            containerColor = theme.darkBackground,
            contentColor = theme.onDarkBackground,
        )
    ) {
        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = uiState.title,
            onValueChange = { sendFormUpdate(NoteFormUpdate.Title(it)) },
            placeholder = {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = stringResource(R.string.hint_editText_title),
                    style = MaterialTheme.typography.titleLarge
                        .copy(
                            textAlign = TextAlign.Center,
                            fontWeight = FontWeight.Bold
                        ),
                )
            },
            enabled = uiState.isEnabled,
            textStyle = MaterialTheme.typography.titleLarge.copy(
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Bold
            ),
            singleLine = true,
            colors = TextFieldDefaults.colors(
                unfocusedTextColor = theme.onDarkBackground,
                focusedTextColor = theme.onDarkBackground,
                disabledTextColor = theme.onDarkBackground,
                cursorColor = theme.onDarkBackground,
                unfocusedContainerColor = Color.Transparent,
                focusedContainerColor = Color.Transparent,
                disabledContainerColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent,
            )
        )

        ContentCard(
            content = uiState.content,
            colorPalette = colorPalette,
            isEnabled = uiState.isEnabled,
            sendFormUpdate = sendFormUpdate,
        )
    }
}

@Preview
@Composable
private fun NoteDetailsCardPreview(
    @PreviewParameter(NoteDetailsPreviewProvider::class)
    noteDetails: NotePreviewParameter
) {
    StickThoseNotesTheme {
        NoteDetailCard(
            uiState = CardUiState(
                isEnabled = noteDetails.isEnabled,
                title = noteDetails.title,
                content = noteDetails.content,
            ),
            colorPalette = noteDetails.colorPalette,
            cornerRadius = 12.dp,
            sendFormUpdate = {},
        )
    }
}
