/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.usecases

import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.notedetail.api.usecases.UpdatePinStateUseCase
import sg.acktor.android.stickthosenotes.notedetail.data.NoteDetailRepository
import java.time.Clock
import javax.inject.Inject

interface UpdateNoteUseCase : UpdatePinStateUseCase {
    suspend fun updateTitle(noteId: Long, title: String)

    suspend fun updateColorPalette(noteId: Long, colorPalette: ColorPalette)

    suspend fun updateLastUpdated(noteId: Long)
}

@BoundTo(UpdateNoteUseCase::class, ViewModelComponent::class)
@BoundTo(UpdatePinStateUseCase::class, SingletonComponent::class)
class UpdateNoteUseCaseImpl @Inject constructor(
    private val repo: NoteDetailRepository,
    private val clock: Clock,
) : UpdateNoteUseCase {
    override suspend fun updateTitle(noteId: Long, title: String) {
        repo.updateTitle(noteId, title, clock.millis())
    }

    override suspend fun updateColorPalette(noteId: Long, colorPalette: ColorPalette) {
        repo.updateColorPalette(noteId, colorPalette, clock.millis())
    }

    override suspend fun updatePinState(noteId: Long, isPinned: Boolean) {
        repo.updatePinState(noteId, isPinned, clock.millis())
    }

    override suspend fun updateLastUpdated(noteId: Long) {
        repo.updateLastUpdated(noteId, clock.millis())
    }
}
