/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.data.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import kotlinx.coroutines.flow.Flow
import sg.acktor.android.stickthosenotes.database.api.room.NotePositionDAO
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItem
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItemInsert
import sg.acktor.android.stickthosenotes.database.api.room.entities.EntityId
import sg.acktor.android.stickthosenotes.database.api.room.entities.Note
import sg.acktor.android.stickthosenotes.database.api.room.entities.NotePosition
import sg.acktor.android.stickthosenotes.database.api.room.entities.TextNote
import sg.acktor.android.stickthosenotes.notedetail.data.models.ChecklistRoom
import sg.acktor.android.stickthosenotes.notedetail.data.models.NoteRoom
import sg.acktor.android.stickthosenotes.notedetail.data.models.NoteStateUpdateRoomModel

@Dao
interface NoteDetailDAO : NotePositionDAO {
    ///////////////////////////////////////////////////////////////////////////
    // Base note details
    ///////////////////////////////////////////////////////////////////////////

    @Query("SELECT title, colorPalette, isPinned, lastUpdated, isDeleted FROM Note WHERE id = :noteId")
    suspend fun getNote(noteId: Long): NoteRoom

    @Query("SELECT isPinned, isDeleted, lastUpdated FROM Note WHERE id = :noteId")
    fun getNoteStateUpdates(noteId: Long): Flow<NoteStateUpdateRoomModel>

    @Update(entity = Note::class)
    suspend fun updateNoteTitle(note: NoteTitleUpdate)

    @Update(entity = Note::class)
    suspend fun updateNoteColorPalette(note: NoteColorPaletteUpdate)

    @Update(entity = Note::class)
    suspend fun updateNotePinState(note: NotePinStateUpdate)

    @Update(entity = Note::class)
    suspend fun updateLastUpdated(note: NoteLastUpdate)

    @Update(entity = Note::class)
    suspend fun updateDeleteStatus(note: NoteDeleteStatus)

    @Update(entity = Note::class)
    suspend fun updateNotePosition(note: NotePosition)

    @Delete(entity = Note::class)
    suspend fun permanentlyDeleteNote(note: EntityId)

    ///////////////////////////////////////////////////////////////////////////
    // Text content details
    ///////////////////////////////////////////////////////////////////////////

    @Query("SELECT content FROM TextNote WHERE id = :noteId")
    suspend fun getTextContent(noteId: Long): String

    @Update
    suspend fun updateTextContent(content: TextNote)

    ///////////////////////////////////////////////////////////////////////////
    // Checklist content details
    ///////////////////////////////////////////////////////////////////////////

    @Insert(entity = ChecklistItem::class)
    suspend fun addChecklistItem(checklistItem: ChecklistItemInsert)

    @Transaction
    @Query("SELECT id FROM ChecklistNote WHERE id = :noteId")
    suspend fun getChecklistItems(noteId: Long): ChecklistRoom

    @Update(entity = ChecklistItem::class)
    suspend fun updateChecklistContent(checklistItem: ChecklistContentUpdate)

    @Update(entity = ChecklistItem::class)
    suspend fun updateChecklistCheckedState(checklistItem: ChecklistCheckUpdate)

    @Delete(entity = ChecklistItem::class)
    suspend fun deleteChecklistItem(checklistItem: ChecklistItemDelete)

    @Query("SELECT MAX(id) + 1 FROM ChecklistItem WHERE noteId = :noteId")
    suspend fun generateChecklistItemId(noteId: Long): Long?
}
