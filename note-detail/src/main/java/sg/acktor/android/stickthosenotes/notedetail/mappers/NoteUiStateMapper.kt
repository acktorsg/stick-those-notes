/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.mappers

import sg.acktor.android.stickthosenotes.notedetail.models.ui.NoteScreenUiModel
import sg.acktor.android.stickthosenotes.notedetail.ui.CardUiState
import sg.acktor.android.stickthosenotes.notedetail.ui.FooterUiState
import sg.acktor.android.stickthosenotes.notedetail.ui.NoteUiState
import sg.acktor.android.stickthosenotes.notedetail.ui.TopBarUiState
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteContentUiModel
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteDetailUiModel
import javax.inject.Inject

class NoteUiStateMapper @Inject constructor() {
    fun map(
        noteDetail: NoteDetailUiModel,
        noteContent: NoteContentUiModel,
        screenState: NoteScreenUiModel,
    ) = NoteUiState(
        topBarUiState = mapTopBarState(noteDetail.isPinned, noteDetail.isDeleted),
        cardUiState = mapCardState(noteDetail.title, noteContent, noteDetail.isDeleted),
        footerUiState = FooterUiState(
            lastUpdated = noteDetail.lastUpdated,
            showColorPicker = !noteDetail.isDeleted,
        ),
        colorPalette = noteDetail.colorPalette,
        userMessage = screenState.userMessage,
        showNotificationRationale = screenState.showNotificationRationale,
        showPermanentDeleteConfirmation = screenState.showPermanentDeleteConfirmation,
    )

    private fun mapTopBarState(
        isPinned: Boolean,
        isNoteDeleted: Boolean,
    ) = TopBarUiState(
        isPinChecked = isPinned,
        isPinEnabled = !isNoteDeleted,
        showRestoreButton = isNoteDeleted,
    )

    private fun mapCardState(
        title: String,
        content: NoteContentUiModel,
        isNoteDeleted: Boolean,
    ) = CardUiState(
        isEnabled = !isNoteDeleted,
        title = title,
        content = content,
    )
}
