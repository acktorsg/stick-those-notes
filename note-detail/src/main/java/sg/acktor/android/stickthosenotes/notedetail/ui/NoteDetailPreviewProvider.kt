/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.ui

import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteContentUiModel

internal data class NotePreviewParameter(
    val colorPalette: ColorPalette,
    val title: String,
    val isEnabled: Boolean,
    val content: NoteContentUiModel,
)

internal class NoteDetailsPreviewProvider : PreviewParameterProvider<NotePreviewParameter> {
    override val values: Sequence<NotePreviewParameter>
        get() = sequenceOf(
            NotePreviewParameter(
                colorPalette = ColorPalette.CYAN,
                title = "This some long title and should probably get truncated at some point",
                isEnabled = true,
                content = NoteContentUiModel.Text("Content goes here"),
            ),
            NotePreviewParameter(
                colorPalette = ColorPalette.INDIGO,
                title = "Disabled card preview",
                isEnabled = false,
                content = NoteContentUiModel.Text(
                    """
                        Some really long text card content that should spawn enough new lines that
                        the content reaches the bottom of the note and spawns new lines at some
                        point
                    """.trimIndent().replace('\n', ' ')
                ),
            ),
            NotePreviewParameter(
                colorPalette = ColorPalette.BLUE,
                title = "Checklist card preview",
                isEnabled = true,
                content = NoteContentUiModel.Checklist(
                    listOf(
                        NoteContentUiModel.Checklist.Item(
                            0, "Some content 1", true,
                        ),
                        NoteContentUiModel.Checklist.Item(
                            1,
                            "A long checklist item entry that should spawn new lines at some point",
                            false,
                        ),
                    )
                ),
            ),
        )
}

internal class UserMessagePreviewProvider : PreviewParameterProvider<UserMessage> {
    override val values: Sequence<UserMessage>
        get() = sequenceOf(
            UserMessage(MessageType.ERROR, "Failed to load note details"),
            UserMessage(MessageType.NORMAL, "Note restored"),
            UserMessage(
                MessageType.ERROR,
                "Some error has happened in this one component in the app, so suck to be you I guess"
            ),
        )
}
