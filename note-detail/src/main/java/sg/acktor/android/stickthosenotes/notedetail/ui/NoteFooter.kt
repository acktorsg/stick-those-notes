/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Palette
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.tooling.preview.Preview
import sg.acktor.acktorsdk.ui.compose.Dimensions
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme

@Composable
internal fun NoteFooter(
    uiState: FooterUiState,
    showColorSelectionMenu: () -> Unit,
    closeNote: () -> Unit,
) {
    Row(
        Modifier
            .padding(horizontal = Dimensions.spacingDefault)
            .clickable(onClick = closeNote),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            uiState.lastUpdated,
            style = MaterialTheme.typography.bodySmall,
            fontStyle = FontStyle.Italic,
            color = Color.White
        )
        Spacer(Modifier.weight(1f))
        if (uiState.showColorPicker) {
            IconButton(
                onClick = showColorSelectionMenu,
                colors = IconButtonDefaults.iconButtonColors(
                    contentColor = Color.White
                )
            ) {
                Icon(Icons.Rounded.Palette, contentDescription = "Show colour options")
            }
        }
    }
}

@Preview
@Composable
private fun NoteFooterPreview() {
    StickThoseNotesTheme {
        NoteFooter(
            uiState = FooterUiState(
                lastUpdated = "Last updated on 23 December at 12:59 pm",
                showColorPicker = true,
            ),
            showColorSelectionMenu = {},
            closeNote = {}
        )
    }
}
