/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.usecases

import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import sg.acktor.android.stickthosenotes.notedetail.api.usecases.UpdateDeleteStateUseCase
import sg.acktor.android.stickthosenotes.notedetail.data.NoteDetailRepository
import javax.inject.Inject

interface DeleteNoteUseCase : UpdateDeleteStateUseCase {
    suspend fun permanentlyDelete(noteId: Long)
}

@BoundTo(DeleteNoteUseCase::class, ViewModelComponent::class)
@BoundTo(UpdateDeleteStateUseCase::class, SingletonComponent::class)
class DeleteNoteUseCaseImpl @Inject constructor(
    private val repo: NoteDetailRepository,
) : DeleteNoteUseCase {
    override suspend fun setDeleteState(noteId: Long, isDeleted: Boolean) {
        repo.setDeleteStatus(noteId, isDeleted)
    }

    override suspend fun permanentlyDelete(noteId: Long) {
        repo.permanentlyDeleteNote(noteId)
    }
}
