/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail

import android.content.Context
import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.filterNot
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.transformLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import sg.acktor.android.stickthosenotes.common.BuildDetailProvider
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.home.api.HomeMessage
import sg.acktor.android.stickthosenotes.home.api.HomeMessageAction
import sg.acktor.android.stickthosenotes.home.api.HomeMessageSender
import sg.acktor.android.stickthosenotes.notedetail.mappers.NoteUiStateMapper
import sg.acktor.android.stickthosenotes.notedetail.models.ui.NoteScreenUiModel
import sg.acktor.android.stickthosenotes.notedetail.ui.ChecklistFormUpdate
import sg.acktor.android.stickthosenotes.notedetail.ui.MessageType
import sg.acktor.android.stickthosenotes.notedetail.ui.NavParams
import sg.acktor.android.stickthosenotes.notedetail.ui.NoteDetailUiAction
import sg.acktor.android.stickthosenotes.notedetail.ui.NoteDetailUiEvent
import sg.acktor.android.stickthosenotes.notedetail.ui.NoteFormUpdate
import sg.acktor.android.stickthosenotes.notedetail.ui.UserMessage
import sg.acktor.android.stickthosenotes.notedetail.usecases.DeleteNoteUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.GetNoteUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.UpdateChecklistUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.UpdateNoteUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.UpdateTextContentUseCase
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteContentUiModel
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteDetailUiModel
import sg.acktor.android.stickthosenotes.pin.api.PinManager
import javax.inject.Inject
import sg.acktor.android.stickthosenotes.common.R as R_common

private const val DEBOUNCE_DURATION_MILLIS = 250L

@OptIn(FlowPreview::class)
@HiltViewModel
internal class NoteDetailViewModel @Inject constructor(
    @ApplicationContext
    private val context: Context,
    private val buildDetailProvider: BuildDetailProvider,
    private val uiStateMapper: NoteUiStateMapper,
    private val getNoteUseCase: GetNoteUseCase,
    private val pinManager: PinManager,
    private val updateNoteUseCase: UpdateNoteUseCase,
    private val updateTextContentUseCase: UpdateTextContentUseCase,
    private val updateChecklistUseCase: UpdateChecklistUseCase,
    private val deleteNoteUseCase: DeleteNoteUseCase,
    private val homeMessageSender: HomeMessageSender,
) : ViewModel() {
    private val _uiEventChannel = Channel<NoteDetailUiEvent>()
    val uiEventChannel = _uiEventChannel.receiveAsFlow()

    private val noteDetailState = MutableStateFlow<NoteDetailUiModel?>(null)
    private val noteContentState = MutableStateFlow<NoteContentUiModel?>(null)
    private val screenState = MutableStateFlow(NoteScreenUiModel())

    val uiState = combine(
        noteDetailState, noteContentState, screenState
    ) { noteDetailUiModel, noteContentUiModel, screenUiModel ->
        if (noteDetailUiModel != null && noteContentUiModel != null) {
            uiStateMapper.map(noteDetailUiModel, noteContentUiModel, screenUiModel)
        } else {
            null
        }
    }.stateIn(
        viewModelScope,
        SharingStarted.Lazily,
        null,
    )

    private val titleUpdates = Channel<String>()
    private val colorPaletteUpdates = Channel<ColorPalette>()
    private val pinUpdates = Channel<Boolean>()
    private val textContentUpdates = Channel<String>()
    private val checklistUpdates = Channel<ChecklistFormUpdate>()

    private lateinit var navParams: NavParams
    private val noteId get() = navParams.noteId
    private val contentType get() = navParams.contentType

    override fun onCleared() {
        titleUpdates.close()
        colorPaletteUpdates.close()
        pinUpdates.close()
        _uiEventChannel.close()
    }

    fun processUiAction(action: NoteDetailUiAction) {
        when (action) {
            is NoteFormUpdate -> processFormUpdate(action)
            NoteDetailUiAction.ClickDeleteButton -> performDelete()
            NoteDetailUiAction.DismissDeleteConfirmation -> dismissPermanentDeleteConfirmation()
            NoteDetailUiAction.DismissNotificationRationale -> dismissNotificationRationale()
            NoteDetailUiAction.ConfirmPermanentDelete -> permanentlyDeleteNote()
            NoteDetailUiAction.ClickRestoreButton -> restoreNote()
            NoteDetailUiAction.DismissUserMessage -> dismissUserMessage()
            NoteDetailUiAction.CloseNote -> closeNote()
            is NoteDetailUiAction.NotificationPermissionRequested -> {
                processNotificationPermissionRequest(action.isGranted)
            }
        }
    }

    fun loadNote(navParams: NavParams) {
        this.navParams = navParams

        viewModelScope.launch(Dispatchers.IO + CoroutineExceptionHandler { _, e ->
            e.printStackTrace()

            homeMessageSender.sendMessage(
                HomeMessage(context.getString(R.string.message_error_loadNoteDetails))
            )
            viewModelScope.launch {
                _uiEventChannel.send(NoteDetailUiEvent.ReturnToPreviousScreen)
            }
        }) {
            val details = getNoteUseCase.getDetails(noteId)
            val content = getNoteUseCase.getContent(noteId, contentType)

            onNoteRetrieved(details, content)
        }
    }

    private fun processFormUpdate(update: NoteFormUpdate) {
        when (update) {
            is NoteFormUpdate.Title -> updateTitle(update.newTitle)
            is NoteFormUpdate.Color -> updateColorPalette(update.newPalette)
            is NoteFormUpdate.Pin -> updatePinnedState(update.newPinState)
            is NoteFormUpdate.TextContent -> updateTextContent(update.newContent)
            is ChecklistFormUpdate -> processChecklistUpdate(update)
        }
    }

    private fun processChecklistUpdate(update: ChecklistFormUpdate) {
        val noteContent = (noteContentState.value as? NoteContentUiModel.Checklist)
            ?: return
        val checklistItems = noteContent.items.toMutableList()

        viewModelScope.launch(Dispatchers.IO) {
            when (update) {
                is ChecklistFormUpdate.Content -> {
                    val checklistItem = checklistItems[update.pos]
                    checklistItem.content = update.content
                }

                is ChecklistFormUpdate.CheckState -> {
                    val checklistItem = checklistItems[update.pos]
                    checklistItem.isChecked = update.isChecked
                }

                is ChecklistFormUpdate.Delete -> {
                    checklistItems.removeAt(update.pos)
                }

                is ChecklistFormUpdate.NewItem -> {
                    val itemId = updateChecklistUseCase.addChecklistItem(noteId)
                    checklistItems.add(NoteContentUiModel.Checklist.Item(itemId))
                }
            }

            noteContentState.update { NoteContentUiModel.Checklist(checklistItems) }
            checklistUpdates.send(update)
        }
    }

    private fun updateTitle(title: String) = viewModelScope.launch {
        noteDetailState.update { it?.copy(title = title) }
        titleUpdates.send(title)
    }

    private fun updateColorPalette(colorPalette: ColorPalette) = viewModelScope.launch {
        noteDetailState.update { it?.copy(colorPalette = colorPalette) }
        colorPaletteUpdates.send(colorPalette)
    }

    private fun updatePinnedState(isPinned: Boolean, requirePermissionRequest: Boolean = true) {
        if (isPinned) {
            val deviceApiVersion = buildDetailProvider.getDeviceApiVersion()
            if (deviceApiVersion >= Build.VERSION_CODES.TIRAMISU && requirePermissionRequest) {
                viewModelScope.launch {
                    _uiEventChannel.send(NoteDetailUiEvent.RequestNotificationPermission)
                }

                return
            }

            if (!pinManager.isSystemPinningEnabled()) {
                displayNotificationRationale()
                return
            }
        }

        val noteDetails = noteDetailState.value ?: return
        val noteContent = noteContentState.value ?: return

        viewModelScope.launch(Dispatchers.Default) {
            val isTitleBlank = noteDetails.title.isBlank()
            val isContentBlank = when (noteContent) {
                is NoteContentUiModel.Text -> {
                    noteContent.content.isBlank()
                }

                is NoteContentUiModel.Checklist -> {
                    val items = noteContent.items
                    items.isEmpty() || items.all { it.content.isBlank() }
                }
            }
            if (isPinned && isTitleBlank && isContentBlank) {
                screenState.update {
                    it.copy(
                        userMessage = UserMessage(
                            MessageType.ERROR,
                            context.getString(R.string.message_error_pinEmptyNote),
                        )
                    )
                }
            } else {
                noteDetailState.update { noteDetails.copy(isPinned = isPinned) }
                pinUpdates.send(isPinned)
            }
        }
    }

    private fun processNotificationPermissionRequest(isGranted: Boolean) {
        if (!isGranted) {
            displayNotificationRationale()
            return
        }

        updatePinnedState(true, requirePermissionRequest = false)
    }

    private fun displayNotificationRationale() {
        screenState.update { it.copy(showNotificationRationale = true) }
    }

    private fun updateTextContent(content: String) {
        noteContentState.update { NoteContentUiModel.Text(content) }
        viewModelScope.launch {
            textContentUpdates.send(content)
        }
    }

    private fun performDelete() {
        val isDeleted = noteDetailState.value?.isDeleted ?: return
        if (isDeleted) {
            screenState.update { it.copy(showPermanentDeleteConfirmation = true) }
        } else {
            viewModelScope.launch { deleteNote() }
        }
    }

    private suspend fun deleteNote() {
        withContext(Dispatchers.IO) {
            deleteNoteUseCase.setDeleteState(noteId, true)
            pinManager.unpinNote(noteId)

            homeMessageSender.sendMessage(
                HomeMessage(
                    message = context.getString(R_common.string.message_success_deleteNote),
                    actionLabel = context.getString(R.string.title_button_undo),
                    action = HomeMessageAction.RestoreNote(noteId),
                )
            )

            _uiEventChannel.send(NoteDetailUiEvent.ReturnToPreviousScreen)
        }
    }

    private fun dismissPermanentDeleteConfirmation() {
        screenState.update { it.copy(showPermanentDeleteConfirmation = false) }
    }

    private fun dismissNotificationRationale() {
        screenState.update { it.copy(showNotificationRationale = false) }
    }

    private fun permanentlyDeleteNote() {
        viewModelScope.launch(Dispatchers.IO + CoroutineExceptionHandler { _, throwable ->
            throwable.printStackTrace()
            screenState.update {
                it.copy(
                    userMessage = UserMessage(
                        MessageType.ERROR,
                        context.getString(R.string.message_error_permanentlyDeleteNote),
                    )
                )
            }
        }) {
            deleteNoteUseCase.permanentlyDelete(noteId)
            _uiEventChannel.send(NoteDetailUiEvent.ReturnToPreviousScreen)
        }
    }

    private fun restoreNote() {
        viewModelScope.launch(Dispatchers.IO + CoroutineExceptionHandler { _, throwable ->
            throwable.printStackTrace()
            screenState.update {
                it.copy(
                    userMessage = UserMessage(
                        MessageType.ERROR,
                        context.getString(R.string.message_error_restoreNote),
                    )
                )
            }
        }) {
            deleteNoteUseCase.setDeleteState(noteId, false)
            pinManager.pinNote(noteId)
            homeMessageSender.sendMessage(
                HomeMessage(context.getString(R.string.message_success_restoreNote))
            )
        }
    }

    private fun dismissUserMessage() {
        screenState.update { it.copy(userMessage = null) }
    }

    private fun closeNote() {
        viewModelScope.launch {
            _uiEventChannel.send(NoteDetailUiEvent.ReturnToPreviousScreen)
        }
    }

    private var currentChecklistTextUpdate: ChecklistFormUpdate.Content? = null

    @OptIn(ExperimentalCoroutinesApi::class)
    private fun onNoteRetrieved(details: NoteDetailUiModel, content: NoteContentUiModel) {
        noteDetailState.update { details }
        noteContentState.update { content }

        titleUpdates.collectUpdates { title ->
            updateNoteUseCase.updateTitle(noteId, title)
            pinManager.pinNote(noteId)
        }
        colorPaletteUpdates.collectUpdates { colorPalette ->
            updateNoteUseCase.updateColorPalette(noteId, colorPalette)
            pinManager.pinNote(noteId)
        }

        pinUpdates.collectUpdates { isPinned ->
            val noteDetails = noteDetailState.value ?: return@collectUpdates

            if (isPinned != noteDetails.isPinned) {
                noteDetailState.update { noteDetails.copy(isPinned = isPinned) }

                if (isPinned) {
                    pinManager.pinNote(noteId)
                } else {
                    pinManager.unpinNote(noteId)
                }
            } else {
                updateNoteUseCase.updatePinState(noteId, isPinned)
                if (isPinned) {
                    pinManager.pinNote(noteId)
                } else {
                    pinManager.unpinNote(noteId)
                }
            }
        }

        textContentUpdates.collectUpdates { textContent ->
            updateTextContentUseCase.run(noteId, textContent)
            updateNoteUseCase.updateLastUpdated(noteId)
            pinManager.pinNote(noteId)
        }

        checklistUpdates.receiveAsFlow()
            .filterNot { it == ChecklistFormUpdate.NewItem }
            .transformLatest { update ->
                if (update is ChecklistFormUpdate.Content) {
                    if (currentChecklistTextUpdate?.pos != update.pos) {
                        currentChecklistTextUpdate?.let { emit(it) }
                    }

                    currentChecklistTextUpdate = update
                    delay(DEBOUNCE_DURATION_MILLIS)
                    emit(update)
                } else {
                    if (
                        update !is ChecklistFormUpdate.Delete ||
                        update.id != currentChecklistTextUpdate?.id
                    ) {
                        currentChecklistTextUpdate?.let { emit(it) }
                        currentChecklistTextUpdate = null
                    }

                    emit(update)
                }
            }
            .buffer()
            .onEach { checklistUpdate ->
                when (checklistUpdate) {
                    is ChecklistFormUpdate.Content -> {
                        updateChecklistUseCase.updateContent(
                            noteId, checklistUpdate.id, checklistUpdate.content,
                        )
                    }

                    is ChecklistFormUpdate.CheckState -> {
                        updateChecklistUseCase.updateCheckedState(
                            noteId, checklistUpdate.id, checklistUpdate.isChecked
                        )
                    }

                    is ChecklistFormUpdate.Delete -> {
                        updateChecklistUseCase.deleteChecklistItem(noteId, checklistUpdate.id)
                    }

                    is ChecklistFormUpdate.NewItem -> {}
                }

                updateNoteUseCase.updateLastUpdated(noteId)
                pinManager.pinNote(noteId)
            }
            .flowOn(Dispatchers.IO)
            .launchIn(viewModelScope)

        getNoteUseCase.getNoteStateUpdates(noteId)
            .catch { it.printStackTrace() }
            .onEach { (lastUpdated, isPinned, isDeleted) ->
                val noteDetails = noteDetailState.value ?: return@onEach

                noteDetailState.update { noteDetails.copy(lastUpdated = lastUpdated) }

                if (isPinned != noteDetails.isPinned) {
                    pinUpdates.send(isPinned)
                }

                if (isDeleted != noteDetails.isDeleted) {
                    _uiEventChannel.send(NoteDetailUiEvent.ReturnToPreviousScreen)
                }
            }
            .launchIn(viewModelScope)
    }

    private fun <T> Channel<T>.collectUpdates(onEach: suspend (T) -> Unit) = receiveAsFlow()
        .debounce(DEBOUNCE_DURATION_MILLIS)
        .onEach(onEach)
        .flowOn(Dispatchers.IO)
        .launchIn(viewModelScope)
}
