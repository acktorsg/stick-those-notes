/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.mappers

import java.time.Clock
import java.time.Instant
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject

class LastUpdatedTextMapper @Inject constructor(private val clock: Clock) {
    private val formatters = listOf(
            DateTimeFormatter.ofPattern("s 'second ago'"),
            DateTimeFormatter.ofPattern("s 'seconds ago'"),
            DateTimeFormatter.ofPattern("m 'minute ago'"),
            DateTimeFormatter.ofPattern("m 'minutes ago'"),
            DateTimeFormatter.ofPattern("'on' d MMMM 'at' h:mm a"),
    )

    fun map(timeInMillis: Long): String {
        val (formatter, timeToFormat) = when (val duration = clock.millis() - timeInMillis) {
            // 1 second
            1 * 1000L -> formatters[0] to duration
            // < 1 minute excluding 1 second
            in 0..(59 * 1000L) -> formatters[1] to duration
            // 1 minute to 1 minute 59 seconds
            in (1 * 60 * 1000L)..<(2 * 60 * 1000L) -> formatters[2] to duration
            // < 1 hour starting from 2 minutes
            in (2 * 60 * 1000L)..(59 * 60 * 1000L) -> formatters[3] to duration

            else -> formatters[4] to timeInMillis
        }

        return LocalDateTime.ofInstant(Instant.ofEpochMilli(timeToFormat), clock.zone)
                .format(formatter)
    }
}
