/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.usecases

import dagger.hilt.android.components.ViewModelComponent
import it.czerwinski.android.hilt.annotations.Bound
import sg.acktor.android.stickthosenotes.notedetail.data.NoteDetailRepository
import javax.inject.Inject

interface UpdateChecklistUseCase {
    suspend fun updateContent(noteId: Long, itemId: Long, content: String)

    suspend fun updateCheckedState(noteId: Long, itemId: Long, isChecked: Boolean)

    suspend fun addChecklistItem(noteId: Long): Long

    suspend fun deleteChecklistItem(noteId: Long, itemId: Long)
}

@Bound(ViewModelComponent::class)
class UpdateChecklistUseCaseImpl @Inject constructor(
        private val repo: NoteDetailRepository
) : UpdateChecklistUseCase {
    override suspend fun updateContent(
            noteId: Long, itemId: Long, content: String
    ) = repo.updateChecklistItemContent(itemId, noteId, content)

    override suspend fun updateCheckedState(
            noteId: Long, itemId: Long, isChecked: Boolean
    ) = repo.updateChecklistItemCheckedState(itemId, noteId, isChecked)

    override suspend fun addChecklistItem(noteId: Long) = repo.addChecklistItem(noteId)

    override suspend fun deleteChecklistItem(
            noteId: Long, itemId: Long
    ) = repo.deleteChecklistItem(itemId, noteId)
}
