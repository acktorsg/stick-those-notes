/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.ui

import sg.acktor.android.stickthosenotes.common.models.ColorPalette

sealed interface NoteDetailUiAction {
    data object ClickDeleteButton : NoteDetailUiAction

    data object DismissDeleteConfirmation : NoteDetailUiAction

    data object DismissNotificationRationale: NoteDetailUiAction

    data object ConfirmPermanentDelete : NoteDetailUiAction

    data object ClickRestoreButton : NoteDetailUiAction

    data object DismissUserMessage : NoteDetailUiAction

    data object CloseNote : NoteDetailUiAction

    data class NotificationPermissionRequested(val isGranted: Boolean) : NoteDetailUiAction
}

sealed interface NoteFormUpdate : NoteDetailUiAction {
    data class Title(val newTitle: String) : NoteFormUpdate

    data class Color(val newPalette: ColorPalette) : NoteFormUpdate

    data class Pin(val newPinState: Boolean) : NoteFormUpdate

    data class TextContent(val newContent: String) : NoteFormUpdate
}

sealed interface ChecklistFormUpdate : NoteFormUpdate {
    data class Content(val id: Long, val pos: Int, val content: String) : ChecklistFormUpdate

    class CheckState(val id: Long, val pos: Int, val isChecked: Boolean) : ChecklistFormUpdate

    class Delete(val id: Long, val pos: Int) : ChecklistFormUpdate

    data object NewItem : ChecklistFormUpdate
}

sealed interface NoteDetailUiEvent {
    data object ReturnToPreviousScreen : NoteDetailUiEvent

    data object RequestNotificationPermission : NoteDetailUiEvent
}
