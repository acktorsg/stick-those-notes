/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Close
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import sg.acktor.acktorsdk.ui.compose.Dimensions
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme

@Composable
internal fun UserMessageBar(
    modifier: Modifier = Modifier,
    userMessage: UserMessage,
    containerShape: Shape,
    containerColor: Color,
    clearMessage: () -> Unit,
) {
    val contentColor = when (userMessage.type) {
        MessageType.NORMAL -> {
            MaterialTheme.colorScheme.onPrimaryContainer
        }

        MessageType.ERROR -> {
            MaterialTheme.colorScheme.onErrorContainer
        }
    }

    Card(
        modifier = modifier,
        shape = containerShape,
        colors = CardDefaults.cardColors(
            containerColor = containerColor,
            contentColor = contentColor,
        ),
    ) {
        Column {
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Spacer(Modifier.width(Dimensions.spacingDefault))
                Text(
                    modifier = Modifier.weight(1f),
                    text = userMessage.text,
                    style = MaterialTheme.typography.bodySmall,
                )
                Spacer(Modifier.width(Dimensions.spacingSmall))
                IconButton(onClick = clearMessage) {
                    Icon(Icons.Rounded.Close, contentDescription = "Close message")
                }
            }
        }
    }
}

@Preview
@Composable
private fun UserMessagePreview(
    @PreviewParameter(UserMessagePreviewProvider::class)
    message: UserMessage
) {
    StickThoseNotesTheme {
        val containerColor = when (message.type) {
            MessageType.NORMAL -> MaterialTheme.colorScheme.primaryContainer
            MessageType.ERROR -> MaterialTheme.colorScheme.errorContainer
        }

        UserMessageBar(
            userMessage = message,
            containerShape = RoundedCornerShape(
                bottomStart = 12.dp,
                bottomEnd = 12.dp,
            ),
            containerColor = containerColor,
            clearMessage = {},
        )
    }
}
