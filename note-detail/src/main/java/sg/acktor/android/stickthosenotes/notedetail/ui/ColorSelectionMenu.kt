/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Check
import androidx.compose.material.icons.rounded.ColorLens
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import sg.acktor.acktorsdk.ui.compose.Dimensions
import sg.acktor.android.stickthosenotes.common.LocalNoteThemes
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.notedetail.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
internal fun ColorSelectionMenu(
    currentPalette: ColorPalette,
    selectColor: (ColorPalette) -> Unit,
    dismissMenu: () -> Unit,
) {
    BasicAlertDialog(
        onDismissRequest = dismissMenu,
    ) {
        Surface(
            shape = RoundedCornerShape(28.dp),
            color = MaterialTheme.colorScheme.surfaceContainerHigh,
        ) {
            Column(
                modifier = Modifier.padding(24.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                val themes = LocalNoteThemes.current

                Icon(
                    Icons.Rounded.ColorLens,
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.secondary,
                )
                Spacer(Modifier.height(Dimensions.spacingDefault))
                Text(
                    text = stringResource(R.string.title_dialog_colorPicker),
                    style = MaterialTheme.typography.headlineSmall,
                    color = MaterialTheme.colorScheme.onSurface,
                )
                Spacer(Modifier.height(Dimensions.spacingDefault))
                LazyVerticalGrid(
                    modifier = Modifier.weight(1f, fill = false),
                    columns = GridCells.Adaptive(56.dp),
                    horizontalArrangement = Arrangement.spacedBy(
                        Dimensions.spacingSmall,
                        alignment = Alignment.CenterHorizontally,
                    ),
                    verticalArrangement = Arrangement.spacedBy(Dimensions.spacingSmall),
                ) {
                    items(ColorPalette.entries, key = { it }) { colorOption ->
                        val theme = themes[colorOption]
                        val isSelectedPalette = colorOption == currentPalette

                        Box(
                            Modifier
                                .aspectRatio(1f)
                                .fillMaxWidth()
                                .border(2.dp, theme.darkBackground, CircleShape)
                                .background(theme.lightBackground, CircleShape)
                                .clickable(enabled = !isSelectedPalette) {
                                    selectColor(colorOption)
                                }
                        ) {
                            if (isSelectedPalette) {
                                Icon(
                                    Icons.Rounded.Check,
                                    contentDescription = "Color is selected",
                                    modifier = Modifier
                                        .size(28.dp)
                                        .align(Alignment.Center),
                                    tint = theme.onLightBackground,
                                )
                            }
                        }
                    }
                }
                Spacer(Modifier.height(24.dp))
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.End,
                ) {
                    TextButton(
                        onClick = dismissMenu,
                    ) {
                        Text(stringResource(R.string.title_button_close))
                    }
                }
            }
        }
    }
}

@Preview
@Composable
private fun ColorSelectionMenuPreview() {
    StickThoseNotesTheme {
        ColorSelectionMenu(
            currentPalette = ColorPalette.DEEP_ORANGE,
            selectColor = {},
            dismissMenu = {},
        )
    }
}
