/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.usecases

import dagger.hilt.android.components.ViewModelComponent
import it.czerwinski.android.hilt.annotations.Bound
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.notedetail.data.NoteDetailRepository
import sg.acktor.android.stickthosenotes.notedetail.mappers.LastUpdatedTextMapper
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteContentUiModel
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteDetailUiModel
import sg.acktor.android.stickthosenotes.notedetail.usecases.models.NoteStateUpdate
import javax.inject.Inject

interface GetNoteUseCase {
    suspend fun getDetails(noteId: Long): NoteDetailUiModel

    suspend fun getContent(noteId: Long, contentType: ContentType): NoteContentUiModel

    fun getNoteStateUpdates(noteId: Long): Flow<NoteStateUpdate>
}

@Bound(ViewModelComponent::class)
class GetNoteUseCaseImpl @Inject constructor(
    private val repo: NoteDetailRepository,
    private val lastUpdatedTextMapper: LastUpdatedTextMapper,
) : GetNoteUseCase {
    override suspend fun getDetails(noteId: Long): NoteDetailUiModel {
        val note = repo.getNote(noteId)
        val lastUpdatedText = lastUpdatedTextMapper.map(note.lastUpdated)

        return NoteDetailUiModel(
            note.title,
            note.colorPalette,
            note.isPinned,
            lastUpdatedText,
            note.isDeleted,
        )
    }

    override suspend fun getContent(noteId: Long, contentType: ContentType): NoteContentUiModel {
        return when (contentType) {
            ContentType.TEXT -> {
                val textContent = repo.getTextContent(noteId)
                NoteContentUiModel.Text(textContent.content)
            }

            ContentType.CHECKLIST -> {
                val checklistContent = repo.getChecklistContent(noteId)
                NoteContentUiModel.Checklist(
                    checklistContent.items.map {
                        NoteContentUiModel.Checklist.Item(it.id, it.content, it.isChecked)
                    },
                )
            }
        }
    }

    override fun getNoteStateUpdates(noteId: Long) = repo.getNoteDetailUpdates(noteId)
        .map { (lastUpdated, isPinned, isDeleted) ->
            NoteStateUpdate(lastUpdatedTextMapper.map(lastUpdated), isPinned, isDeleted)
        }
}
