/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.ui

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.notedetail.R

@Composable
internal fun PermanentDeleteDialog(
    permanentlyDeleteNote: () -> Unit,
    dismissDialog: () -> Unit
) {
    AlertDialog(
        onDismissRequest = dismissDialog,
        title = {
            Text(stringResource(R.string.title_dialog_permanentDeleteNote))
        },
        text = {
            Text(stringResource(R.string.message_dialog_permanentlyDeleteNote))
        },
        dismissButton = {
            TextButton(onClick = dismissDialog) {
                Text(
                    stringResource(
                        sg.acktor.android.stickthosenotes.common.R.string.title_button_cancel
                    )
                )
            }
        },
        confirmButton = {
            TextButton(onClick = permanentlyDeleteNote) {
                Text(stringResource(R.string.title_button_deleteForever))
            }
        }
    )
}

@Composable
internal fun NotifRationaleDialog(
    openSettingsApp: () -> Unit,
    dismissDialog: () -> Unit,
) {
    AlertDialog(
        onDismissRequest = dismissDialog,
        text = {
            Text(stringResource(R.string.message_dialog_notifRationale))
        },
        dismissButton = {
            TextButton(onClick = dismissDialog) {
                Text(stringResource(R.string.title_button_denyPermission))
            }
        },
        confirmButton = {
            TextButton(onClick = openSettingsApp) {
                Text(stringResource(R.string.title_button_askAgain))
            }
        }
    )
}

@Preview
@Composable
private fun PermanentDeleteDialogPreview() {
    StickThoseNotesTheme {
        PermanentDeleteDialog(
            permanentlyDeleteNote = {},
            dismissDialog = {}
        )
    }
}

@Preview
@Composable
private fun NotifRationaleDialogPreview() {
    StickThoseNotesTheme {
        NotifRationaleDialog(
            openSettingsApp = {},
            dismissDialog = {}
        )
    }
}
