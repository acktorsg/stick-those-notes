/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.ui

import android.view.Gravity
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.window.DialogProperties
import androidx.compose.ui.window.DialogWindowProvider
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.dialog
import androidx.navigation.toRoute
import androidx.window.core.layout.WindowWidthSizeClass
import kotlinx.serialization.Serializable
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.common.ui.EntryPoint
import sg.acktor.android.stickthosenotes.common.ui.WindowSizeClassProvider
import javax.inject.Inject

@Serializable
data class NoteDetailDestination(
    val noteId: Long,
    val contentType: ContentType,
)

internal data class NavParams(
    val noteId: Long,
    val contentType: ContentType,
)

internal class NoteDetailEntryPoint @Inject constructor(
    private val windowSizeClassProvider: WindowSizeClassProvider,
) : EntryPoint {
    private val isTablet
        get(): Boolean {
            val windowSizeClass = windowSizeClassProvider.sizeClass
            return windowSizeClass.windowWidthSizeClass != WindowWidthSizeClass.COMPACT
        }

    override fun createDestination(navGraphBuilder: NavGraphBuilder) {
        navGraphBuilder.dialog<NoteDetailDestination>(
            dialogProperties = DialogProperties(
                usePlatformDefaultWidth = isTablet,
                decorFitsSystemWindows = false,
            )
        ) {
            if (!isTablet) {
                val dialogWindowProvider = LocalView.current.parent as DialogWindowProvider
                dialogWindowProvider.window.setGravity(Gravity.BOTTOM)
            }

            val destinationRoute = it.toRoute<NoteDetailDestination>()
            NoteDetailsScreen(
                navParams = NavParams(destinationRoute.noteId, destinationRoute.contentType),
            )
        }
    }
}
