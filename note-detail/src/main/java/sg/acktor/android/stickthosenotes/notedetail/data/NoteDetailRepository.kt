/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.data

import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItemInsert
import sg.acktor.android.stickthosenotes.database.api.room.entities.EntityId
import sg.acktor.android.stickthosenotes.database.api.room.entities.NotePosition
import sg.acktor.android.stickthosenotes.database.api.room.entities.TextNote
import sg.acktor.android.stickthosenotes.notedetail.data.models.ContentApiModel
import sg.acktor.android.stickthosenotes.notedetail.data.models.NoteApiModel
import sg.acktor.android.stickthosenotes.notedetail.data.models.NoteStateUpdateApiModel
import sg.acktor.android.stickthosenotes.notedetail.data.room.ChecklistCheckUpdate
import sg.acktor.android.stickthosenotes.notedetail.data.room.ChecklistContentUpdate
import sg.acktor.android.stickthosenotes.notedetail.data.room.ChecklistItemDelete
import sg.acktor.android.stickthosenotes.notedetail.data.room.NoteColorPaletteUpdate
import sg.acktor.android.stickthosenotes.notedetail.data.room.NoteDeleteStatus
import sg.acktor.android.stickthosenotes.notedetail.data.room.NoteDetailDAO
import sg.acktor.android.stickthosenotes.notedetail.data.room.NoteLastUpdate
import sg.acktor.android.stickthosenotes.notedetail.data.room.NotePinStateUpdate
import sg.acktor.android.stickthosenotes.notedetail.data.room.NoteTitleUpdate
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NoteDetailRepository @Inject constructor(
    private val noteDetailDAO: NoteDetailDAO,
) {
    suspend fun getNote(noteId: Long): NoteApiModel {
        val note = noteDetailDAO.getNote(noteId)
        return NoteApiModel(
            note.title, note.colorPalette, note.isPinned, note.lastUpdated, note.isDeleted
        )
    }

    fun getNoteDetailUpdates(noteId: Long) = noteDetailDAO.getNoteStateUpdates(noteId)
        .distinctUntilChanged()
        .map { NoteStateUpdateApiModel(it.lastUpdated, it.isPinned, it.isDeleted) }

    suspend fun getTextContent(noteId: Long): ContentApiModel.Text {
        val content = noteDetailDAO.getTextContent(noteId)
        return ContentApiModel.Text(content)
    }

    suspend fun getChecklistContent(noteId: Long): ContentApiModel.Checklist {
        val checklist = noteDetailDAO.getChecklistItems(noteId)
        return ContentApiModel.Checklist(
            checklist.items.map { ContentApiModel.Checklist.Item(it.id, it.content, it.isChecked) }
        )
    }

    suspend fun updateTextContent(
        noteId: Long, content: String
    ) = noteDetailDAO.updateTextContent(TextNote(noteId, content))

    suspend fun updateChecklistItemContent(
        itemId: Long, noteId: Long, content: String,
    ) = noteDetailDAO.updateChecklistContent(ChecklistContentUpdate(itemId, noteId, content))

    suspend fun updateChecklistItemCheckedState(
        itemId: Long, noteId: Long, isChecked: Boolean,
    ) = noteDetailDAO.updateChecklistCheckedState(ChecklistCheckUpdate(itemId, noteId, isChecked))

    suspend fun addChecklistItem(noteId: Long): Long {
        val itemId = noteDetailDAO.generateChecklistItemId(noteId) ?: 1
        noteDetailDAO.addChecklistItem(ChecklistItemInsert(itemId, noteId))
        return itemId
    }

    suspend fun deleteChecklistItem(itemId: Long, noteId: Long) {
        noteDetailDAO.deleteChecklistItem(ChecklistItemDelete(itemId, noteId))
    }

    suspend fun setDeleteStatus(noteId: Long, isDeleted: Boolean) {
        noteDetailDAO.updateDeleteStatus(NoteDeleteStatus(noteId, isDeleted))

        if (!isDeleted) {
            val pos = noteDetailDAO.generateNextPosition() ?: 1
            noteDetailDAO.updateNotePosition(NotePosition(noteId, pos))
        }
    }

    suspend fun permanentlyDeleteNote(noteId: Long) {
        noteDetailDAO.permanentlyDeleteNote(EntityId(noteId))
    }

    suspend fun updateTitle(noteId: Long, title: String, lastUpdated: Long) {
        noteDetailDAO.updateNoteTitle(NoteTitleUpdate(noteId, title, lastUpdated))
    }

    suspend fun updateColorPalette(noteId: Long, colorPalette: ColorPalette, lastUpdated: Long) {
        noteDetailDAO.updateNoteColorPalette(
            NoteColorPaletteUpdate(noteId, colorPalette, lastUpdated)
        )
    }

    suspend fun updatePinState(noteId: Long, isPinned: Boolean, lastUpdated: Long) {
        noteDetailDAO.updateNotePinState(NotePinStateUpdate(noteId, isPinned, lastUpdated))
    }

    suspend fun updateLastUpdated(noteId: Long, lastUpdated: Long) {
        noteDetailDAO.updateLastUpdated(NoteLastUpdate(noteId, lastUpdated))
    }
}
