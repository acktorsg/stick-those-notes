/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notedetail.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Close
import androidx.compose.material.icons.rounded.Delete
import androidx.compose.material.icons.rounded.Restore
import androidx.compose.material3.FilledTonalIconButton
import androidx.compose.material3.FilledTonalIconToggleButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.notedetail.R
import sg.acktor.android.stickthosenotes.pin.api.R as R_pin

@Composable
internal fun NoteTopBar(
    uiState: TopBarUiState,
    sendUiAction: (NoteDetailUiAction) -> Unit,
) {
    Row(
        Modifier.clickable(
            // Removes ripple effect when clicking on row
            interactionSource = remember { MutableInteractionSource() },
            indication = null,
            onClick = { sendUiAction(NoteDetailUiAction.CloseNote) },
        )
    ) {
        FilledTonalIconButton(onClick = { sendUiAction(NoteDetailUiAction.ClickDeleteButton) }) {
            Icon(Icons.Rounded.Delete, contentDescription = "Delete note")
        }
        if (uiState.showRestoreButton) {
            FilledTonalIconButton(
                onClick = { sendUiAction(NoteDetailUiAction.ClickRestoreButton) }
            ) {
                Icon(Icons.Rounded.Restore, contentDescription = "Restore note")
            }
        }
        Spacer(Modifier.weight(1f))
        PinButton(
            isChecked = uiState.isPinChecked,
            isEnabled = uiState.isPinEnabled,
            sendUiAction = sendUiAction,
        )
        FilledTonalIconButton(onClick = { sendUiAction(NoteDetailUiAction.CloseNote) }) {
            Icon(Icons.Rounded.Close, contentDescription = "Close note")
        }
    }
}

@Composable
private fun PinButton(
    isChecked: Boolean,
    isEnabled: Boolean,
    sendUiAction: (NoteDetailUiAction) -> Unit,
) {
    FilledTonalIconToggleButton(
        checked = isChecked,
        onCheckedChange = { isChecked -> sendUiAction(NoteFormUpdate.Pin(isChecked)) },
        enabled = isEnabled,
        colors = IconButtonDefaults.filledTonalIconToggleButtonColors(
            containerColor = MaterialTheme.colorScheme.secondaryContainer,
            checkedContainerColor = MaterialTheme.colorScheme.secondaryContainer,
            disabledContainerColor = MaterialTheme.colorScheme.secondaryContainer,
            contentColor = MaterialTheme.colorScheme.onSecondaryContainer,
            checkedContentColor = MaterialTheme.colorScheme.onSecondaryContainer,
            disabledContentColor = MaterialTheme.colorScheme.onSecondaryContainer,
        )
    ) {
        Icon(
            painterResource(
                if (isChecked) R.drawable.ic_pin else R_pin.drawable.ic_unpin
            ),
            contentDescription = "Note is ${if (!isChecked) "not " else ""} pinned"
        )
    }
}

@Preview
@Composable
private fun TopBarPreview() {
    StickThoseNotesTheme {
        NoteTopBar(
            uiState = TopBarUiState(
                isPinChecked = false,
                isPinEnabled = true,
                showRestoreButton = true,
            ),
            sendUiAction = {},
        )
    }
}
