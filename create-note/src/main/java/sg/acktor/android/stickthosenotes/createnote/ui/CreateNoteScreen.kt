/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.createnote.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.Subject
import androidx.compose.material.icons.rounded.CheckBox
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Text
import androidx.compose.material3.adaptive.currentWindowAdaptiveInfo
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.movableContentOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.window.core.layout.WindowWidthSizeClass
import sg.acktor.acktorsdk.ui.compose.Dimensions
import sg.acktor.android.stickthosenotes.common.LocalRootNavigator
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.createnote.CreateNoteViewModel
import sg.acktor.android.stickthosenotes.createnote.R

@Composable
fun CreateNoteScreen(createNoteViewModel: CreateNoteViewModel = hiltViewModel()) {
    val rootNavigator = LocalRootNavigator.current

    val uiEvent by createNoteViewModel.uiEventState.collectAsStateWithLifecycle(null)
    LaunchedEffect(uiEvent) {
        when (val event = uiEvent) {
            is CreateNoteUiEvent.ShowNoteDetails -> {
                rootNavigator.showNoteDetailsScreen(event.id, event.contentType, true)
            }

            CreateNoteUiEvent.ReturnToPreviousScreen -> {
                rootNavigator.returnToHomeScreen()
            }

            else -> {}
        }
    }

    val content = remember(createNoteViewModel) {
        movableContentOf {
            CreateNoteMenu(
                modifier = Modifier.padding(Dimensions.spacingDefault),
                createNote = {
                    createNoteViewModel.processUiAction(CreateNoteUiAction.NoteTypeSelected(it))
                },
            )
        }
    }

    val widthClass = currentWindowAdaptiveInfo().windowSizeClass.windowWidthSizeClass
    if (widthClass == WindowWidthSizeClass.COMPACT) {
        CreateNoteBottomSheet(
            dismissDialog = {
                createNoteViewModel.processUiAction(CreateNoteUiAction.DialogDismiss)
            },
            content = content,
        )
    } else {
        CreateNoteDialog(
            dismissDialog = {
                createNoteViewModel.processUiAction(CreateNoteUiAction.DialogDismiss)
            },
            content = content,
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CreateNoteDialog(
    dismissDialog: () -> Unit,
    content: @Composable () -> Unit,
) {
    BasicAlertDialog(onDismissRequest = dismissDialog) {
        ElevatedCard(
            modifier = Modifier
                .fillMaxWidth()
                .padding(Dimensions.spacingDefault),
            shape = RoundedCornerShape(Dimensions.spacingDefault),
        ) {
            content()
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CreateNoteBottomSheet(
    dismissDialog: () -> Unit,
    content: @Composable () -> Unit,
) {
    ModalBottomSheet(
        onDismissRequest = dismissDialog
    ) {
        content()
    }
}

@Composable
private fun CreateNoteMenu(
    createNote: (ContentType) -> Unit,
    modifier: Modifier = Modifier,
) {
    Column(modifier.fillMaxWidth()) {
        Text(
            stringResource(R.string.title_menu_noteType),
            style = MaterialTheme.typography.titleMedium,
        )
        Spacer(Modifier.height(Dimensions.spacingTiny))
        Row {
            NoteTypeOption(
                Modifier.weight(1f, true),
                icon = Icons.AutoMirrored.Rounded.Subject,
                contentDesc = "Create new text note",
                text = stringResource(R.string.title_menu_text),
                onClick = { createNote(ContentType.TEXT) }
            )
            Spacer(Modifier.width(Dimensions.spacingSmall))
            NoteTypeOption(
                Modifier.weight(1f, true),
                icon = Icons.Rounded.CheckBox,
                contentDesc = "Create new checklist note",
                text = stringResource(R.string.title_menu_checklist),
                onClick = { createNote(ContentType.CHECKLIST) }
            )
        }
    }
}

@Composable
private fun NoteTypeOption(
    modifier: Modifier = Modifier,
    icon: ImageVector,
    contentDesc: String,
    text: String,
    onClick: () -> Unit,
) {
    Button(
        modifier = modifier,
        onClick = onClick,
        colors = ButtonDefaults.buttonColors(
            containerColor = MaterialTheme.colorScheme.secondaryContainer,
            contentColor = MaterialTheme.colorScheme.onSecondaryContainer
        )
    ) {
        Icon(icon, contentDescription = contentDesc)
        Spacer(Modifier.width(Dimensions.spacingSmall))
        Text(text)
    }
}

@Preview(showBackground = true)
@Composable
private fun CreateNoteMenuPreview() {
    StickThoseNotesTheme {
        CreateNoteMenu(createNote = {})
    }
}
