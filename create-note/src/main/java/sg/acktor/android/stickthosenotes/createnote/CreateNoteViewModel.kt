/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.createnote

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import sg.acktor.android.stickthosenotes.createnote.ui.CreateNoteUiAction
import sg.acktor.android.stickthosenotes.createnote.ui.CreateNoteUiEvent
import sg.acktor.android.stickthosenotes.createnote.usecases.CreateNoteUseCase
import javax.inject.Inject

@HiltViewModel
class CreateNoteViewModel @Inject constructor(
    private val createNoteUseCase: CreateNoteUseCase,
) : ViewModel() {
    private val _uiEventChannel = Channel<CreateNoteUiEvent?>()
    val uiEventState = _uiEventChannel.receiveAsFlow()

    fun processUiAction(action: CreateNoteUiAction) {
        when (action) {
            is CreateNoteUiAction.NoteTypeSelected -> createNewNote(action)
            CreateNoteUiAction.DialogDismiss -> closeScreen()
        }
    }

    private fun createNewNote(action: CreateNoteUiAction.NoteTypeSelected) {
        viewModelScope.launch(Dispatchers.IO) {
            val newNoteId = createNoteUseCase.run(action.contentType)
            _uiEventChannel.send(
                CreateNoteUiEvent.ShowNoteDetails(newNoteId, action.contentType)
            )
        }
    }

    private fun closeScreen() {
        viewModelScope.launch {
            _uiEventChannel.send(CreateNoteUiEvent.ReturnToPreviousScreen)
        }
    }
}
