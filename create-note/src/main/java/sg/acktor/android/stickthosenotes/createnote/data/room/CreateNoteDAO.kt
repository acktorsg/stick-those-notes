/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.createnote.data.room

import androidx.room.Dao
import androidx.room.Insert
import sg.acktor.android.stickthosenotes.database.api.room.NotePositionDAO
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItem
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItemInsert
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistNote
import sg.acktor.android.stickthosenotes.database.api.room.entities.EntityId
import sg.acktor.android.stickthosenotes.database.api.room.entities.Note
import sg.acktor.android.stickthosenotes.database.api.room.entities.TextNote

@Dao
interface CreateNoteDAO : NotePositionDAO {
    // ID type MUST be Long because that's the only one that supports returning of ID from insert
    @Insert(entity = Note::class)
    suspend fun addNote(note: NoteInsert): Long

    @Insert(entity = TextNote::class)
    suspend fun addTextNote(note: EntityId)

    @Insert(entity = ChecklistNote::class)
    suspend fun addChecklistNote(note: EntityId)

    @Insert(entity = ChecklistItem::class)
    suspend fun addChecklistItem(checklistItem: ChecklistItemInsert)
}
