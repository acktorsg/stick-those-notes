/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.createnote.data

import dagger.hilt.android.scopes.ViewModelScoped
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.createnote.data.room.CreateNoteDAO
import sg.acktor.android.stickthosenotes.createnote.data.room.NoteInsert
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItemInsert
import sg.acktor.android.stickthosenotes.database.api.room.entities.EntityId
import java.time.Clock
import javax.inject.Inject

@ViewModelScoped
class CreateNoteRepository @Inject constructor(
    private val clock: Clock,
    private val dao: CreateNoteDAO,
) {
    suspend fun createNote(type: ContentType): Long {
        val position = dao.generateNextPosition() ?: 1
        val noteId = dao.addNote(NoteInsert(position = position, lastUpdated = clock.millis()))
        val note = EntityId(noteId)
        when (type) {
            ContentType.TEXT -> dao.addTextNote(note)
            ContentType.CHECKLIST -> {
                dao.addChecklistNote(note)
                dao.addChecklistItem(ChecklistItemInsert(noteId = noteId))
            }
        }

        return noteId
    }
}
