package sg.acktor.android.stickthosenotes.createnote.data

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.createnote.data.room.CreateNoteDAO
import sg.acktor.android.stickthosenotes.createnote.data.room.NoteInsert
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItemInsert
import sg.acktor.android.stickthosenotes.database.api.room.entities.EntityId
import java.time.Clock
import java.time.Instant
import java.time.ZoneId
import kotlin.test.Test
import kotlin.test.assertEquals

private const val DEFAULT_NOTE_ID = 2L
private const val DEFAULT_POSITION = 2
private const val DEFAULT_TIME_MILLIS = 1000170240000

class CreateNoteRepositoryTest {
    private val clock: Clock = Clock.fixed(
        Instant.ofEpochMilli(DEFAULT_TIME_MILLIS), ZoneId.of("GMT+8")
    )
    private val dao = mockk<CreateNoteDAO>(relaxUnitFun = true) {
        coEvery { addNote(any()) } returns DEFAULT_NOTE_ID
        coEvery { generateNextPosition() } returns DEFAULT_POSITION
    }

    private val sut = CreateNoteRepository(clock, dao)

    @Test
    fun `verify create text note`() = runTest {
        val result = sut.createNote(ContentType.TEXT)
        assertEquals(DEFAULT_NOTE_ID, result)

        coVerify {
            dao.generateNextPosition()
            dao.addNote(NoteInsert(position = DEFAULT_POSITION, lastUpdated = DEFAULT_TIME_MILLIS))
            dao.addTextNote(EntityId(DEFAULT_NOTE_ID))
        }
    }

    @Test
    fun `verify create checklist note`() = runTest {
        val result = sut.createNote(ContentType.CHECKLIST)
        assertEquals(DEFAULT_NOTE_ID, result)

        coVerify {
            dao.generateNextPosition()
            dao.addNote(NoteInsert(position = DEFAULT_POSITION, lastUpdated = DEFAULT_TIME_MILLIS))
            dao.addChecklistNote(EntityId(DEFAULT_NOTE_ID))
            dao.addChecklistItem(ChecklistItemInsert(noteId = DEFAULT_NOTE_ID))
        }
    }

    @Test
    fun `verify default starting position if last position is not available`() = runTest {
        coEvery { dao.generateNextPosition() } returns null
        sut.createNote(ContentType.TEXT)
        coVerify {
            dao.addNote(NoteInsert(position = 1, lastUpdated = DEFAULT_TIME_MILLIS))
        }
    }
}
