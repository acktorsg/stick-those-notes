package sg.acktor.android.stickthosenotes.createnote.usecases

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.createnote.data.CreateNoteRepository
import kotlin.test.Test

class CreateNoteUseCaseTest {
    private val repo = mockk<CreateNoteRepository>()

    private val sut: CreateNoteUseCase = CreateNoteUseCaseImpl(repo)

    @Test
    fun `verify create note operation`() = runTest {
        val contentType = ContentType.TEXT
        coEvery { repo.createNote(contentType) } returns 2L
        sut.run(ContentType.TEXT)
        coVerify {
            repo.createNote(contentType)
        }
    }
}
