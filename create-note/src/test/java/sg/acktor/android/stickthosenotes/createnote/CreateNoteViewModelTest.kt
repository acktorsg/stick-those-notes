package sg.acktor.android.stickthosenotes.createnote

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.createnote.ui.CreateNoteUiAction
import sg.acktor.android.stickthosenotes.createnote.ui.CreateNoteUiEvent
import sg.acktor.android.stickthosenotes.createnote.usecases.CreateNoteUseCase
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class CreateNoteViewModelTest {
    companion object {
        @BeforeAll
        @JvmStatic
        fun setUpAll() {
            Dispatchers.setMain(StandardTestDispatcher())
        }

        @AfterAll
        @JvmStatic
        fun tearDownAll() {
            Dispatchers.resetMain()
        }
    }

    private val createNoteUseCase = mockk<CreateNoteUseCase>()

    private val sut = CreateNoteViewModel(createNoteUseCase)

    @Test
    fun `return to previous screen when dialog is dismissed`() = runTest {
        val expected = CreateNoteUiEvent.ReturnToPreviousScreen
        sut.processUiAction(CreateNoteUiAction.DialogDismiss)
        val result = sut.uiEventState.first()
        assertEquals(expected, result)
    }

    @Test
    fun `show note details when note is created`() = runTest {
        val expected = CreateNoteUiEvent.ShowNoteDetails(2L, ContentType.TEXT)

        coEvery { createNoteUseCase.run(expected.contentType) } returns expected.id

        sut.processUiAction(CreateNoteUiAction.NoteTypeSelected(expected.contentType))
        val result = sut.uiEventState.first()
        assertEquals(expected, result)

        coVerify {
            createNoteUseCase.run(expected.contentType)
        }
    }
}
