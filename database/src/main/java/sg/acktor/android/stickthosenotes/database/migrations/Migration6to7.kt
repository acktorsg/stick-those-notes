/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.database.migrations

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.sqlite.db.SupportSQLiteQueryBuilder
import kotlinx.serialization.json.Json
import sg.acktor.android.stickthosenotes.database.api.room.entities.NoteV6Content
import javax.inject.Inject

class Migration6to7 @Inject constructor() : Migration(6, 7) {
    override fun migrate(db: SupportSQLiteDatabase) {
        db.beginTransaction()

        // Renamed old table to something different
        db.execSQL("ALTER TABLE $TABLE_NOTE RENAME TO $TABLE_NOTE_TEMP")
        // Create new tables
        db.execSQL(
            """
                CREATE TABLE IF NOT EXISTS Note (
                    $COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    $COLUMN_TITLE TEXT NOT NULL DEFAULT '',
                    $COLUMN_COLOR_PALETTE TEXT NOT NULL,
                    $COLUMN_IS_PINNED INTEGER NOT NULL DEFAULT 0,
                    $COLUMN_POSITION INTEGER NOT NULL,
                    $COLUMN_LAST_UPDATED INTEGER NOT NULL,
                    $COLUMN_IS_DELETED INTEGER NOT NULL DEFAULT 0
                )
            """
        )

        db.execSQL(
            """
               CREATE TABLE IF NOT EXISTS TextNote (
                    $COLUMN_ID INTEGER NOT NULL,
                    $COLUMN_CONTENT TEXT NOT NULL DEFAULT '',
                    PRIMARY KEY($COLUMN_ID),
                    FOREIGN KEY($COLUMN_ID) REFERENCES Note($COLUMN_ID)
                    ON UPDATE NO ACTION
                    ON DELETE CASCADE
               ) 
            """
        )

        db.execSQL(
            """
               CREATE TABLE IF NOT EXISTS ChecklistNote (
                    $COLUMN_ID INTEGER NOT NULL,
                    PRIMARY KEY($COLUMN_ID),
                    FOREIGN KEY($COLUMN_ID) REFERENCES Note($COLUMN_ID)
                    ON UPDATE NO ACTION
                    ON DELETE CASCADE
               ) 
            """
        )

        db.execSQL(
            """
               CREATE TABLE IF NOT EXISTS ChecklistItem (
                    $COLUMN_ID INTEGER NOT NULL,
                    $COLUMN_NOTE_ID INTEGER NOT NULL,
                    $COLUMN_CONTENT TEXT NOT NULL DEFAULT '',
                    $COLUMN_IS_CHECKED INTEGER NOT NULL DEFAULT 0,
                    PRIMARY KEY($COLUMN_ID, $COLUMN_NOTE_ID),
                    FOREIGN KEY($COLUMN_NOTE_ID) REFERENCES ChecklistNote($COLUMN_ID)
                    ON UPDATE NO ACTION
                    ON DELETE CASCADE
               ) 
            """
        )

        db.execSQL("CREATE INDEX IF NOT EXISTS index_ChecklistItem_noteId ON ChecklistItem(noteId)")
        db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY, identity_hash TEXT)")
        db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'd39e902fb04cee9e99696bb04972d82f')")

        // Query data from previous database version
        val cursor = db.query(
            SupportSQLiteQueryBuilder.builder(TABLE_NOTE_TEMP)
                .columns(
                    arrayOf(
                        COLUMN_ID, COLUMN_TITLE, COLUMN_CONTENT, COLUMN_COLOR_PALETTE,
                        COLUMN_IS_PINNED, COLUMN_IS_DELETED, COLUMN_POSITION, COLUMN_LAST_UPDATED
                    )
                )
                .orderBy(COLUMN_POSITION)
                .create()
        )
        if (cursor.moveToFirst()) {
            val noteValues = ContentValues(7)
            val textNoteValues = ContentValues(2)
            val checklistNoteValues = ContentValues(1)
            val checklistItemValues = ContentValues(4)

            var noteId: Long
            var title: String
            var content: NoteV6Content
            var colorPalette: String
            var isPinned: Int
            var isDeleted: Int
            var position: Int
            var lastUpdated: Long

            var contentNoteId: Long

            var errorCode: Long
            do {
                noteId = cursor.getLong(0)
                title = cursor.getString(1)
                content = Json.decodeFromString(cursor.getString(2))
                colorPalette = cursor.getString(3)
                isPinned = cursor.getInt(4)
                isDeleted = cursor.getInt(5)
                position = cursor.getInt(6)
                lastUpdated = cursor.getLong(7)

                // Retrieve note details
                noteValues.put(COLUMN_ID, noteId)
                noteValues.put(COLUMN_TITLE, title)
                noteValues.put(COLUMN_COLOR_PALETTE, colorPalette)
                noteValues.put(COLUMN_IS_PINNED, isPinned)
                noteValues.put(COLUMN_POSITION, position)
                noteValues.put(COLUMN_LAST_UPDATED, lastUpdated)
                noteValues.put(COLUMN_IS_DELETED, isDeleted)

                // Insert note to new table. Skip everything else if it fails
                errorCode = db.insert("Note", SQLiteDatabase.CONFLICT_IGNORE, noteValues)
                if (errorCode == -1L) continue
                when (content) {
                    is sg.acktor.android.stickthosenotes.database.api.room.entities.NoteV6Content.Text -> {
                        // Retrieve text content details
                        textNoteValues.put(COLUMN_ID, noteId)
                        textNoteValues.put(COLUMN_CONTENT, content.content)

                        // Insert text content to new table. Undo previous changes if it fails
                        contentNoteId = db.insert(
                            "TextNote", SQLiteDatabase.CONFLICT_FAIL, textNoteValues
                        )
                        if (contentNoteId == -1L) {
                            db.delete("Note", "$COLUMN_ID = ?", arrayOf(noteId))
                        }
                    }

                    is sg.acktor.android.stickthosenotes.database.api.room.entities.NoteV6Content.Checklist -> {
                        // Retrieve & insert checklist note to new table. Undo previous changes if
                        // it fails
                        checklistNoteValues.put(COLUMN_ID, noteId)
                        contentNoteId = db.insert(
                            "ChecklistNote", SQLiteDatabase.CONFLICT_FAIL, checklistNoteValues
                        )
                        if (contentNoteId == -1L) {
                            db.delete("Note", "$COLUMN_ID = ?", arrayOf(noteId))
                        } else {
                            // Retrieve & insert checklist items to new table. Ignore errors on the
                            // particular row if it fails
                            for ((i, checklistItem) in content.items.withIndex()) {
                                checklistItemValues.put(COLUMN_ID, (i + 1).toLong())
                                checklistItemValues.put(COLUMN_NOTE_ID, contentNoteId)
                                checklistItemValues.put(COLUMN_CONTENT, checklistItem.content)
                                checklistItemValues.put(COLUMN_IS_CHECKED, checklistItem.isChecked)

                                db.insert(
                                    "ChecklistItem", SQLiteDatabase.CONFLICT_IGNORE,
                                    checklistItemValues
                                )
                            }
                        }
                    }
                }
            } while (cursor.moveToNext())
        }

        db.setTransactionSuccessful()
        db.endTransaction()
    }
}
