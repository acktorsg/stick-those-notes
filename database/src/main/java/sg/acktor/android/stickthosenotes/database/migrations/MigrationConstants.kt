/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.database.migrations

internal const val COLUMN_IS_PINNED_V1 = "is_pinned"
internal const val COLUMN_IS_DELETED_V1 = "is_deleted"
internal const val COLUMN_ROW_ID = "rowid"
internal const val COLUMN_STROKE_COLOR = "strokeColor"
internal const val COLUMN_BACKGROUND_COLOR = "backgroundColor"
internal const val COLUMN_DATE_TIME_CREATED = "date_time_created"
internal const val COLUMN_ID = "id"
internal const val COLUMN_TITLE = "title"
internal const val COLUMN_CONTENT = "content"
internal const val COLUMN_COLOR_PALETTE = "colorPalette"
internal const val COLUMN_IS_PINNED = "isPinned"
internal const val COLUMN_IS_DELETED = "isDeleted"
internal const val COLUMN_POSITION = "position"
internal const val COLUMN_LAST_UPDATED = "lastUpdated"
internal const val COLUMN_COLOR = "color"
internal const val COLUMN_NOTE_ID = "noteId"
internal const val COLUMN_IS_CHECKED = "isChecked"

internal const val TABLE_NOTE_TEMP = "noteTemp"
internal const val TABLE_NOTE = "note"
