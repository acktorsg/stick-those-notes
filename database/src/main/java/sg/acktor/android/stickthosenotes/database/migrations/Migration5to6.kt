/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.database.migrations

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.sqlite.db.SupportSQLiteQueryBuilder
import sg.acktor.android.stickthosenotes.database.mappers.BgColorToPaletteMapper
import javax.inject.Inject

/**
 * Screw you, past me, for merging stroke & background colour columns
 */
class Migration5to6 @Inject constructor(
        private val bgColorToPaletteMapper: BgColorToPaletteMapper
) : Migration(5, 6) {
    override fun migrate(db: SupportSQLiteDatabase) {
        db.beginTransaction()

        db.execSQL("ALTER TABLE $TABLE_NOTE RENAME TO $TABLE_NOTE_TEMP")
        db.execSQL(
                """
                CREATE TABLE $TABLE_NOTE (
                    $COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    $COLUMN_TITLE TEXT NOT NULL,
                    $COLUMN_CONTENT TEXT NOT NULL,
                    $COLUMN_COLOR_PALETTE TEXT NOT NULL,
                    $COLUMN_IS_PINNED INTEGER NOT NULL,
                    $COLUMN_IS_DELETED INTEGER NOT NULL DEFAULT 0,
                    $COLUMN_POSITION INTEGER NOT NULL,
                    $COLUMN_LAST_UPDATED INTEGER NOT NULL
                )                
            """
        )

        val cursor = db.query(
                SupportSQLiteQueryBuilder.builder(TABLE_NOTE_TEMP)
                        .columns(
                                arrayOf(
                                        COLUMN_ID, COLUMN_TITLE, COLUMN_CONTENT, COLUMN_BACKGROUND_COLOR,
                                        COLUMN_IS_PINNED, COLUMN_IS_DELETED, COLUMN_POSITION, COLUMN_LAST_UPDATED
                                )
                        )
                        .orderBy(COLUMN_POSITION)
                        .create()
        )
        if (cursor.moveToFirst()) {
            val values = ContentValues(8)

            var id: Long
            var title: String
            var content: String
            var colorPalette: String
            var isPinned: Int
            var isDeleted: Int
            var position: Int
            var lastUpdated: Long
            do {
                id = cursor.getLong(0)
                title = cursor.getString(1)
                content = cursor.getString(2)
                colorPalette = bgColorToPaletteMapper.map(cursor.getInt(3))
                isPinned = cursor.getInt(4)
                isDeleted = cursor.getInt(5)
                position = cursor.getInt(6)
                lastUpdated = cursor.getLong(7)

                values.put(COLUMN_ID, id)
                values.put(COLUMN_TITLE, title)
                values.put(COLUMN_CONTENT, content)
                values.put(COLUMN_COLOR_PALETTE, colorPalette)
                values.put(COLUMN_IS_PINNED, isPinned)
                values.put(COLUMN_IS_DELETED, isDeleted)
                values.put(COLUMN_POSITION, position)
                values.put(COLUMN_LAST_UPDATED, lastUpdated)

                db.insert(TABLE_NOTE, SQLiteDatabase.CONFLICT_IGNORE, values)
            } while (cursor.moveToNext())
        }

        cursor.close()
        db.execSQL("DROP TABLE $TABLE_NOTE_TEMP")

        db.setTransactionSuccessful()
        db.endTransaction()
    }
}
