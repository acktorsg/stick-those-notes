/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.database.di

import androidx.room.migration.Migration
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet
import sg.acktor.android.stickthosenotes.database.migrations.Migration1to5
import sg.acktor.android.stickthosenotes.database.migrations.Migration2to5
import sg.acktor.android.stickthosenotes.database.migrations.Migration3to5
import sg.acktor.android.stickthosenotes.database.migrations.Migration4to5
import sg.acktor.android.stickthosenotes.database.migrations.Migration5to6
import sg.acktor.android.stickthosenotes.database.migrations.Migration6to7

@Module
@InstallIn(SingletonComponent::class)
object MigrationModule {
    @Provides
    @IntoSet
    fun provideMigration1to5(migration: Migration1to5): Migration = migration

    @Provides
    @IntoSet
    fun provideMigration2to5(migration: Migration2to5): Migration = migration

    @Provides
    @IntoSet
    fun provideMigration3to5(migration: Migration3to5): Migration = migration

    @Provides
    @IntoSet
    fun provideMigration4to5(migration: Migration4to5): Migration = migration

    @Provides
    @IntoSet
    fun provideMigration5to6(migration: Migration5to6): Migration = migration

    @Provides
    @IntoSet
    fun provideMigration6to7(migration: Migration6to7): Migration = migration
}
