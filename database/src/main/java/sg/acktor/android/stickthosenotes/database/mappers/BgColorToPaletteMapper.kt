/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.database.mappers

import androidx.annotation.ColorInt
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import javax.inject.Inject

class BgColorToPaletteMapper @Inject constructor() {
    fun map(@ColorInt backgroundColor: Int) =
            when (backgroundColor) {
                0xFFE57373.toInt() -> ColorPalette.RED
                0xFFF06292.toInt() -> ColorPalette.PINK
                0xFFBA68C8.toInt() -> ColorPalette.PURPLE
                0xFF9575CD.toInt() -> ColorPalette.DEEP_PURPLE
                0xFF7986CB.toInt() -> ColorPalette.INDIGO
                0xFF64B5F6.toInt() -> ColorPalette.BLUE
                0xFF4FC3F7.toInt() -> ColorPalette.LIGHT_BLUE
                0xFF4DD0E1.toInt() -> ColorPalette.CYAN
                0xFF4DB6AC.toInt() -> ColorPalette.TEAL
                0xFF81C784.toInt() -> ColorPalette.GREEN
                0xFFAED581.toInt() -> ColorPalette.LIGHT_GREEN
                0xFFDCE775.toInt() -> ColorPalette.LIME
                0xFFFFF176.toInt() -> ColorPalette.YELLOW
                0xFFFFD54F.toInt() -> ColorPalette.AMBER
                0xFFFFB74D.toInt() -> ColorPalette.ORANGE
                0xFFFF8A65.toInt() -> ColorPalette.DEEP_ORANGE
                0xFFA1887F.toInt() -> ColorPalette.BROWN
                0xFFE0E0E0.toInt() -> ColorPalette.GREY
                0xFF90A4AE.toInt() -> ColorPalette.BLUE_GREY
                0xFF616161.toInt() -> ColorPalette.BLACK
                else -> ColorPalette.WHITE
            }.name
}
