/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.database.migrations

import androidx.annotation.ColorInt
import javax.inject.Inject

class BackgroundColorGenerator @Inject constructor() {
    fun generate(@ColorInt color: Int) = when (color) {
        0xFFF44336.toInt() -> 0xFFE57373 // RED
        0xFFE91E63.toInt() -> 0xFFF06292 // PINK
        0xFF9C27B0.toInt() -> 0xFFBA68C8 // PURPLE
        0xFF673AB7.toInt() -> 0xFF9575CD // DEEP PURPLE
        0xFF3F51B5.toInt() -> 0xFF7986CB // INDIGO
        0xFF2196F3.toInt() -> 0xFF64B5F6 // BLUE
        0xFF03A9F4.toInt() -> 0xFF4FC3F7 // LIGHT BLUE
        0xFF00BCD4.toInt() -> 0xFF4DD0E1 // CYAN
        0xFF009688.toInt() -> 0xFF4DB6AC // TEAL
        0xFF4CAF50.toInt() -> 0xFF81C784 // GREEN
        0xFF8BC34A.toInt() -> 0xFFAED581 // LIGHT GREEN
        0xFFCDDC39.toInt() -> 0xFFDCE775 // LIME
        0xFFFFEB3B.toInt() -> 0xFFFFF176 // YELLOW
        0xFFFFC107.toInt() -> 0xFFFFD54F // AMBER
        0xFFFF9800.toInt() -> 0xFFFFB74D // ORANGE
        0xFFFF5722.toInt() -> 0xFFFF8A65 // DEEP ORANGE
        0xFF795548.toInt() -> 0xFFA1887F // BROWN
        0xFF9E9E9E.toInt() -> 0xFFE0E0E0 // GREY
        0xFF607D8B.toInt() -> 0xFF90A4AE // BLUE GREY
        0xFF212121.toInt() -> 0xFF616161 // BLACK
        0xFFE0E0E0.toInt() -> 0xFFFFFFFF // WHITE
        else -> 0xFFFAFAFA
    }.toInt()
}
