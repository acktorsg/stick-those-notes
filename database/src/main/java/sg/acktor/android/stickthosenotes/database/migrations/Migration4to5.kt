/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.database.migrations

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.sqlite.db.SupportSQLiteQueryBuilder
import java.time.Clock
import javax.inject.Inject

class Migration4to5 @Inject constructor(
        private val clock: Clock
) : Migration(4, 5) {
    override fun migrate(db: SupportSQLiteDatabase) {
        db.beginTransaction()

        db.execSQL("ALTER TABLE noteV4 RENAME TO $TABLE_NOTE_TEMP")
        db.execSQL(
                """
               CREATE TABLE $TABLE_NOTE (
                    $COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    $COLUMN_TITLE TEXT NOT NULL,
                    $COLUMN_CONTENT TEXT NOT NULL,
                    $COLUMN_STROKE_COLOR INTEGER NOT NULL,
                    $COLUMN_BACKGROUND_COLOR INTEGER NOT NULL,
                    $COLUMN_IS_PINNED INTEGER NOT NULL,
                    $COLUMN_IS_DELETED INTEGER NOT NULL DEFAULT 0,
                    $COLUMN_POSITION INTEGER NOT NULL,
                    $COLUMN_LAST_UPDATED INTEGER NOT NULL
               )
            """
        )

        val cursor = db.query(
                SupportSQLiteQueryBuilder.builder(TABLE_NOTE_TEMP)
                        .columns(
                                arrayOf(
                                        COLUMN_ID, COLUMN_TITLE, COLUMN_CONTENT, COLUMN_STROKE_COLOR,
                                        COLUMN_BACKGROUND_COLOR, COLUMN_IS_PINNED
                                )
                        )
                        .selection("$COLUMN_IS_DELETED = ?", arrayOf("0"))
                        .create()
        )
        if (cursor.moveToFirst()) {
            val values = ContentValues(8)
            var position = 0

            var id: Long
            var title: String
            var content: String
            var strokeColor: Int
            var bgColor: Int
            var isPinned: Int
            do {
                id = cursor.getLong(0)
                title = cursor.getString(1)
                content = cursor.getString(2)
                strokeColor = cursor.getInt(3)
                bgColor = cursor.getInt(4)
                isPinned = cursor.getInt(5)

                values.put(COLUMN_ID, id)
                values.put(COLUMN_TITLE, title)
                values.put(COLUMN_CONTENT, content)
                values.put(COLUMN_STROKE_COLOR, strokeColor)
                values.put(COLUMN_BACKGROUND_COLOR, bgColor)
                values.put(COLUMN_IS_PINNED, isPinned)
                values.put(COLUMN_POSITION, position++)
                values.put(COLUMN_LAST_UPDATED, clock.millis())

                db.insert(TABLE_NOTE, SQLiteDatabase.CONFLICT_IGNORE, values)
            } while (cursor.moveToNext())
        }

        cursor.close()
        db.execSQL("DROP TABLE $TABLE_NOTE_TEMP")

        db.setTransactionSuccessful()
        db.endTransaction()
    }
}
