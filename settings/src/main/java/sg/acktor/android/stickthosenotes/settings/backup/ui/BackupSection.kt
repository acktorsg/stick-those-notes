/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup.ui

import android.net.Uri
import android.provider.DocumentsContract
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Download
import androidx.compose.material.icons.rounded.Upload
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import sg.acktor.acktorsdk.ui.compose.Dimensions
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.settings.R
import sg.acktor.android.stickthosenotes.settings.backup.BackupViewModel
import sg.acktor.android.stickthosenotes.settings.ui.PreferenceItem
import sg.acktor.android.stickthosenotes.settings.ui.PreferenceTitle
import sg.acktor.android.stickthosenotes.settings.ui.theme.spacingPreferenceTitle

@Composable
internal fun BackupSection(
    backupViewModel: BackupViewModel = hiltViewModel(),
    showMessage: (String) -> Unit
) {
    val context = LocalContext.current
    var showLoadBackupWarning by remember { mutableStateOf(false) }

    val createBackupEvent = rememberLauncherForActivityResult(
        ActivityResultContracts.CreateDocument("application/json")
    ) { uri: Uri? ->
        if (uri == null || !DocumentsContract.isDocumentUri(context, uri)) {
            return@rememberLauncherForActivityResult
        }

        backupViewModel.processUiAction(BackupUiAction.BackupFileCreated(uri))
    }

    val loadBackupEvent = rememberLauncherForActivityResult(
        ActivityResultContracts.OpenDocument()
    ) { uri: Uri? ->
        if (uri == null || !DocumentsContract.isDocumentUri(context, uri)) {
            return@rememberLauncherForActivityResult
        }

        backupViewModel.processUiAction(BackupUiAction.BackupFileSelected(uri))
    }

    LaunchedEffect(Unit) {
        backupViewModel.uiEventState.collect { uiEvent ->
            when (uiEvent) {
                is BackupUiEvent.CreateBackupFile -> createBackupEvent.launch(uiEvent.fileName)
                is BackupUiEvent.ShowMessage -> showMessage(uiEvent.message)
                BackupUiEvent.SelectBackupFile -> loadBackupEvent.launch(arrayOf("application/json"))
                BackupUiEvent.ShowLoadBackupWarning -> {
                    showLoadBackupWarning = true
                }

                else -> {}
            }
        }
    }

    Content(sendUiAction = backupViewModel::processUiAction)

    if (showLoadBackupWarning) {
        LoadBackupWarningDialog(
            performAction = {
                backupViewModel.processUiAction(BackupUiAction.LoadBackupConfirmed)
                showLoadBackupWarning = false
            },
            dismissDialog = { showLoadBackupWarning = false }
        )
    }
}

@Composable
private fun Content(
    sendUiAction: (BackupUiAction) -> Unit,
) {
    Column {
        PreferenceTitle(
            title = stringResource(R.string.title_section_backup),
            modifier = Modifier.padding(start = Dimensions.spacingPreferenceTitle)
        )
        PreferenceItem(
            icon = {
                Icon(Icons.Rounded.Download, contentDescription = "Create backup")
            },
            title = stringResource(R.string.title_settings_createBackup),
            summary = stringResource(R.string.summary_settings_createBackup),
            onClick = { sendUiAction(BackupUiAction.CreateBackup) }
        )
        PreferenceItem(
            icon = {
                Icon(Icons.Rounded.Upload, contentDescription = "Load backup")
            },
            title = stringResource(R.string.title_settings_loadBackup),
            summary = stringResource(R.string.summary_settings_loadBackup),
            onClick = { sendUiAction(BackupUiAction.LoadBackup) }
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun BackupSectionPreview() {
    StickThoseNotesTheme {
        Content(sendUiAction = {})
    }
}
