/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup.ui

import android.net.Uri

sealed interface BackupUiAction {
    data object CreateBackup : BackupUiAction

    data class BackupFileCreated(val uri: Uri) : BackupUiAction

    data object LoadBackup : BackupUiAction

    data class BackupFileSelected(val uri: Uri) : BackupUiAction

    data object LoadBackupConfirmed : BackupUiAction
}

sealed interface BackupUiEvent {
    data class CreateBackupFile(val fileName: String) : BackupUiEvent

    data object SelectBackupFile : BackupUiEvent

    data object ShowLoadBackupWarning : BackupUiEvent

    data class ShowMessage(val message: String) : BackupUiEvent
}
