/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup.ui

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.window.DialogProperties
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.settings.R

@Composable
internal fun LoadBackupWarningDialog(
    performAction: () -> Unit,
    dismissDialog: () -> Unit,
) {
    AlertDialog(
        text = { Text(stringResource(R.string.message_warning_backupOverride)) },
        confirmButton = {
            TextButton(onClick = performAction) {
                Text(stringResource(R.string.title_button_loadBackup))
            }
        },
        dismissButton = {
            TextButton(onClick = dismissDialog) {
                Text(
                    stringResource(
                        sg.acktor.android.stickthosenotes.common.R.string.title_button_cancel
                    )
                )
            }
        },
        onDismissRequest = dismissDialog,
        properties = DialogProperties(
            dismissOnBackPress = false,
            dismissOnClickOutside = false,
        )
    )
}

@Preview
@Composable
private fun LoadBackupWarningDialogPreview() {
    StickThoseNotesTheme {
        LoadBackupWarningDialog(
            performAction = {},
            dismissDialog = {},
        )
    }
}
