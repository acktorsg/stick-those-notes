/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.customization.ui

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Style
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import sg.acktor.acktorsdk.ui.compose.Dimensions
import sg.acktor.android.stickthosenotes.settings.R
import sg.acktor.android.stickthosenotes.settings.customization.CustomizationViewModel
import sg.acktor.android.stickthosenotes.settings.models.AppThemeMode
import sg.acktor.android.stickthosenotes.settings.ui.PreferenceItem
import sg.acktor.android.stickthosenotes.settings.ui.PreferenceTitle
import sg.acktor.android.stickthosenotes.settings.ui.theme.spacingPreferenceTitle

@Composable
fun CustomizationSection(customizationViewModel: CustomizationViewModel = hiltViewModel()) {
    val uiState by customizationViewModel.uiState.collectAsStateWithLifecycle()

    PreferenceTitle(
        title = stringResource(R.string.title_section_customization),
        modifier = Modifier.padding(start = Dimensions.spacingPreferenceTitle)
    )
    Box {
        PreferenceItem(
            icon = { Icon(Icons.Rounded.Style, contentDescription = "Theme mode") },
            title = stringResource(R.string.title_settings_theme),
            summary = uiState.currentSetting,
            onClick = { customizationViewModel.processUiAction(CustomizationUiAction.ChangeTheme) }
        )

        DropdownMenu(
            expanded = uiState.showPopup,
            offset = DpOffset(Dimensions.spacingPreferenceTitle, 0.dp),
            onDismissRequest = {
                customizationViewModel.processUiAction(CustomizationUiAction.HidePopup)
            }
        ) {
            AppThemeMode.entries.map { theme ->
                DropdownMenuItem(
                    text = { Text(theme.strValue) },
                    onClick = {
                        customizationViewModel.processUiAction(
                            CustomizationUiAction.ThemeSelected(theme)
                        )
                    }
                )
            }
        }
    }
}
