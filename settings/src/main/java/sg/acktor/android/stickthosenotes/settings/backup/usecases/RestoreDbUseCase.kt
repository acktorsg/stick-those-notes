/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup.usecases

import android.content.ContentResolver
import android.net.Uri
import androidx.annotation.VisibleForTesting
import dagger.hilt.android.components.ViewModelComponent
import it.czerwinski.android.hilt.annotations.Bound
import sg.acktor.android.stickthosenotes.settings.backup.data.BackupRepository
import sg.acktor.android.stickthosenotes.settings.backup.domain.ReaderBuilder
import sg.acktor.android.stickthosenotes.settings.backup.mappers.NoteBackupReader
import java.io.File
import java.io.IOException
import java.io.Reader
import javax.inject.Inject

interface RestoreDbUseCase {
    suspend fun run(contentResolver: ContentResolver, uri: Uri)

    suspend fun run(file: File)
}

@Bound(ViewModelComponent::class)
class RestoreDbUseCaseImpl @Inject constructor(
        private val repo: BackupRepository,
        private val readerBuilder: ReaderBuilder,
        private val backupReader: NoteBackupReader
) : RestoreDbUseCase {
    override suspend fun run(contentResolver: ContentResolver, uri: Uri) {
        val parcelFileDescriptor = contentResolver.openFileDescriptor(uri, "r")
                ?: throw IOException("An error occurred while opening the file")
        run(readerBuilder.build(parcelFileDescriptor))
        parcelFileDescriptor.close()
    }

    override suspend fun run(file: File) = run(readerBuilder.build(file))

    @VisibleForTesting
    internal suspend fun run(reader: Reader) {
        val apiModel = backupReader.readFromFile(reader)
        repo.saveBackup(apiModel)
    }
}
