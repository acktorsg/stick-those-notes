/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup.data

import dagger.hilt.android.scopes.ViewModelScoped
import sg.acktor.android.stickthosenotes.settings.backup.models.BackupApiModel
import sg.acktor.android.stickthosenotes.settings.backup.data.room.BackupNotesDAO
import javax.inject.Inject

@ViewModelScoped
class BackupRepository @Inject constructor(
        private val dao: BackupNotesDAO,
) {
    suspend fun getBackup(): BackupApiModel {
        val notes = dao.getAllNotes()
        val textNotes = dao.getAllTextNotes()
        val checklistNotes = dao.getAllChecklistNotes()
        val checklistItems = dao.getAllChecklistItems()

        return BackupApiModel(notes, textNotes, checklistNotes, checklistItems)
    }

    suspend fun saveBackup(apiModel: BackupApiModel) {
        dao.clearAllNotes()
        dao.addAllNotes(apiModel.noteDetails)
        dao.addAllTextNotes(apiModel.textContents)
        dao.addAllChecklistNotes(apiModel.checklistContents)
        dao.addAllChecklistItems(apiModel.checklistItems)
    }
}
