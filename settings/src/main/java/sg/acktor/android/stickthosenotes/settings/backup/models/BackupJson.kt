/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup.models

import kotlinx.serialization.Serializable
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItem
import sg.acktor.android.stickthosenotes.database.api.room.entities.Note
import sg.acktor.android.stickthosenotes.database.api.room.entities.TextNote

@Serializable
class BackupJson(
    val databaseVersion: Int,
    val notes: List<Note>,
    val textNotes: List<TextNote>,
    val checklistNotes: List<Long>,
    val checklistItems: List<ChecklistItem>,
)

@Serializable
class BackupV6Json(
    val databaseVersion: Int,
    val notes: List<NoteV6>,
)

@Serializable
class NoteV6(
    val id: Long,
    val title: String,
    val contentType: Int,
    val content: String,
    val colorPalette: ColorPalette,
    val isPinned: Boolean,
    val position: Int,
)
