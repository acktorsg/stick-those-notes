/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Download
import androidx.compose.material.icons.rounded.Upload
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import sg.acktor.acktorsdk.ui.compose.Dimensions
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.settings.R
import sg.acktor.android.stickthosenotes.settings.ui.theme.spacingPreferenceTitle

@Composable
internal fun PreferenceItem(
        icon: @Composable () -> Unit,
        title: String,
        summary: String,
        onClick: () -> Unit,
) {
    Box(
            Modifier
                    .fillMaxWidth()
                    .clickable(onClick = onClick)
    ) {
        Row(
                Modifier.padding(Dimensions.spacingDefault),
                verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(Modifier.width(Dimensions.spacingSmall))
            icon()
            Spacer(Modifier.width(Dimensions.spacingDefault + Dimensions.spacingSmall))
            Column {
                Text(
                        title,
                        style = MaterialTheme.typography.titleSmall
                )
                Text(
                        summary,
                        style = MaterialTheme.typography.bodySmall
                )
            }
        }
    }
}

@Composable
internal fun PreferenceTitle(
        modifier: Modifier = Modifier,
        title: String
) {
    Text(
            title,
            modifier = modifier.padding(vertical = Dimensions.spacingSmall),
            color = MaterialTheme.colorScheme.primary,
            style = MaterialTheme.typography.labelMedium
    )
}

@Preview(showBackground = true)
@Composable
private fun PreferenceSectionPreview() {
    StickThoseNotesTheme {
        Column {
            PreferenceTitle(
                    title = stringResource(R.string.title_section_customization),
                    modifier = Modifier.padding(start = Dimensions.spacingPreferenceTitle)
            )
            PreferenceItem(
                    icon = {
                        Icon(Icons.Rounded.Download, contentDescription = "Create backup")
                    },
                    title = stringResource(R.string.title_settings_createBackup),
                    summary = stringResource(R.string.summary_settings_createBackup),
                    onClick = {}
            )
            PreferenceItem(
                    icon = {
                        Icon(Icons.Rounded.Upload, contentDescription = "Load backup")
                    },
                    title = stringResource(R.string.title_settings_loadBackup),
                    summary = stringResource(R.string.summary_settings_loadBackup),
                    onClick = {}
            )
        }
    }
}
