/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup.usecases

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import androidx.annotation.VisibleForTesting
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import it.czerwinski.android.hilt.annotations.Bound
import sg.acktor.android.stickthosenotes.settings.backup.data.BackupRepository
import sg.acktor.android.stickthosenotes.settings.backup.domain.WriterBuilder
import sg.acktor.android.stickthosenotes.settings.backup.mappers.NoteBackupWriter
import java.io.File
import java.io.IOException
import java.io.Writer
import java.time.Clock
import javax.inject.Inject

@VisibleForTesting
internal const val FILE_NAME_TEMP_BACKUP = "tempNotesBackup.json"

private const val BACKUP_FILE_TEMPLATE =
    "stick-those-notes-backup_%1\$tY%1\$tm%1\$td_%1\$tH%1\$tM%1\$tS.json"

interface BackupDbUseCase {
    fun generateBackupFileName(): String

    suspend fun createBackup(contentResolver: ContentResolver, uri: Uri)

    suspend fun createTempBackupFile(): File
}

@Bound(ViewModelComponent::class)
class BackupDbUseCaseImpl @Inject constructor(
    @ApplicationContext
    private val context: Context,
    private val repo: BackupRepository,
    private val writerBuilder: WriterBuilder,
    private val backupWriter: NoteBackupWriter,
    private val clock: Clock,
) : BackupDbUseCase {
    private val tempFileDir = context.cacheDir

    override fun generateBackupFileName() = BACKUP_FILE_TEMPLATE.format(clock.millis())

    override suspend fun createBackup(contentResolver: ContentResolver, uri: Uri) {
        val parcelFileDescriptor = contentResolver.openFileDescriptor(uri, "w")
            ?: throw IOException("An error occurred while opening the file")
        val writer = writerBuilder.build(parcelFileDescriptor)
        run(writer)
        parcelFileDescriptor.close()
    }

    override suspend fun createTempBackupFile(): File {
        val file = File(tempFileDir, FILE_NAME_TEMP_BACKUP)
        run(writerBuilder.build(file))
        return file
    }

    private suspend fun run(writer: Writer) {
        val backup = repo.getBackup()
        backupWriter.writeToFile(writer, backup)
    }
}
