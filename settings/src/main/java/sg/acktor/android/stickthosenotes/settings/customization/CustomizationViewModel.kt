/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.customization

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import sg.acktor.android.stickthosenotes.settings.customization.domain.ThemeProvider
import sg.acktor.android.stickthosenotes.settings.customization.ui.CustomizationUiAction
import sg.acktor.android.stickthosenotes.settings.customization.ui.CustomizationUiModel
import javax.inject.Inject

@HiltViewModel
class CustomizationViewModel @Inject constructor(
    private val themeProvider: ThemeProvider,
) : ViewModel() {
    private val _uiState = MutableStateFlow(
        CustomizationUiModel(
            currentSetting = "",
            showPopup = false,
        )
    )
    val uiState = _uiState.asStateFlow()

    init {
        themeProvider.themeState
            .onEach { themeMode ->
                _uiState.update { it.copy(currentSetting = themeMode.strValue) }
            }
            .launchIn(viewModelScope)
    }

    fun processUiAction(action: CustomizationUiAction) {
        when (action) {
            CustomizationUiAction.ChangeTheme -> showThemeOptions()
            CustomizationUiAction.HidePopup -> hideThemeOptions()
            is CustomizationUiAction.ThemeSelected -> changeTheme(action)
        }
    }

    private fun showThemeOptions() {
        _uiState.update { it.copy(showPopup = true) }
    }

    private fun hideThemeOptions() {
        _uiState.update { it.copy(showPopup = false) }
    }

    private fun changeTheme(action: CustomizationUiAction.ThemeSelected) {
        hideThemeOptions()

        viewModelScope.launch(Dispatchers.IO) {
            themeProvider.setCurrentTheme(action.theme)
        }
    }
}
