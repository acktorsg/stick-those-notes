/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup.data.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItem
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistNote
import sg.acktor.android.stickthosenotes.database.api.room.entities.Note
import sg.acktor.android.stickthosenotes.database.api.room.entities.TextNote

@Dao
interface BackupNotesDAO {
    ///////////////////////////////////////////////////////////////////////////
    // Backup
    ///////////////////////////////////////////////////////////////////////////
    @Query("SELECT * FROM Note WHERE isDeleted = 0 ORDER BY position")
    suspend fun getAllNotes(): List<Note>

    @Query("SELECT t.* FROM TextNote t INNER JOIN Note n USING(id) WHERE isDeleted = 0")
    suspend fun getAllTextNotes(): List<TextNote>

    @Query("SELECT c.* FROM ChecklistNote c INNER JOIN Note n USING(id) WHERE isDeleted = 0")
    suspend fun getAllChecklistNotes(): List<ChecklistNote>

    @Query("SELECT c.* FROM ChecklistItem c INNER JOIN Note n ON c.noteId = n.id WHERE isDeleted = 0")
    suspend fun getAllChecklistItems(): List<ChecklistItem>

    ///////////////////////////////////////////////////////////////////////////
    // Restore
    ///////////////////////////////////////////////////////////////////////////
    @Insert
    suspend fun addAllNotes(notes: List<Note>)

    @Insert
    suspend fun addAllTextNotes(notes: List<TextNote>)

    @Insert
    suspend fun addAllChecklistNotes(notes: List<ChecklistNote>)

    @Insert
    suspend fun addAllChecklistItems(notes: List<ChecklistItem>)

    ///////////////////////////////////////////////////////////////////////////
    // Clear database
    ///////////////////////////////////////////////////////////////////////////
    @Query("DELETE FROM Note")
    suspend fun clearAllNotes()
}
