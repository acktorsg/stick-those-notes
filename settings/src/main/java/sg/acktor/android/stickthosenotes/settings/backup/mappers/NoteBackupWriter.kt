/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup.mappers

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import sg.acktor.android.stickthosenotes.database.api.room.DATABASE_VERSION
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistNote
import sg.acktor.android.stickthosenotes.settings.backup.models.BackupApiModel
import sg.acktor.android.stickthosenotes.settings.backup.models.BackupJson
import java.io.Writer
import javax.inject.Inject

class NoteBackupWriter @Inject constructor(
    private val json: Json,
) {
    fun writeToFile(writer: Writer, apiModel: BackupApiModel) {
        val jsonData = BackupJson(
            DATABASE_VERSION,
            apiModel.noteDetails,
            apiModel.textContents,
            apiModel.checklistContents.map(ChecklistNote::id),
            apiModel.checklistItems
        )

        val jsonStr = json.encodeToString(jsonData)
        writer.use { it.write(jsonStr) }
    }
}
