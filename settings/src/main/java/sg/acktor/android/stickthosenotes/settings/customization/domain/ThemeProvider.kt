/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.customization.domain

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.edit
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import it.czerwinski.android.hilt.annotations.Bound
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import sg.acktor.android.stickthosenotes.settings.models.AppThemeMode
import javax.inject.Inject

internal const val KEY_UI_THEME = "uiTheme"

interface ThemeProvider {
    val themeState: StateFlow<AppThemeMode>

    suspend fun setCurrentTheme(themeMode: AppThemeMode)
}

@Bound(ActivityRetainedComponent::class)
@ActivityRetainedScoped
class ThemeProviderImpl @Inject constructor(
    private val prefs: SharedPreferences,
) : ThemeProvider {
    private val _themeState: MutableStateFlow<AppThemeMode>
    override val themeState: StateFlow<AppThemeMode>

    init {
        val currentTheme = prefs.getString(KEY_UI_THEME, null)
        val initialThemeMode = AppThemeMode.entries.firstOrNull { it.strValue == currentTheme }
            ?: generateDefaultMode()

        _themeState = MutableStateFlow(initialThemeMode)
        themeState = _themeState.asStateFlow()
    }

    override suspend fun setCurrentTheme(themeMode: AppThemeMode) {
        _themeState.update { themeMode }
        saveToPrefs(themeMode)
    }

    private fun generateDefaultMode(): AppThemeMode {
        val defaultMode = when (AppCompatDelegate.getDefaultNightMode()) {
            AppCompatDelegate.MODE_NIGHT_NO -> AppThemeMode.LIGHT
            AppCompatDelegate.MODE_NIGHT_YES -> AppThemeMode.DARK
            else -> AppThemeMode.SYSTEM
        }

        saveToPrefs(defaultMode)
        return defaultMode
    }

    private fun saveToPrefs(themeMode: AppThemeMode) {
        prefs.edit(commit = true) {
            putString(KEY_UI_THEME, themeMode.strValue)
        }
    }
}
