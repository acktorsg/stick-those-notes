/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup.mappers

import kotlinx.serialization.json.Json
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistNote
import sg.acktor.android.stickthosenotes.settings.backup.models.BackupApiModel
import sg.acktor.android.stickthosenotes.settings.backup.models.BackupJson
import sg.acktor.android.stickthosenotes.settings.backup.models.BackupV6Json
import java.io.Reader
import javax.inject.Inject

class NoteBackupReader @Inject constructor(private val backup6To7Mapper: Backup6To7Mapper) {
    fun readFromFile(reader: Reader): BackupApiModel {
        val jsonText = reader.readText()

        val jsonData = decodeAsV7BackupFile(jsonText)
            ?: decodeAsV6BackupFile(jsonText)
            ?: error("Invalid backup file found")

        reader.close()

        return BackupApiModel(
            jsonData.notes,
            jsonData.textNotes,
            jsonData.checklistNotes.map { ChecklistNote(it) },
            jsonData.checklistItems,
        )
    }

    private fun decodeAsV7BackupFile(jsonText: String) = try {
        Json.decodeFromString<BackupJson>(jsonText)
    } catch (e: IllegalArgumentException) {
        e.printStackTrace()
        null
    }

    private fun decodeAsV6BackupFile(jsonText: String) = try {
        val model = Json.decodeFromString<BackupV6Json>(jsonText)
        backup6To7Mapper.map(model)
    } catch (e: IllegalArgumentException) {
        e.printStackTrace()
        null
    }
}
