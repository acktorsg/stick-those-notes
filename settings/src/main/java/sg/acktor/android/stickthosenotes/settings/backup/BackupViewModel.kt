/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import sg.acktor.android.stickthosenotes.pin.api.PinManager
import sg.acktor.android.stickthosenotes.settings.R
import sg.acktor.android.stickthosenotes.settings.backup.ui.BackupUiAction
import sg.acktor.android.stickthosenotes.settings.backup.ui.BackupUiEvent
import sg.acktor.android.stickthosenotes.settings.backup.usecases.BackupDbUseCase
import sg.acktor.android.stickthosenotes.settings.backup.usecases.RestoreDbUseCase
import javax.inject.Inject

@HiltViewModel
class BackupViewModel @Inject constructor(
    @ApplicationContext
    private val context: Context,
    private val contentResolver: ContentResolver,
    private val backupDbUseCase: BackupDbUseCase,
    private val restoreDbUseCase: RestoreDbUseCase,
    private val pinManager: PinManager,
) : ViewModel() {
    private val _uiEventChannel = Channel<BackupUiEvent?>()
    val uiEventState = _uiEventChannel.receiveAsFlow()

    private var backupUri: Uri? = null

    fun processUiAction(action: BackupUiAction) {
        when (action) {
            BackupUiAction.CreateBackup -> createBackupFile()
            is BackupUiAction.BackupFileCreated -> writeToBackupFile(action)
            BackupUiAction.LoadBackup -> loadBackupFile()
            is BackupUiAction.BackupFileSelected -> showLoadBackupWarning(action)
            BackupUiAction.LoadBackupConfirmed -> readFromBackupFile()
        }
    }

    private fun createBackupFile() {
        val fileName = backupDbUseCase.generateBackupFileName()

        viewModelScope.launch {
            _uiEventChannel.send(BackupUiEvent.CreateBackupFile(fileName))
        }
    }

    private fun writeToBackupFile(action: BackupUiAction.BackupFileCreated) {
        viewModelScope.launch(
            Dispatchers.IO + CoroutineExceptionHandler { _, throwable ->
                throwable.printStackTrace()
                viewModelScope.launch {
                    _uiEventChannel.send(
                        BackupUiEvent.ShowMessage(
                            context.getString(R.string.message_error_completeBackup)
                        )
                    )
                }
            }
        ) {
            backupDbUseCase.createBackup(contentResolver, action.uri)
            _uiEventChannel.send(
                BackupUiEvent.ShowMessage(context.getString(R.string.message_success_backupNotes))
            )
        }
    }

    private fun loadBackupFile() {
        viewModelScope.launch {
            _uiEventChannel.send(BackupUiEvent.SelectBackupFile)
        }
    }

    private fun showLoadBackupWarning(action: BackupUiAction.BackupFileSelected) {
        backupUri = action.uri
        viewModelScope.launch {
            _uiEventChannel.send(BackupUiEvent.ShowLoadBackupWarning)
        }
    }

    private fun readFromBackupFile() {
        viewModelScope.launch(
            CoroutineExceptionHandler { _, throwable ->
                throwable.printStackTrace()

                viewModelScope.launch {
                    _uiEventChannel.send(
                        BackupUiEvent.ShowMessage(
                            context.getString(R.string.message_error_loadBackup)
                        )
                    )
                }
            }
        ) {
            val uri = backupUri
            if (uri == null) {
                _uiEventChannel.send(
                    BackupUiEvent.ShowMessage(context.getString(R.string.message_error_loadBackup))
                )
                return@launch
            }

            withContext(Dispatchers.IO) {
                val tempFile = backupDbUseCase.createTempBackupFile()

                var isBackupSuccessful = false
                try {
                    restoreDbUseCase.run(contentResolver, uri)
                    pinManager.restorePins()
                    isBackupSuccessful = true
                } catch (e: Exception) {
                    e.printStackTrace()
                    restoreDbUseCase.run(tempFile)
                } finally {
                    tempFile.delete()
                }

                _uiEventChannel.send(
                    if (isBackupSuccessful) {
                        BackupUiEvent.ShowMessage(
                            context.getString(R.string.message_success_loadBackup)
                        )
                    } else {
                        BackupUiEvent.ShowMessage(
                            context.getString(R.string.message_error_loadBackup)
                        )
                    }
                )
            }
        }
    }
}
