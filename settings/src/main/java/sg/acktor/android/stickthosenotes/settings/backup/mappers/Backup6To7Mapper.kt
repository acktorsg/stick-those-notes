/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup.mappers

import kotlinx.serialization.json.Json
import sg.acktor.android.stickthosenotes.database.api.room.DATABASE_VERSION
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItem
import sg.acktor.android.stickthosenotes.database.api.room.entities.Note
import sg.acktor.android.stickthosenotes.database.api.room.entities.NoteType
import sg.acktor.android.stickthosenotes.database.api.room.entities.NoteV6Content
import sg.acktor.android.stickthosenotes.database.api.room.entities.TextNote
import sg.acktor.android.stickthosenotes.settings.backup.models.BackupJson
import sg.acktor.android.stickthosenotes.settings.backup.models.BackupV6Json
import java.time.Clock
import javax.inject.Inject

class Backup6To7Mapper @Inject constructor(private val clock: Clock) {
    fun map(model: BackupV6Json): BackupJson {
        val notes = mutableListOf<Note>()
        val textNotes = mutableListOf<TextNote>()
        val checklistNotes = mutableListOf<Long>()
        val checklistItems = mutableListOf<ChecklistItem>()
        for (note in model.notes) {
            notes.add(
                Note(
                    note.id,
                    note.title,
                    note.colorPalette,
                    note.isPinned,
                    note.position,
                    clock.millis()
                )
            )

            when (note.contentType) {
                NoteType.TEXT -> {
                    val textContent = Json.decodeFromString<NoteV6Content.Text>(note.content)
                    textNotes.add(TextNote(note.id, textContent.content))
                }

                NoteType.CHECKLIST -> {
                    checklistNotes.add(note.id)

                    val checklistContent = Json.decodeFromString<NoteV6Content.Checklist>(
                        note.content
                    )
                    for ((i, item) in checklistContent.items.withIndex()) {
                        checklistItems.add(
                            ChecklistItem(
                                i + 1L, note.id, item.content, item.isChecked
                            )
                        )
                    }
                }

                else -> {}
            }
        }

        return BackupJson(DATABASE_VERSION, notes, textNotes, checklistNotes, checklistItems)
    }
}
