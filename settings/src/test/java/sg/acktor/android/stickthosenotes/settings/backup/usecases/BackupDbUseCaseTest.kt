package sg.acktor.android.stickthosenotes.settings.backup.usecases

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.os.ParcelFileDescriptor
import io.mockk.Ordering
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.assertThrows
import sg.acktor.android.stickthosenotes.settings.DEFAULT_CLOCK
import sg.acktor.android.stickthosenotes.settings.backup.data.BackupRepository
import sg.acktor.android.stickthosenotes.settings.backup.domain.WriterBuilder
import sg.acktor.android.stickthosenotes.settings.backup.mappers.NoteBackupWriter
import sg.acktor.android.stickthosenotes.settings.backup.models.BackupApiModel
import java.io.File
import java.io.IOException
import java.io.Writer
import kotlin.test.Test
import kotlin.test.assertEquals

private const val MOCK_CACHE_PATH = "cache_path"

class BackupDbUseCaseTest {
    private val parcelFileDescriptor = mockk<ParcelFileDescriptor>(relaxUnitFun = true)
    private val uri = mockk<Uri>()
    private val contentResolver = mockk<ContentResolver> {
        every { openFileDescriptor(uri, "w") } returns parcelFileDescriptor
    }

    private val writer = mockk<Writer>(relaxed = true)
    private val context = mockk<Context> {
        every { cacheDir } returns mockk {
            every { path } returns MOCK_CACHE_PATH
        }

        every { contentResolver } returns this@BackupDbUseCaseTest.contentResolver
    }

    private val repo = mockk<BackupRepository>()
    private val backupWriter = mockk<NoteBackupWriter>(relaxed = true)
    private val writerBuilder = mockk<WriterBuilder> {
        every { build(any<File>()) } returns writer
        every { build(parcelFileDescriptor) } returns writer
    }

    private val sut: BackupDbUseCase =
        BackupDbUseCaseImpl(context, repo, writerBuilder, backupWriter, DEFAULT_CLOCK)

    @Test
    fun `verify backup file name format`() {
        val expected = "stick-those-notes-backup_20010911_090400.json"
        val result = sut.generateBackupFileName()
        assertEquals(expected, result)
    }

    @Test
    fun `verify temp backup file created`() {
        val mockRepoData = BackupApiModel(listOf(), listOf(), listOf(), listOf())
        coEvery { repo.getBackup() } returns mockRepoData

        runTest {
            val expectedFileName = FILE_NAME_TEMP_BACKUP
            val result = sut.createTempBackupFile()

            assertEquals(expectedFileName, result.name)
        }

        coVerify(ordering = Ordering.SEQUENCE) {
            writerBuilder.build(any<File>())
            repo.getBackup()
            backupWriter.writeToFile(any(), mockRepoData)
        }
    }

    @Test
    fun `verify backup written to URI`() {
        val mockRepoData = BackupApiModel(listOf(), listOf(), listOf(), listOf())
        coEvery { repo.getBackup() } returns mockRepoData

        runTest {
            sut.createBackup(contentResolver, uri)
        }

        coVerify(ordering = Ordering.SEQUENCE) {
            contentResolver.openFileDescriptor(uri, "w")
            writerBuilder.build(parcelFileDescriptor)
            repo.getBackup()
            backupWriter.writeToFile(any(), mockRepoData)
            parcelFileDescriptor.close()
        }
    }

    @Test
    fun `verify error thrown when file descriptor is not present`() {
        every { contentResolver.openFileDescriptor(uri, "w") } returns null

        runBlocking {
            assertThrows<IOException> {
                sut.createBackup(contentResolver, uri)
            }
        }
    }
}
