/*
 * Copyright (c) 2023. Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup.mappers

import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItem
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistNote
import sg.acktor.android.stickthosenotes.database.api.room.entities.Note
import sg.acktor.android.stickthosenotes.database.api.room.entities.TextNote
import sg.acktor.android.stickthosenotes.settings.DEFAULT_CLOCK
import sg.acktor.android.stickthosenotes.settings.DEFAULT_TIME_MILLIS
import sg.acktor.android.stickthosenotes.settings.backup.models.BackupApiModel
import java.io.InputStreamReader
import java.io.Reader
import kotlin.test.Test
import kotlin.test.assertEquals

class NoteBackupReaderTest {
    private val expectedResult = BackupApiModel(
        listOf(
            Note(
                id = 1,
                title = "",
                colorPalette = ColorPalette.GREEN,
                isPinned = false,
                position = 1,
                lastUpdated = DEFAULT_TIME_MILLIS,
                isDeleted = false
            ),
            Note(
                id = 2,
                title = "",
                colorPalette = ColorPalette.WHITE,
                isPinned = true,
                position = 2,
                lastUpdated = DEFAULT_TIME_MILLIS,
                isDeleted = false
            ),
        ),
        listOf(
            TextNote(
                id = 1,
                content = ""
            )
        ),
        listOf(
            ChecklistNote(2),
        ),
        listOf(
            ChecklistItem(1, 2, "", false),
            ChecklistItem(2, 2, "", true),
        ),
    )

    private val backupReader = NoteBackupReader(Backup6To7Mapper(DEFAULT_CLOCK))

    @Test
    fun `read from v6 data`() {
        val reader = checkNotNull(readJsonFile("v6_backup.json")) { "Can't open JSON file" }

        val result = backupReader.readFromFile(reader)
        assertEquals(expectedResult, result)
    }

    @Test
    fun `read from v7 backup`() {
        val reader = checkNotNull(readJsonFile("v7_backup.json")) { "Can't open JSON file" }

        val result = backupReader.readFromFile(reader)
        assertEquals(expectedResult, result)
    }

    private fun readJsonFile(fileName: String): Reader? {
        return javaClass.classLoader?.getResourceAsStream(fileName)?.let {
            InputStreamReader(it)
        }
    }
}
