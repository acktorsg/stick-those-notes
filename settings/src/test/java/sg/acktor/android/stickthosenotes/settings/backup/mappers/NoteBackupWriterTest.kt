/*
 * Copyright (c) 2023. Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.settings.backup.mappers

import sg.acktor.android.stickthosenotes.common.di.SharedAppModule
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItem
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistNote
import sg.acktor.android.stickthosenotes.database.api.room.entities.Note
import sg.acktor.android.stickthosenotes.database.api.room.entities.TextNote
import sg.acktor.android.stickthosenotes.settings.DEFAULT_TIME_MILLIS
import sg.acktor.android.stickthosenotes.settings.backup.models.BackupApiModel
import java.io.StringWriter
import kotlin.test.Test
import kotlin.test.assertEquals

class NoteBackupWriterTest {
    private val expectedResult = """
                    {"databaseVersion":7,
                        "notes": [
                            {
                                "id": 0,
                                "title": "",
                                "colorPalette": "GREEN",
                                "isPinned": false,
                                "position": 1,
                                "lastUpdated": ${DEFAULT_TIME_MILLIS},
                                "isDeleted": false
                            },
                            {
                                "id": 1,
                                "title": "",
                                "colorPalette": "WHITE",
                                "isPinned": true,
                                "position": 2,
                                "lastUpdated": ${DEFAULT_TIME_MILLIS},
                                "isDeleted": false
                            }
                        ],
                        "textNotes": [
                            {
                                "id": 0,
                                "content": ""
                            }
                        ],
                        "checklistNotes": [
                            1
                        ],
                        "checklistItems": [
                            {
                                "id": 1,
                                "noteId": 1,
                                "content": "",
                                "isChecked": false
                            },
                            {
                                "id": 2,
                                "noteId": 1,
                                "content": "",
                                "isChecked": true
                            }
                        ]
                    }
                """
        .replace("\n", "")
        .replace(" ", "")

    private val json = SharedAppModule.provideEncodeJson()
    private val backupWriter = NoteBackupWriter(json)

    @Test
    fun `write backup to writer`() {
        val writer = StringWriter()
        val model = BackupApiModel(
            listOf(
                Note(
                    id = 0,
                    title = "",
                    colorPalette = ColorPalette.GREEN,
                    isPinned = false,
                    position = 1,
                    lastUpdated = DEFAULT_TIME_MILLIS,
                    isDeleted = false
                ),
                Note(
                    id = 1,
                    title = "",
                    colorPalette = ColorPalette.WHITE,
                    isPinned = true,
                    position = 2,
                    lastUpdated = DEFAULT_TIME_MILLIS,
                    isDeleted = false
                ),
            ),
            listOf(
                TextNote(
                    id = 0,
                    content = ""
                )
            ),
            listOf(
                ChecklistNote(1),
            ),
            listOf(
                ChecklistItem(1, 1, "", false),
                ChecklistItem(2, 1, "", true),
            ),
        )

        backupWriter.writeToFile(writer, model)
        val result = writer.toString()
        assertEquals(result, expectedResult)
    }
}
