package sg.acktor.android.stickthosenotes.settings.backup

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import sg.acktor.android.stickthosenotes.pin.api.PinManager
import sg.acktor.android.stickthosenotes.settings.R
import sg.acktor.android.stickthosenotes.settings.backup.ui.BackupUiAction
import sg.acktor.android.stickthosenotes.settings.backup.ui.BackupUiEvent
import sg.acktor.android.stickthosenotes.settings.backup.usecases.BackupDbUseCase
import sg.acktor.android.stickthosenotes.settings.backup.usecases.RestoreDbUseCase
import java.io.File
import java.io.IOException
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class BackupViewModelTest {
    companion object {
        @BeforeAll
        @JvmStatic
        fun setUpAll() {
            Dispatchers.setMain(StandardTestDispatcher())
        }

        @AfterAll
        @JvmStatic
        fun tearDownAll() {
            Dispatchers.resetMain()
        }
    }

    private val context = mockk<Context>()
    private val contentResolver = mockk<ContentResolver>()
    private val backupDbUseCase = mockk<BackupDbUseCase>(relaxUnitFun = true)
    private val restoreDbUseCase = mockk<RestoreDbUseCase>(relaxUnitFun = true)
    private val pinManager = mockk<PinManager>(relaxUnitFun = true)

    private val sut =
        BackupViewModel(context, contentResolver, backupDbUseCase, restoreDbUseCase, pinManager)

    @Test
    fun `verify backup file creation when user selects back up option`() = runTest {
        val fileName = "some_file.json"
        val expected = BackupUiEvent.CreateBackupFile(fileName)

        every { backupDbUseCase.generateBackupFileName() } returns fileName

        sut.processUiAction(BackupUiAction.CreateBackup)
        val result = sut.uiEventState.first()
        assertEquals(expected, result)
    }

    @Test
    fun `verify success message shown when backup file is written`() = runTest {
        val message = "Notes backed up successfully"
        val expected = BackupUiEvent.ShowMessage(message)

        val uri = mockk<Uri>()
        every { context.getString(R.string.message_success_backupNotes) } returns message

        sut.processUiAction(BackupUiAction.BackupFileCreated(uri))
        val result = sut.uiEventState.first()
        assertEquals(expected, result)
    }

    @Test
    fun `verify error message shown when write to backup file fails`() = runTest {
        val message = "Failed to complete backup"
        val expected = BackupUiEvent.ShowMessage(message)

        val uri = mockk<Uri>()
        every { context.getString(R.string.message_error_completeBackup) } returns message
        coEvery { backupDbUseCase.createBackup(contentResolver, uri) } throws IOException()


        sut.processUiAction(BackupUiAction.BackupFileCreated(uri))
        val result = sut.uiEventState.first()
        assertEquals(expected, result)
    }

    @Test
    fun `verify backup file loading when user selects load backup option`() = runTest {
        val expected = BackupUiEvent.SelectBackupFile

        sut.processUiAction(BackupUiAction.LoadBackup)
        val result = sut.uiEventState.first()
        assertEquals(expected, result)
    }

    @Test
    fun `verify load backup warning shown`() = runTest {
        val uri = mockk<Uri>()
        val expected = BackupUiEvent.ShowLoadBackupWarning

        sut.processUiAction(BackupUiAction.BackupFileSelected(uri))
        val result = sut.uiEventState.first()
        assertEquals(expected, result)
    }

    @Test
    fun `verify success message shown when backup file is read`() = runTest {
        val message = "Notes restored successfully"
        val expected = BackupUiEvent.ShowMessage(message)

        val uri = mockk<Uri>()
        every { context.getString(R.string.message_success_loadBackup) } returns message
        val tempFile = mockk<File> {
            every { delete() } returns true
        }
        coEvery { backupDbUseCase.createTempBackupFile() } returns tempFile

        sut.processUiAction(BackupUiAction.BackupFileSelected(uri))
        sut.processUiAction(BackupUiAction.LoadBackupConfirmed)
        val result = sut.uiEventState.firstOrNull { it is BackupUiEvent.ShowMessage }
        assertEquals(expected, result)
    }

    @Test
    fun `verify temp file restored if backup file read failed`() = runTest {
        val message = "Failed to load from backup file. Restoring previous notes..."
        val expected = BackupUiEvent.ShowMessage(message)

        val uri = mockk<Uri>()
        every { context.getString(R.string.message_error_loadBackup) } returns message
        val tempFile = mockk<File> {
            every { delete() } returns true
        }
        coEvery { backupDbUseCase.createTempBackupFile() } returns tempFile
        coEvery { restoreDbUseCase.run(contentResolver, uri) } throws IOException()

        sut.processUiAction(BackupUiAction.BackupFileSelected(uri))
        sut.processUiAction(BackupUiAction.LoadBackupConfirmed)
        val result = sut.uiEventState.firstOrNull { it is BackupUiEvent.ShowMessage }
        assertEquals(expected, result)
    }

    @Test
    fun `verify error message shown if unexpected error occurs`() = runTest {
        val message = "Failed to load from backup file. Restoring previous notes..."
        val expected = BackupUiEvent.ShowMessage(message)

        val uri = mockk<Uri>()
        every { context.getString(R.string.message_error_loadBackup) } returns message
        coEvery { backupDbUseCase.createTempBackupFile() } throws IOException()

        sut.processUiAction(BackupUiAction.BackupFileSelected(uri))
        sut.processUiAction(BackupUiAction.LoadBackupConfirmed)
        val result = sut.uiEventState.firstOrNull { it is BackupUiEvent.ShowMessage }
        assertEquals(expected, result)
    }
}
