package sg.acktor.android.stickthosenotes.settings

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import sg.acktor.android.stickthosenotes.settings.ui.SettingsUiAction
import sg.acktor.android.stickthosenotes.settings.ui.SettingsUiEvent
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class SettingsViewModelTest {
    private val sut = SettingsViewModel()

    @Test
    fun `verify return to previous screen when back button is clicked`() = runTest {
        Dispatchers.setMain(StandardTestDispatcher(testScheduler))

        val expected = SettingsUiEvent.ReturnToPreviousScreen
        sut.processUiAction(SettingsUiAction.BackButtonClicked)
        val result = sut.uiEventChannel.first()
        assertEquals(expected, result)

        Dispatchers.resetMain()
    }
}
