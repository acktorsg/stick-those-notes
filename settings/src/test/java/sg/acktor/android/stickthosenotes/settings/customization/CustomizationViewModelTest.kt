package sg.acktor.android.stickthosenotes.settings.customization

import app.cash.turbine.test
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import sg.acktor.android.stickthosenotes.settings.customization.domain.ThemeProvider
import sg.acktor.android.stickthosenotes.settings.customization.ui.CustomizationUiAction
import sg.acktor.android.stickthosenotes.settings.customization.ui.CustomizationUiModel
import sg.acktor.android.stickthosenotes.settings.models.AppThemeMode
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class CustomizationViewModelTest {
    companion object {
        @BeforeAll
        @JvmStatic
        fun setUpAll() {
            Dispatchers.setMain(StandardTestDispatcher())
        }

        @AfterAll
        @JvmStatic
        fun tearDownAll() {
            Dispatchers.resetMain()
        }
    }

    private val themeModeState = MutableStateFlow(AppThemeMode.SYSTEM)
    private val themeProvider = mockk<ThemeProvider>(relaxUnitFun = true) {
        every { themeState } returns themeModeState
    }
    private val sut = CustomizationViewModel(themeProvider)

    @Test
    fun `verify initial state`() = runTest {
        val expected = CustomizationUiModel("System default", false)
        val result = sut.uiState.first()
        assertEquals(expected, result)
    }

    @Test
    fun `show theme options when setting is clicked`() = runTest {
        val expected = true
        sut.processUiAction(CustomizationUiAction.ChangeTheme)
        val result = sut.uiState.first().showPopup
        assertEquals(expected, result)
    }

    @Test
    fun `hide theme options when dismissed`() = runTest {
        val expected = false

        sut.uiState.test {
            skipItems(1)
            sut.processUiAction(CustomizationUiAction.ChangeTheme)
            skipItems(1)

            sut.processUiAction(CustomizationUiAction.HidePopup)
            val result = awaitItem().showPopup
            assertEquals(expected, result)
        }
    }

    @Test
    fun `verify theme state updated when theme is selected`() = runTest {
        val expected = false

        val themeMode = AppThemeMode.DARK
        sut.processUiAction(CustomizationUiAction.ThemeSelected(themeMode))
        val result = sut.uiState.first().showPopup
        assertEquals(expected, result)

        coVerify {
            themeProvider.setCurrentTheme(themeMode)
        }
    }
}
