package sg.acktor.android.stickthosenotes.settings.backup.usecases

import android.content.ContentResolver
import android.net.Uri
import android.os.ParcelFileDescriptor
import io.mockk.Ordering
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.assertThrows
import sg.acktor.android.stickthosenotes.settings.backup.data.BackupRepository
import sg.acktor.android.stickthosenotes.settings.backup.domain.ReaderBuilder
import sg.acktor.android.stickthosenotes.settings.backup.mappers.NoteBackupReader
import sg.acktor.android.stickthosenotes.settings.backup.models.BackupApiModel
import java.io.File
import java.io.IOException
import java.io.Reader
import kotlin.test.Test

class RestoreDbUseCaseTest {
    private val uri = mockk<Uri>()
    private val repoContent = BackupApiModel(listOf(), listOf(), listOf(), listOf())

    private val reader = mockk<Reader>()
    private val parcelFileDescriptor = mockk<ParcelFileDescriptor>(relaxUnitFun = true)
    private val contentResolver = mockk<ContentResolver> {
        every { openFileDescriptor(uri, "r") } returns parcelFileDescriptor
    }

    private val repo = mockk<BackupRepository>(relaxUnitFun = true)
    private val readerBuilder = mockk<ReaderBuilder> {
        every { build(any<File>()) } returns reader
        every { build(parcelFileDescriptor) } returns reader
    }
    private val backupReader = mockk<NoteBackupReader> {
        every { readFromFile(reader) } returns repoContent
    }

    private val sut: RestoreDbUseCase = RestoreDbUseCaseImpl(repo, readerBuilder, backupReader)

    @Test
    fun `verify restore from temp file`() {
        runTest {
            sut.run(mockk())
        }

        coVerify(ordering = Ordering.SEQUENCE) {
            readerBuilder.build(any<File>())
            backupReader.readFromFile(reader)
            repo.saveBackup(repoContent)
        }
    }

    @Test
    fun `verify restore from URI`() {
        runTest {
            sut.run(contentResolver, uri)
        }

        coVerify(ordering = Ordering.SEQUENCE) {
            contentResolver.openFileDescriptor(uri, "r")
            readerBuilder.build(parcelFileDescriptor)
            backupReader.readFromFile(reader)
            repo.saveBackup(repoContent)
            parcelFileDescriptor.close()
        }
    }

    @Test
    fun `verify error thrown when file descriptor is not present`() {
        every { contentResolver.openFileDescriptor(uri, "r") } returns null

        runBlocking {
            assertThrows<IOException> {
                sut.run(contentResolver, uri)
            }
        }
    }
}
