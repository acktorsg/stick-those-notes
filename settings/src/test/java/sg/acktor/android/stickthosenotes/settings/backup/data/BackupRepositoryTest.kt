package sg.acktor.android.stickthosenotes.settings.backup.data

import io.mockk.Ordering
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItem
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistNote
import sg.acktor.android.stickthosenotes.database.api.room.entities.Note
import sg.acktor.android.stickthosenotes.database.api.room.entities.TextNote
import sg.acktor.android.stickthosenotes.settings.backup.data.room.BackupNotesDAO
import sg.acktor.android.stickthosenotes.settings.backup.models.BackupApiModel
import kotlin.test.Test
import kotlin.test.assertEquals

class BackupRepositoryTest {
    private val notes = listOf(
        Note(0, "title", ColorPalette.WHITE, true, 0, 2L),
        Note(1, "title", ColorPalette.WHITE, true, 0, 2L),
    )
    private val textNotes = listOf(TextNote(0, "content"))
    private val checklistNotes = listOf(ChecklistNote(1))
    private val checklistItems = listOf(ChecklistItem(0, 1, "content", true))

    private val apiModel = BackupApiModel(notes, textNotes, checklistNotes, checklistItems)

    private val dao = mockk<BackupNotesDAO>(relaxUnitFun = true) {
        coEvery { getAllNotes() } returns notes
        coEvery { getAllTextNotes() } returns textNotes
        coEvery { getAllChecklistNotes() } returns checklistNotes
        coEvery { getAllChecklistItems() } returns checklistItems
    }

    private val sut: BackupRepository = BackupRepository(dao)

    @Test
    fun `verify fetching backup`() {
        runTest {
            val result = sut.getBackup()
            assertEquals(apiModel, result)
        }
    }

    @Test
    fun `verify saving backup`() {
        runTest {
            sut.saveBackup(apiModel)
        }

        coVerify(ordering = Ordering.SEQUENCE) {
            dao.clearAllNotes()
            dao.addAllNotes(apiModel.noteDetails)
            dao.addAllTextNotes(apiModel.textContents)
            dao.addAllChecklistNotes(apiModel.checklistContents)
            dao.addAllChecklistItems(apiModel.checklistItems)
        }
    }
}
