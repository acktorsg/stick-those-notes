package sg.acktor.android.stickthosenotes.settings.customization.domain

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import io.mockk.Ordering
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import sg.acktor.android.stickthosenotes.settings.models.AppThemeMode
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class ThemeProviderTest {
    private val prefsEditor = mockk<SharedPreferences.Editor>(relaxed = true)

    private val prefs = mockk<SharedPreferences> {
        every { getString(KEY_UI_THEME, null) } returns AppThemeMode.SYSTEM.strValue
        every { edit() } returns prefsEditor
    }

    @Test
    fun `verify provider init`() {
        val expected = AppThemeMode.SYSTEM

        val sut = ThemeProviderImpl(prefs)
        val result = sut.themeState.value

        assertEquals(expected, result)

        verify(ordering = Ordering.ORDERED) {
            prefs.getString(KEY_UI_THEME, null)
        }
    }

    @Test
    fun `verify default theme is set if no prefs saved`() {
        every { prefs.getString(KEY_UI_THEME, null) } returns null

        val expected = AppThemeMode.SYSTEM
        val sut = ThemeProviderImpl(prefs)

        val result = sut.themeState.value
        assertEquals(expected, result)

        verify(ordering = Ordering.ORDERED) {
            AppCompatDelegate.getDefaultNightMode()
            prefs.edit()
            prefsEditor.commit()
        }
    }

    @Test
    fun `verify set different theme`() {
        val expected = AppThemeMode.DARK
        val sut = ThemeProviderImpl(prefs)

        runTest {
            Dispatchers.setMain(StandardTestDispatcher(testScheduler))

            sut.setCurrentTheme(AppThemeMode.DARK)
            val result = sut.themeState.value
            assertEquals(expected, result)

            Dispatchers.resetMain()
        }

        coVerify(ordering = Ordering.ORDERED) {
            prefs.edit()
            prefsEditor.commit()
        }
    }
}
