/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.ksp)
    alias(libs.plugins.hilt)
    alias(libs.plugins.composeCompiler)
    alias(libs.plugins.kotlin.serialization)
    jacoco
}

android {
    namespace = "sg.acktor.android.stickthosenotes.settings"
    compileSdk = libs.versions.sdk.max.get().toInt()
    buildToolsVersion = libs.versions.buildTools.get()

    defaultConfig {
        minSdk = libs.versions.sdk.min.get().toInt()
    }

    buildTypes {
        debug {
            enableUnitTestCoverage = true
        }

        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro",
            )
        }

        // Register a JacocoReport task for code coverage analysis
        tasks.register<JacocoReport>("settingsCodeCoverage") {
            // Depend on unit tests and Android tests tasks
            dependsOn(listOf("testDebugUnitTest"))

            // Set task grouping and description
            group = "Reporting"
            description = "Execute UI and unit tests, generate and combine Jacoco coverage report"

            // Configure reports to generate HTML format
            reports {
                html.required.set(true)
            }

            // Set source directories to the main source directory
            sourceDirectories.setFrom(layout.projectDirectory.dir("src/main/java"))

            val compiledExclusions = listOf(
                "**/R.class",
                "**/R\$*.class",
                "**/BuildConfig.*",
                "**/Manifest*.*",
                "**/*Test*.*",
                "**/hilt*",
                "**/Dagger**",
                "**/*Hilt**",
                "**/*_Factory*",
                "**/models/**",
                "**/di/**",
                "**/ui/**",
                "**/WriterBuilder*",
                "**/ReaderBuilder*",
            )

            // Set class directories to compiled Java and Kotlin classes, excluding specified exclusions
            classDirectories.setFrom(
                files(
                    fileTree(layout.buildDirectory.dir("intermediates/classes/debug")) {
                        exclude(compiledExclusions)
                    }
                )
            )

            // Collect execution data from .exec and .ec files generated during test execution
            executionData.setFrom(files(
                fileTree(layout.buildDirectory) {
                    include(listOf("**/*.exec", "**/*.ec"))
                }
            ))
        }
    }

    kotlin {
        jvmToolchain(libs.versions.jvm.get().toInt())
    }

    buildFeatures {
        compose = true
    }

    compileOptions {
        isCoreLibraryDesugaringEnabled = true
    }
}

dependencies {
    coreLibraryDesugaring(libs.desugar)

    implementation(project(":common"))
    implementation(project(":database-api"))
    implementation(project(":pin-api"))

    implementation(libs.acktorsdk)
    implementation(libs.activity.compose)
    implementation(libs.bundles.lifecycle.compose)
    implementation(libs.coroutines)
    implementation(libs.kotlin.serialization.json)
    implementation(libs.navigation.compose)
    implementation(libs.preference)
    implementation(libs.room.runtime)

    // Jetpack Compose
    implementation(platform(libs.compose.bom))
    implementation(libs.bundles.compose)
    implementation(libs.compose.materialIcons.extended)
    debugImplementation(libs.compose.uiTooling)

    // Hilt
    implementation(libs.hilt.android)
    ksp(libs.compiler.hilt.android)
    implementation(libs.hilt.extensions)
    ksp(libs.compiler.hilt.extensions)

    // Unit testing
    testImplementation(kotlin("test"))
    testImplementation(libs.mockk)
    testImplementation(libs.coroutines)
    testImplementation(libs.test.coroutines)
    testImplementation(libs.turbine)
}

tasks.withType<Test> {
    useJUnitPlatform()

    configure<JacocoTaskExtension> {
        isIncludeNoLocationClasses = true
        excludes = listOf("jdk.internal.*")
    }
}
