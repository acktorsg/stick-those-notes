/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.common.theme

import androidx.compose.material3.darkColorScheme
import androidx.compose.ui.graphics.Color

private val ThemeDarkPrimary = Color(0xFFFFB59B)
private val ThemeDarkOnPrimary = Color(0xFF5B1B00)
private val ThemeDarkPrimaryContainer = Color(0xFF7B2E0D)
private val ThemeDarkOnPrimaryContainer = Color(0xFFFFDBCF)
private val ThemeDarkSecondary = Color(0xFFFFB779)
private val ThemeDarkOnSecondary = Color(0xFF4C2700)
private val ThemeDarkSecondaryContainer = Color(0xFF6C3A00)
private val ThemeDarkOnSecondaryContainer = Color(0xFFFFDCC1)
private val ThemeDarkTertiary = Color(0xFFF2BF48)
private val ThemeDarkOnTertiary = Color(0xFF3F2E00)
private val ThemeDarkTertiaryContainer = Color(0xFF5B4300)
private val ThemeDarkOnTertiaryContainer = Color(0xFFFFDF9E)
private val ThemeDarkError = Color(0xFFFFB4AB)
private val ThemeDarkErrorContainer = Color(0xFF93000A)
private val ThemeDarkOnError = Color(0xFF690005)
private val ThemeDarkOnErrorContainer = Color(0xFFFFDAD6)
private val ThemeDarkBackground = Color(0xFF201A18)
private val ThemeDarkOnBackground = Color(0xFFEDE0DC)
private val ThemeDarkSurface = Color(0xFF201A18)
private val ThemeDarkOnSurface = Color(0xFFEDE0DC)
private val ThemeDarkSurfaceVariant = Color(0xFF53433E)
private val ThemeDarkOnSurfaceVariant = Color(0xFFD8C2BB)
private val ThemeDarkOutline = Color(0xFFA08D86)
private val ThemeDarkInverseOnSurface = Color(0xFF201A18)
private val ThemeDarkInverseSurface = Color(0xFFEDE0DC)
private val ThemeDarkInversePrimary = Color(0xFF9A4523)
private val ThemeDarkSurfaceTint = Color(0xFFFFB59B)
private val ThemeDarkOutlineVariant = Color(0xFF53433E)
private val ThemeDarkScrim = Color(0xFF000000)
val ThemeDarkShadow = Color(0xFF000000)

internal val DarkColors = darkColorScheme(
        primary = ThemeDarkPrimary,
        onPrimary = ThemeDarkOnPrimary,
        primaryContainer = ThemeDarkPrimaryContainer,
        onPrimaryContainer = ThemeDarkOnPrimaryContainer,
        secondary = ThemeDarkSecondary,
        onSecondary = ThemeDarkOnSecondary,
        secondaryContainer = ThemeDarkSecondaryContainer,
        onSecondaryContainer = ThemeDarkOnSecondaryContainer,
        tertiary = ThemeDarkTertiary,
        onTertiary = ThemeDarkOnTertiary,
        tertiaryContainer = ThemeDarkTertiaryContainer,
        onTertiaryContainer = ThemeDarkOnTertiaryContainer,
        error = ThemeDarkError,
        errorContainer = ThemeDarkErrorContainer,
        onError = ThemeDarkOnError,
        onErrorContainer = ThemeDarkOnErrorContainer,
        background = ThemeDarkBackground,
        onBackground = ThemeDarkOnBackground,
        surface = ThemeDarkSurface,
        onSurface = ThemeDarkOnSurface,
        surfaceVariant = ThemeDarkSurfaceVariant,
        onSurfaceVariant = ThemeDarkOnSurfaceVariant,
        outline = ThemeDarkOutline,
        inverseOnSurface = ThemeDarkInverseOnSurface,
        inverseSurface = ThemeDarkInverseSurface,
        inversePrimary = ThemeDarkInversePrimary,
        surfaceTint = ThemeDarkSurfaceTint,
        outlineVariant = ThemeDarkOutlineVariant,
        scrim = ThemeDarkScrim,
)
