/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.common.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import sg.acktor.acktorsdk.ui.compose.NunitoTypography
import sg.acktor.android.stickthosenotes.common.LocalNoteThemes
import sg.acktor.android.stickthosenotes.common.models.DarkNoteThemes
import sg.acktor.android.stickthosenotes.common.models.LightNoteThemes

@Composable
fun StickThoseNotesTheme(
        isDarkTheme: Boolean = isSystemInDarkTheme(),
        content: @Composable () -> Unit
) {
    MaterialTheme(
            colorScheme = if (isDarkTheme) DarkColors else LightColors,
            typography = NunitoTypography,
            content = {
                CompositionLocalProvider(
                        LocalNoteThemes provides (if (isDarkTheme) {
                            DarkNoteThemes()
                        } else {
                            LightNoteThemes()
                        }),
                        content = content,
                )
            }
    )
}
