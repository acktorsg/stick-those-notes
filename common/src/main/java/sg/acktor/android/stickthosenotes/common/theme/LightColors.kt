/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.common.theme

import androidx.compose.material3.lightColorScheme
import androidx.compose.ui.graphics.Color

private val ThemeLightPrimary = Color(0xFF9A4523)
private val ThemeLightOnPrimary = Color(0xFFFFFFFF)
private val ThemeLightPrimaryContainer = Color(0xFFFFDBCF)
private val ThemeLightOnPrimaryContainer = Color(0xFF380D00)
private val ThemeLightSecondary = Color(0xFF8E4E00)
private val ThemeLightOnSecondary = Color(0xFFFFFFFF)
private val ThemeLightSecondaryContainer = Color(0xFFFFDCC1)
private val ThemeLightOnSecondaryContainer = Color(0xFF2E1500)
private val ThemeLightTertiary = Color(0xFF795900)
private val ThemeLightOnTertiary = Color(0xFFFFFFFF)
private val ThemeLightTertiaryContainer = Color(0xFFFFDF9E)
private val ThemeLightOnTertiaryContainer = Color(0xFF261A00)
private val ThemeLightError = Color(0xFFBA1A1A)
private val ThemeLightErrorContainer = Color(0xFFFFDAD6)
private val ThemeLightOnError = Color(0xFFFFFFFF)
private val ThemeLightOnErrorContainer = Color(0xFF410002)
private val ThemeLightBackground = Color(0xFFFFFBFF)
private val ThemeLightOnBackground = Color(0xFF201A18)
private val ThemeLightSurface = Color(0xFFFFFBFF)
private val ThemeLightOnSurface = Color(0xFF201A18)
private val ThemeLightSurfaceVariant = Color(0xFFF5DED6)
private val ThemeLightOnSurfaceVariant = Color(0xFF53433E)
private val ThemeLightOutline = Color(0xFF85736D)
private val ThemeLightInverseOnSurface = Color(0xFFFBEEEA)
private val ThemeLightInverseSurface = Color(0xFF362F2C)
private val ThemeLightInversePrimary = Color(0xFFFFB59B)
private val ThemeLightSurfaceTint = Color(0xFF9A4523)
private val ThemeLightOutlineVariant = Color(0xFFD8C2BB)
private val ThemeLightScrim = Color(0xFF000000)
val ThemeLightShadow = Color(0xFF000000)

internal val LightColors = lightColorScheme(
        primary = ThemeLightPrimary,
        onPrimary = ThemeLightOnPrimary,
        primaryContainer = ThemeLightPrimaryContainer,
        onPrimaryContainer = ThemeLightOnPrimaryContainer,
        secondary = ThemeLightSecondary,
        onSecondary = ThemeLightOnSecondary,
        secondaryContainer = ThemeLightSecondaryContainer,
        onSecondaryContainer = ThemeLightOnSecondaryContainer,
        tertiary = ThemeLightTertiary,
        onTertiary = ThemeLightOnTertiary,
        tertiaryContainer = ThemeLightTertiaryContainer,
        onTertiaryContainer = ThemeLightOnTertiaryContainer,
        error = ThemeLightError,
        errorContainer = ThemeLightErrorContainer,
        onError = ThemeLightOnError,
        onErrorContainer = ThemeLightOnErrorContainer,
        background = ThemeLightBackground,
        onBackground = ThemeLightOnBackground,
        surface = ThemeLightSurface,
        onSurface = ThemeLightOnSurface,
        surfaceVariant = ThemeLightSurfaceVariant,
        onSurfaceVariant = ThemeLightOnSurfaceVariant,
        outline = ThemeLightOutline,
        inverseOnSurface = ThemeLightInverseOnSurface,
        inverseSurface = ThemeLightInverseSurface,
        inversePrimary = ThemeLightInversePrimary,
        surfaceTint = ThemeLightSurfaceTint,
        outlineVariant = ThemeLightOutlineVariant,
        scrim = ThemeLightScrim,
)
