/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.common.models

import androidx.compose.ui.graphics.Color
import javax.inject.Inject

interface NoteThemes {
    operator fun get(colorPalette: ColorPalette): NoteTheme
}

class NoteTheme internal constructor(
    val darkBgArgb: Int,
    val onDarkBgArgb: Int,
    val lightBgArgb: Int,
    val onLightBgArgb: Int,
) {
    val darkBackground = Color(darkBgArgb)

    val onDarkBackground = Color(onDarkBgArgb)

    val lightBackground = Color(lightBgArgb)

    val onLightBackground = Color(onLightBgArgb)
}

/**
 * Class needs to be injected into NotificationBuilder to provide note colours
 */
class LightNoteThemes @Inject constructor() : NoteThemes {
    override fun get(colorPalette: ColorPalette): NoteTheme {
        return when (colorPalette) {
            ColorPalette.RED -> NoteTheme(
                darkBgArgb = 0xffe57373.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffef9a9a.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.PINK -> NoteTheme(
                darkBgArgb = 0xfff06292.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xfff48fb1.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.PURPLE -> NoteTheme(
                darkBgArgb = 0xffba68c8.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffce93d8.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.DEEP_PURPLE -> NoteTheme(
                darkBgArgb = 0xff9575cd.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffb39ddb.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.INDIGO -> NoteTheme(
                darkBgArgb = 0xff7986cb.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xff9fa8da.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.BLUE -> NoteTheme(
                darkBgArgb = 0xff64b5f6.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xff90caf9.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.LIGHT_BLUE -> NoteTheme(
                darkBgArgb = 0xff4fc3f7.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xff81d4fa.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.CYAN -> NoteTheme(
                darkBgArgb = 0xff4dd0e1.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xff80deea.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.TEAL -> NoteTheme(
                darkBgArgb = 0xff4db6ac.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xff80cbc4.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.GREEN -> NoteTheme(
                darkBgArgb = 0xff81c784.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffa5d6a7.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.LIGHT_GREEN -> NoteTheme(
                darkBgArgb = 0xffaed581.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffc5e1a5.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.LIME -> NoteTheme(
                darkBgArgb = 0xffdce775.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffe6ee9c.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.YELLOW -> NoteTheme(
                darkBgArgb = 0xffffee58.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xfffff59d.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.AMBER -> NoteTheme(
                darkBgArgb = 0xffffd54f.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffffe082.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.ORANGE -> NoteTheme(
                darkBgArgb = 0xffffb74d.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffffcc80.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.DEEP_ORANGE -> NoteTheme(
                darkBgArgb = 0xffff8a65.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffffab91.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.BROWN -> NoteTheme(
                darkBgArgb = 0xffa1887f.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffbcaaa4.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.GREY -> NoteTheme(
                darkBgArgb = 0xffbdbdbd.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffe0e0e0.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.BLUE_GREY -> NoteTheme(
                darkBgArgb = 0xff90a4ae.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffb0bec5.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.BLACK -> NoteTheme(
                darkBgArgb = 0xff424242.toInt(),
                onDarkBgArgb = 0xfff0f0f0.toInt(),
                lightBgArgb = 0xff757575.toInt(),
                onLightBgArgb = 0xfff0f0f0.toInt(),
            )

            ColorPalette.WHITE -> NoteTheme(
                darkBgArgb = 0xffe0e0e0.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xfff5f5f5.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )
        }
    }
}

internal class DarkNoteThemes : NoteThemes {
    override fun get(colorPalette: ColorPalette): NoteTheme {
        return when (colorPalette) {
            ColorPalette.RED -> NoteTheme(
                darkBgArgb = 0xffb71c1c.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xffc62828.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.PINK -> NoteTheme(
                darkBgArgb = 0xff880e4f.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xffad1457.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.PURPLE -> NoteTheme(
                darkBgArgb = 0xff4a148c.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xff6a1b9a.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.DEEP_PURPLE -> NoteTheme(
                darkBgArgb = 0xff311b92.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xff4527a0.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.INDIGO -> NoteTheme(
                darkBgArgb = 0xff1a237e.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xff283593.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.BLUE -> NoteTheme(
                darkBgArgb = 0xff0d47a1.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xff1565c0.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.LIGHT_BLUE -> NoteTheme(
                darkBgArgb = 0xff01579b.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xff0277bd.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.CYAN -> NoteTheme(
                darkBgArgb = 0xff006064.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xff00838f.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.TEAL -> NoteTheme(
                darkBgArgb = 0xff004d40.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xff00695c.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.GREEN -> NoteTheme(
                darkBgArgb = 0xff1b5e20.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xff2e7d32.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.LIGHT_GREEN -> NoteTheme(
                darkBgArgb = 0xff33691e.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xff558b2f.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.LIME -> NoteTheme(
                darkBgArgb = 0xff827717.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xff9e9d24.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.YELLOW -> NoteTheme(
                darkBgArgb = 0xfff57f17.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xfff9a825.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.AMBER -> NoteTheme(
                darkBgArgb = 0xffff6f00.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffff8f00.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.ORANGE -> NoteTheme(
                darkBgArgb = 0xffe65100.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffef6c00.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.DEEP_ORANGE -> NoteTheme(
                darkBgArgb = 0xffbf360c.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffd84315.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )

            ColorPalette.BROWN -> NoteTheme(
                darkBgArgb = 0xff3e2723.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xff4e342e.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.GREY -> NoteTheme(
                darkBgArgb = 0xff434343.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xff5a5a5a.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.BLUE_GREY -> NoteTheme(
                darkBgArgb = 0xff263238.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xff37474f.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.BLACK -> NoteTheme(
                darkBgArgb = 0xff232323.toInt(),
                onDarkBgArgb = 0xfffafafa.toInt(),
                lightBgArgb = 0xff343434.toInt(),
                onLightBgArgb = 0xfffafafa.toInt(),
            )

            ColorPalette.WHITE -> NoteTheme(
                darkBgArgb = 0xff9e9e9e.toInt(),
                onDarkBgArgb = 0xff212121.toInt(),
                lightBgArgb = 0xffbdbdbd.toInt(),
                onLightBgArgb = 0xff212121.toInt(),
            )
        }
    }
}
