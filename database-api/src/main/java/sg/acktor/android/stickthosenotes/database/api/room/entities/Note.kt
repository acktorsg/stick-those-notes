/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.database.api.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import kotlinx.serialization.Serializable
import sg.acktor.android.stickthosenotes.common.models.ColorPalette

@Entity
@Serializable
data class Note(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    @ColumnInfo(defaultValue = "")
    val title: String,
    val colorPalette: ColorPalette,
    @ColumnInfo(defaultValue = "0")
    val isPinned: Boolean,
    val position: Int,
    val lastUpdated: Long,
    @ColumnInfo(defaultValue = "0")
    val isDeleted: Boolean = false
)

@Entity(
    foreignKeys = [ForeignKey(
        entity = Note::class,
        parentColumns = ["id"],
        childColumns = ["id"],
        onDelete = ForeignKey.CASCADE
    )]
)
@Serializable
data class TextNote(
    @PrimaryKey
    val id: Long,
    @ColumnInfo(defaultValue = "")
    val content: String
)

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = Note::class,
            parentColumns = ["id"],
            childColumns = ["id"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
@Serializable
data class ChecklistNote(
    @PrimaryKey
    val id: Long
)

@Entity(
    primaryKeys = ["id", "noteId"],
    foreignKeys = [
        ForeignKey(
            entity = ChecklistNote::class,
            parentColumns = ["id"],
            childColumns = ["noteId"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
@Serializable
data class ChecklistItem(
    val id: Long,
    @ColumnInfo(index = true)
    val noteId: Long,
    @ColumnInfo(defaultValue = "")
    val content: String,
    @ColumnInfo(defaultValue = "0")
    val isChecked: Boolean
)
