/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.database.api.room.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

sealed class NoteV6Content {
    @NoteType
    abstract val type: Int
    abstract fun asString(): String
    abstract fun isBlank(): Boolean
    abstract fun containsKeyword(keyword: String): Boolean

    @Serializable
    class Text(var content: String = "") : NoteV6Content() {
        override val type = NoteType.TEXT

        override fun asString() = content

        override fun isBlank() = content.isBlank()

        override fun containsKeyword(keyword: String) = content.contains(keyword, ignoreCase = true)
    }

    @Serializable
    class Checklist(
        @SerialName("checklist")
        val items: MutableList<ChecklistItemV6> = mutableListOf(ChecklistItemV6())
    )
        : NoteV6Content() {
        override val type = NoteType.CHECKLIST

        override fun asString() = items.joinToString(separator = "\n") { checklistItem ->
            "[${if (checklistItem.isChecked) "\u2713" else "   "}] ${checklistItem.content}"
        }

        override fun isBlank() = if (items.size == 1) items[0].content.isBlank() else items.isEmpty()

        override fun containsKeyword(keyword: String) = items.any {
            it.content.contains(keyword, ignoreCase = true)
        }
    }
}

@Serializable
data class ChecklistItemV6(var content: String = "", var isChecked: Boolean = false)
