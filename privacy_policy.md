# Privacy Policy
By using the Stick Those Notes! app, you agree to the privacy policy outlined below.

## Collection of Personal Information
The Stick Those Notes! app does not collect, transmit or share any information, personal or otherwise. It does not use any third-party analytics or advertising frameworks.

## Email
If you email us for support or other feedback, the emails with email addresses will be retained for quality assurance purposes. The email addresses will be used only to reply to the concerns or suggestions raised and will never be used for any marketing purpose.

## Disclosure of Personal Information
We will not disclose your information to any third party unless you expressly consent or where required by law.

---

If you have any questions about this privacy policy, email us at acktor.sg@gmail.com