package sg.acktor.android.stickthosenotes.search

import io.mockk.Ordering
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import sg.acktor.android.stickthosenotes.notelist.api.domain.NoteListTypeStateProvider
import sg.acktor.android.stickthosenotes.notelist.api.models.NoteListType
import sg.acktor.android.stickthosenotes.search.ui.SearchUiAction
import sg.acktor.android.stickthosenotes.search.ui.SearchUiEvent
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class SearchViewModelTest {
    companion object {
        @BeforeAll
        @JvmStatic
        fun setUpAll() {
            Dispatchers.setMain(StandardTestDispatcher())
        }

        @AfterAll
        @JvmStatic
        fun tearDownAll() {
            Dispatchers.resetMain()
        }
    }

    private val listTypeStateProvider = mockk<NoteListTypeStateProvider>(relaxUnitFun = true)

    private val sut = SearchViewModel(listTypeStateProvider)

    @Test
    fun `verify query state updated when query changes`() = runTest {
        val expected = "new query"

        sut.processUiAction(SearchUiAction.SubmitQuery(expected))
        val result = sut.queryState.first()
        assertEquals(expected, result)

        advanceTimeBy(300)
        verify(ordering = Ordering.ORDERED) {
            listTypeStateProvider.setState(NoteListType.Search(expected))
        }
    }

    @Test
    fun `verify list type is reset & return to previous screen on back button click`() = runTest {
        val expected = SearchUiEvent.ReturnToPreviousScreen

        sut.processUiAction(SearchUiAction.BackButtonClicked)
        val result = sut.uiEventState.first()
        assertEquals(expected, result)

        verify(ordering = Ordering.ORDERED) {
            listTypeStateProvider.setState(NoteListType.Home)
        }
    }

    @Test
    fun `verify message shown when received from note list`() = runTest {
        val message = "snackbar message"
        val expected = SearchUiEvent.ShowSnackbar(message)

        sut.processUiAction(SearchUiAction.NoteListMessageReceived(message))
        val result = sut.uiEventState.first()
        assertEquals(expected, result)
    }
}
