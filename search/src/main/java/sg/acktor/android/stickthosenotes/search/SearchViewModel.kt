/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import sg.acktor.android.stickthosenotes.notelist.api.domain.NoteListTypeStateProvider
import sg.acktor.android.stickthosenotes.notelist.api.models.NoteListType
import sg.acktor.android.stickthosenotes.search.ui.SearchUiAction
import sg.acktor.android.stickthosenotes.search.ui.SearchUiEvent
import javax.inject.Inject

@OptIn(FlowPreview::class)
@HiltViewModel
class SearchViewModel @Inject constructor(
    private val noteListTypeStateProvider: NoteListTypeStateProvider,
) : ViewModel() {
    private val _queryState = MutableStateFlow("")
    val queryState get() = _queryState.asStateFlow()

    private val _uiEventChannel = MutableSharedFlow<SearchUiEvent?>()
    val uiEventState = _uiEventChannel.asSharedFlow()

    init {
        _queryState.debounce(250)
            .onEach { query ->
                noteListTypeStateProvider.setState(NoteListType.Search(query))
            }
            .launchIn(viewModelScope)
    }

    fun processUiAction(action: SearchUiAction) {
        when (action) {
            is SearchUiAction.SubmitQuery -> submitQuery(action)
            SearchUiAction.BackButtonClicked -> returnToPreviousScreen()
            is SearchUiAction.NoteListMessageReceived -> showNoteListMessage(action.message)
        }
    }

    private fun submitQuery(action: SearchUiAction.SubmitQuery) {
        _queryState.update { action.query }
    }

    private fun returnToPreviousScreen() {
        noteListTypeStateProvider.setState(NoteListType.Home)

        viewModelScope.launch {
            _uiEventChannel.emit(SearchUiEvent.ReturnToPreviousScreen)
        }
    }

    private fun showNoteListMessage(message: String) {
        viewModelScope.launch {
            _uiEventChannel.emit(SearchUiEvent.ShowSnackbar(message))
        }
    }
}
