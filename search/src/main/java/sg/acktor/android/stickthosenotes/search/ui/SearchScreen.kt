/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.search.ui

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.ArrowBack
import androidx.compose.material.icons.rounded.Clear
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import sg.acktor.android.stickthosenotes.common.LocalRootNavigator
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.notelist.api.ui.NoteListUiProvider
import sg.acktor.android.stickthosenotes.search.R
import sg.acktor.android.stickthosenotes.search.SearchViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchScreen(
    searchViewModel: SearchViewModel = hiltViewModel(),
    noteListUiProvider: NoteListUiProvider,
) {
    val rootNavigator = LocalRootNavigator.current
    val snackbarHostState = remember { SnackbarHostState() }
    val query by searchViewModel.queryState.collectAsStateWithLifecycle()

    LaunchedEffect(Unit) {
        searchViewModel.uiEventState.collect { uiEvent ->
            when (uiEvent) {
                is SearchUiEvent.ShowSnackbar -> snackbarHostState.showSnackbar(uiEvent.message)
                SearchUiEvent.ReturnToPreviousScreen -> rootNavigator.returnToHomeScreen()
                null -> {}
            }
        }
    }

    BackHandler {
        searchViewModel.processUiAction(SearchUiAction.BackButtonClicked)
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    SearchTopBar(
                        query = query,
                        onQueryChange = {
                            searchViewModel.processUiAction(SearchUiAction.SubmitQuery(it))
                        }
                    )
                },
                navigationIcon = {
                    IconButton(
                        onClick = {
                            searchViewModel.processUiAction(SearchUiAction.BackButtonClicked)
                        }
                    ) {
                        Icon(
                            Icons.AutoMirrored.Rounded.ArrowBack,
                            contentDescription = "Back to home"
                        )
                    }
                }
            )
        },
        snackbarHost = { SnackbarHost(snackbarHostState) }
    ) {
        noteListUiProvider.NoteList(
            modifier = Modifier.padding(top = it.calculateTopPadding()),
            showSnackbarMessage = { message ->
                searchViewModel.processUiAction(SearchUiAction.NoteListMessageReceived(message))
            }
        )
    }
}

@Composable
private fun SearchTopBar(
    query: String,
    onQueryChange: (String) -> Unit
) {
    val focusRequester = remember { FocusRequester() }

    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }

    TextField(
        modifier = Modifier
            .fillMaxWidth()
            .focusRequester(focusRequester),
        value = query,
        onValueChange = onQueryChange,
        singleLine = true,
        textStyle = MaterialTheme.typography.bodyLarge,
        placeholder = {
            Text(stringResource(R.string.hint_editText_search))
        },
        colors = TextFieldDefaults.colors(
            unfocusedContainerColor = Color.Transparent,
            focusedContainerColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
        ),
        trailingIcon = if (query.isNotBlank()) {
            {
                IconButton(onClick = { onQueryChange("") }) {
                    Icon(
                        Icons.Rounded.Clear,
                        contentDescription = "Clear search field"
                    )
                }
            }
        } else null
    )
}

@Preview(showBackground = true)
@Composable
private fun BlankSearchBarPreview() {
    StickThoseNotesTheme {
        SearchTopBar(query = "", onQueryChange = {})
    }
}

@Preview(showBackground = true)
@Composable
private fun PopulatedSearchBarPreview() {
    StickThoseNotesTheme {
        SearchTopBar(query = "apple", onQueryChange = {})
    }
}
