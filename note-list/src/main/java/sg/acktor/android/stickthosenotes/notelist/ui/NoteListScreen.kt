/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.asPaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.navigationBars
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.items
import androidx.compose.foundation.lazy.staggeredgrid.rememberLazyStaggeredGridState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material.icons.rounded.VerticalAlignTop
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SmallFloatingActionButton
import androidx.compose.material3.Text
import androidx.compose.material3.adaptive.currentWindowAdaptiveInfo
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.window.core.layout.WindowWidthSizeClass
import kotlinx.coroutines.launch
import sg.acktor.acktorsdk.ui.compose.Dimensions
import sg.acktor.android.stickthosenotes.common.LocalRootNavigator
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.notelist.NoteListViewModel
import sg.acktor.android.stickthosenotes.notelist.R
import sg.acktor.android.stickthosenotes.notelist.api.models.NoteListType
import sh.calvin.reorderable.ReorderableItem
import sh.calvin.reorderable.rememberReorderableLazyStaggeredGridState

@Composable
fun NoteListScreen(
    modifier: Modifier = Modifier,
    noteListViewModel: NoteListViewModel = hiltViewModel(),
    showMessage: (String) -> Unit,
) {
    val rootNavigator = LocalRootNavigator.current

    val uiState by noteListViewModel.noteListState.collectAsStateWithLifecycle()

    LaunchedEffect(Unit) {
        noteListViewModel.uiEventChannel.collect { event ->
            when (event) {
                is NoteListUiEvent.OpenNote -> {
                    rootNavigator.showNoteDetailsScreen(event.noteId, event.contentType)
                }

                NoteListUiEvent.OpenCreateNoteDialog -> {
                    rootNavigator.showCreateNoteScreen()
                }

                is NoteListUiEvent.ShowSnackbarMessage -> showMessage(event.message)

                null -> {}
            }
        }
    }

    LaunchedEffect(uiState) {
        if (uiState is NoteListUiState.Loading) {
            noteListViewModel.processUiAction(
                NoteListUiAction.ChangeListType(NoteListType.Home)
            )
        }
    }

    Box(modifier.fillMaxSize()) {
        when (val state = uiState) {
            NoteListUiState.Loading -> {
                LoadingScreen(Modifier.align(Alignment.Center))
            }

            is NoteListUiState.Empty -> {
                EmptyListScreen(
                    message = state.emptyMessage,
                    showAddButton = state.isAddButtonVisible,
                    createNote = {
                        noteListViewModel.processUiAction(NoteListUiAction.CreateNoteSelected)
                    },
                )
            }

            is NoteListUiState.Loaded -> {
                NoteList(
                    state = state,
                    sendUiAction = noteListViewModel::processUiAction,
                )
            }
        }
    }
}

@Composable
private fun BoxScope.NoteList(
    state: NoteListUiState.Loaded,
    sendUiAction: (NoteListUiAction) -> Unit,
) {
    val scope = rememberCoroutineScope()
    val staggeredGridState = rememberLazyStaggeredGridState()
    val reorderableStaggeredGridState = rememberReorderableLazyStaggeredGridState(
        staggeredGridState
    ) { from, to ->
        sendUiAction(NoteListUiAction.MoveNote(from.index, to.index))
    }

    val navigationBarPadding = WindowInsets.navigationBars.asPaddingValues()
        .calculateBottomPadding()

    var addButtonPaddingPx by remember { mutableIntStateOf(0) }
    val bottomPadding = with(LocalDensity.current) {
        addButtonPaddingPx.toDp()
    } + navigationBarPadding

    val windowWidthClass = currentWindowAdaptiveInfo().windowSizeClass.windowWidthSizeClass
    val columnCount = if (windowWidthClass == WindowWidthSizeClass.COMPACT) 2 else 3

    LazyVerticalStaggeredGrid(
        modifier = Modifier.fillMaxSize(),
        contentPadding = PaddingValues(
            start = Dimensions.spacingSmall,
            end = Dimensions.spacingSmall,
            top = Dimensions.spacingSmall,
            bottom = Dimensions.spacingSmall + bottomPadding,
        ),
        horizontalArrangement = Arrangement.spacedBy(Dimensions.spacingSmall),
        verticalItemSpacing = Dimensions.spacingSmall,
        columns = StaggeredGridCells.Fixed(columnCount),
        state = staggeredGridState
    ) {
        items(state.notes, key = NoteListItemUiModel::id) { note ->
            ReorderableItem(reorderableStaggeredGridState, key = note.id) {
                var isDragging by remember { mutableStateOf(false) }
                var scale = remember { Animatable(1f) }

                LaunchedEffect(isDragging) {
                    launch {
                        scale.animateTo(if (isDragging) 1.1f else 1f)
                    }
                }

                NoteListItem(
                    modifier = Modifier
                        .longPressDraggableHandle(
                            enabled = state.isDraggingEnabled,
                            onDragStarted = {
                                isDragging = true
                                sendUiAction(NoteListUiAction.StartNoteMove)
                            },
                            onDragStopped = {
                                isDragging = false
                                sendUiAction(NoteListUiAction.EndNoteMove)
                            },
                        )
                        .scale(scale.value),
                    noteUiModel = note,
                    openNoteDetailsScreen = {
                        sendUiAction(NoteListUiAction.NoteSelected(note.id, note.contentType))
                    },
                )
            }
        }
    }

    Column(
        Modifier
            .align(Alignment.BottomEnd)
            .padding(Dimensions.spacingDefault),
        horizontalAlignment = Alignment.End
    ) {
        AnimatedVisibility(
            visible = staggeredGridState.canScrollBackward,
            enter = scaleIn(),
            exit = scaleOut(),
        ) {
            SmallFloatingActionButton(
                onClick = { scope.launch { staggeredGridState.animateScrollToItem(0) } },
                containerColor = MaterialTheme.colorScheme.tertiaryContainer,
                contentColor = MaterialTheme.colorScheme.onTertiaryContainer
            ) {
                Icon(Icons.Rounded.VerticalAlignTop, contentDescription = "Scroll to top")
            }
        }

        if (state.isAddButtonVisible) {
            val buttonPadding = with(LocalDensity.current) {
                (Dimensions.spacingDefault * 2).toPx().toInt()
            }

            Spacer(Modifier.height(Dimensions.spacingSmall))
            AddNoteButton(
                modifier = Modifier
                    .padding(bottom = navigationBarPadding)
                    .onSizeChanged {
                        if (addButtonPaddingPx == 0) {
                            addButtonPaddingPx = it.height + buttonPadding
                        }
                    },
                onClick = { sendUiAction(NoteListUiAction.CreateNoteSelected) },
                isExpanded = !staggeredGridState.canScrollBackward
                        || staggeredGridState.lastScrolledBackward
                        || !staggeredGridState.canScrollForward,
            )
        }
    }
}

@Composable
private fun LoadingScreen(modifier: Modifier) {
    CircularProgressIndicator(modifier = modifier)
}

@Composable
private fun BoxScope.EmptyListScreen(
    message: String,
    showAddButton: Boolean,
    createNote: () -> Unit,
) {
    Text(
        modifier = Modifier.align(Alignment.Center),
        text = message,
    )

    if (showAddButton) {
        AddNoteButton(
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .padding(Dimensions.spacingDefault)
                .navigationBarsPadding(),
            onClick = createNote,
            isExpanded = true,
        )
    }
}

@Composable
private fun AddNoteButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    isExpanded: Boolean,
) {
    ExtendedFloatingActionButton(
        modifier = modifier,
        text = { Text(stringResource(R.string.title_button_addNote)) },
        icon = { Icon(Icons.Rounded.Add, contentDescription = "Create a new note") },
        onClick = onClick,
        expanded = isExpanded,
    )
}

@Preview(showBackground = true)
@Composable
private fun EmptyListPreview() {
    val state = NoteListUiState.Loaded(
        notes = emptyList(),
        isDraggingEnabled = false,
        isAddButtonVisible = true,
    )

    StickThoseNotesTheme {
        Box(Modifier.fillMaxSize()) {
            NoteList(
                state = state,
                sendUiAction = {},
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun LoadingScreenPreview() {
    StickThoseNotesTheme {
        Box(Modifier.fillMaxSize()) {
            LoadingScreen(Modifier.align(Alignment.Center))
        }
    }
}
