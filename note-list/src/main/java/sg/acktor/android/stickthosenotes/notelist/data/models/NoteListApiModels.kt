/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.data.models

import sg.acktor.android.stickthosenotes.common.models.ColorPalette

sealed class NoteListItemApiModel(
        val id: Long,
        val title: String,
        val colorPalette: ColorPalette,
        val isPinned: Boolean,
        val position: Int,
) {
    class Text(
            id: Long,
            title: String,
            colorPalette: ColorPalette,
            isPinned: Boolean,
            position: Int,
            val content: String,
    ): NoteListItemApiModel(id, title, colorPalette, isPinned, position) {
        override fun equals(other: Any?): Boolean {
            if (other !is Text) return false

            return super.equals(other)
                    && this.content == other.content
        }

        override fun hashCode() = 31 * super.hashCode() + content.hashCode()
    }

    class Checklist(
        id: Long,
        title: String,
        colorPalette: ColorPalette,
        isPinned: Boolean,
        position: Int,
        val content: List<ChecklistItem>,
    ) : NoteListItemApiModel(id, title, colorPalette, isPinned, position) {
        override fun equals(other: Any?): Boolean {
            if (other !is Checklist) return false

            return super.equals(other)
                    && this.content.size == other.content.size
                    && this.content.zip(other.content) { a, b -> a == b }.all { it }
        }

        override fun hashCode(): Int {
            var hashCode = super.hashCode()
            hashCode = 31 * hashCode + content.size.hashCode()
            for (item in content) {
                hashCode = 31 * hashCode + item.hashCode()
            }

            return hashCode
        }
    }

    data class ChecklistItem(val isChecked: Boolean, val content: String)

    override fun equals(other: Any?): Boolean {
        if (other !is NoteListItemApiModel) return false

        return this.id == other.id
                && this.title == other.title
                && this.colorPalette == other.colorPalette
                && this.isPinned == other.isPinned
                && this.position == other.position
    }

    override fun hashCode(): Int {
        var hashCode = id.hashCode()
        hashCode = 31 * hashCode + title.hashCode()
        hashCode = 31 * hashCode + colorPalette.hashCode()
        hashCode = 31 * hashCode + isPinned.hashCode()
        hashCode = 31 * hashCode + position.hashCode()
        return hashCode
    }
}

data class NotePositionApiModel(val id: Long, val position: Int)
