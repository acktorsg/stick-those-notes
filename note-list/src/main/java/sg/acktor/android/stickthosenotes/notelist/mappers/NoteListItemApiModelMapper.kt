/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.mappers

import sg.acktor.android.stickthosenotes.notelist.data.models.ChecklistNoteListRoom
import sg.acktor.android.stickthosenotes.notelist.data.models.NoteListItemApiModel
import sg.acktor.android.stickthosenotes.notelist.data.models.TextNoteListRoom
import javax.inject.Inject

class NoteListItemApiModelMapper @Inject constructor() {
    fun mapTextNotes(notes: Array<TextNoteListRoom>) = notes.map { note ->
        NoteListItemApiModel.Text(
                note.id,
                note.title,
                note.colorPalette,
                note.isPinned,
                note.position,
                note.content
        )
    }

    fun mapChecklistNotes(notes: Array<ChecklistNoteListRoom>) = notes.map { note ->
        NoteListItemApiModel.Checklist(
                note.id,
                note.title,
                note.colorPalette,
                note.isPinned,
                note.position,
                note.items.map { checklistItem ->
                    NoteListItemApiModel.ChecklistItem(
                            checklistItem.isChecked, checklistItem.content
                    )
                }
        )
    }
}
