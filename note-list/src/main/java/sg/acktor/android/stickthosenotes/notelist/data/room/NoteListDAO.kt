/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.data.room

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import kotlinx.coroutines.flow.Flow
import sg.acktor.android.stickthosenotes.database.api.room.entities.Note
import sg.acktor.android.stickthosenotes.database.api.room.entities.NotePosition
import sg.acktor.android.stickthosenotes.notelist.data.models.ChecklistNoteListRoom
import sg.acktor.android.stickthosenotes.notelist.data.models.TextNoteListRoom

@Dao
interface NoteListDAO {
    @Query(
            """
            SELECT n.id, n.title, n.colorPalette, n.isPinned, n.position, t.content
            FROM TextNote t INNER JOIN Note n USING(id)
            WHERE isDeleted = :isDeleted
            ORDER BY position
        """
    )
    fun getAllTextNotes(isDeleted: Boolean): Flow<Array<TextNoteListRoom>>

    @Transaction
    @Query(
            """
            SELECT c.id, n.title, n.colorPalette, n.isPinned, n.position
            FROM ChecklistNote c INNER JOIN Note n USING(id)
            WHERE isDeleted = :isDeleted
            ORDER BY position
        """
    )
    fun getAllChecklistNotes(isDeleted: Boolean): Flow<Array<ChecklistNoteListRoom>>

    @Query(
        """
            SELECT n.id, title, colorPalette, isPinned, position, content
            FROM TextNote t INNER JOIN Note n USING(id)
            WHERE isDeleted = 0
            AND (
                title LIKE '%' || :query || '%'
                OR content LIKE '%' || :query || '%'
            )
        """
    )
    fun searchTextNotes(query: String): Flow<Array<TextNoteListRoom>>

    @Transaction
    @Query(
        """
            SELECT c.id, title, colorPalette, isPinned, position
            FROM ChecklistNote c INNER JOIN Note n USING(id)
            WHERE isDeleted = 0 AND title LIKE '%' || :query || '%'
        """
    )
    fun searchChecklistNotes(query: String): Flow<Array<ChecklistNoteListRoom>>

    @Query(
        """
            SELECT DISTINCT noteId
            FROM ChecklistItem
            WHERE content LIKE '%' || :query || '%'
        """
    )
    fun searchChecklistItems(query: String): Flow<Array<Long>>

    @Query(
        """
            SELECT c.id, title, colorPalette, isPinned, position
            FROM ChecklistNote c INNER JOIN Note n USING(id)
            WHERE c.id IN(:noteIds)
        """
    )
    @Transaction
    fun getChecklistNotes(noteIds: Array<Long>): Flow<Array<ChecklistNoteListRoom>>

    @Update(entity = Note::class)
    suspend fun updateNotePositions(notes: List<NotePosition>)
}
