/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.data

import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import sg.acktor.android.stickthosenotes.database.api.room.entities.NotePosition
import sg.acktor.android.stickthosenotes.notelist.data.models.NoteListItemApiModel
import sg.acktor.android.stickthosenotes.notelist.data.models.NotePositionApiModel
import sg.acktor.android.stickthosenotes.notelist.data.room.NoteListDAO
import sg.acktor.android.stickthosenotes.notelist.mappers.NoteListItemApiModelMapper
import javax.inject.Inject

@ViewModelScoped
class NoteListRepository @Inject constructor(
    private val dao: NoteListDAO,
    private val noteItemMapper: NoteListItemApiModelMapper,
) {
    fun getAllNotes(isDeleted: Boolean) = combine(
        dao.getAllTextNotes(isDeleted).map(noteItemMapper::mapTextNotes),
        dao.getAllChecklistNotes(isDeleted).map(noteItemMapper::mapChecklistNotes)
    ) { textNotes, checklistNotes ->
        buildList {
            addAll(textNotes)
            addAll(checklistNotes)
        }.sortedBy(NoteListItemApiModel::position)
    }.distinctUntilChanged()

    @OptIn(ExperimentalCoroutinesApi::class)
    fun searchNotes(query: String) = combine(
        dao.searchTextNotes(query).map(noteItemMapper::mapTextNotes),
        dao.searchChecklistNotes(query).map(noteItemMapper::mapChecklistNotes),
        dao.searchChecklistItems(query).flatMapLatest { noteIds ->
            dao.getChecklistNotes(noteIds).map { notes ->
                noteItemMapper.mapChecklistNotes(notes).toMutableList()
            }
        }
    ) { textNotes, checklistNotes, checklistItemNotes ->
        buildList {
            addAll(textNotes)
            addAll(checklistNotes)

            val existingIds = checklistNotes.map(NoteListItemApiModel.Checklist::id)
            checklistItemNotes.removeAll { it.id in existingIds }
            addAll(checklistItemNotes)
        }
    }.distinctUntilChanged()

    suspend fun updateNotePositions(notes: List<NotePositionApiModel>) = dao.updateNotePositions(
        notes.map { NotePosition(it.id, it.position) }
    )
}
