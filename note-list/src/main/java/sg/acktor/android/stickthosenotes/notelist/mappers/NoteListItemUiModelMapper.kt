/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.mappers

import sg.acktor.android.stickthosenotes.notelist.data.models.NoteListItemApiModel
import sg.acktor.android.stickthosenotes.notelist.ui.NoteListItemUiModel
import javax.inject.Inject

class NoteListItemUiModelMapper @Inject constructor() {
    fun map(note: NoteListItemApiModel) = when (note) {
        is NoteListItemApiModel.Text -> NoteListItemUiModel.Text(
            note.id,
            note.title,
            note.colorPalette,
            note.isPinned,
            note.content
        )

        is NoteListItemApiModel.Checklist -> NoteListItemUiModel.Checklist(
            note.id,
            note.title,
            note.colorPalette,
            note.isPinned,
            note.content.map { checklistItem ->
                NoteListItemUiModel.ChecklistItem(
                    checklistItem.isChecked, checklistItem.content
                )
            }
        )
    }
}
