/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.ui

import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.notelist.api.models.NoteListType

sealed interface NoteListUiAction {
    data class ChangeListType(val listType: NoteListType) : NoteListUiAction

    data object CreateNoteSelected : NoteListUiAction

    data class NoteSelected(val noteId: Long, val contentType: ContentType) : NoteListUiAction

    data object StartNoteMove : NoteListUiAction

    data class MoveNote(val fromPos: Int, val toPos: Int) : NoteListUiAction

    data object EndNoteMove : NoteListUiAction
}

sealed interface NoteListUiEvent {
    data object OpenCreateNoteDialog : NoteListUiEvent

    data class OpenNote(val noteId: Long, val contentType: ContentType) : NoteListUiEvent

    data class ShowSnackbarMessage(val message: String): NoteListUiEvent
}
