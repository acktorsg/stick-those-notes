/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.usecases

import dagger.hilt.android.components.ViewModelComponent
import it.czerwinski.android.hilt.annotations.Bound
import sg.acktor.android.stickthosenotes.notelist.ui.NoteListItemUiModel
import sg.acktor.android.stickthosenotes.notelist.data.NoteListRepository
import sg.acktor.android.stickthosenotes.notelist.mappers.NoteListApiModelMapper
import javax.inject.Inject

interface MoveNotesUseCase {
    fun updatePositions(
        notes: List<NoteListItemUiModel>,
        fromPos: Int, toPos: Int,
    ): List<NoteListItemUiModel>

    suspend fun updateRepo(notes: List<NoteListItemUiModel>)
}

@Bound(ViewModelComponent::class)
class MoveNotesUseCaseImpl @Inject constructor(
    private val repo: NoteListRepository,
    private val noteItemMapper: NoteListApiModelMapper,
) : MoveNotesUseCase {
    override fun updatePositions(
        notes: List<NoteListItemUiModel>,
        fromPos: Int, toPos: Int,
    ): List<NoteListItemUiModel> = notes.toMutableList().apply {
        add(toPos, removeAt(fromPos))
    }

    override suspend fun updateRepo(notes: List<NoteListItemUiModel>) {
        repo.updateNotePositions(noteItemMapper.mapToPosition(notes))
    }
}
