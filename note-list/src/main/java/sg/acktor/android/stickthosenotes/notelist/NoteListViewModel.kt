/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.notelist.api.domain.NoteListTypeStateProvider
import sg.acktor.android.stickthosenotes.notelist.api.models.NoteListType
import sg.acktor.android.stickthosenotes.notelist.ui.NoteListUiAction
import sg.acktor.android.stickthosenotes.notelist.ui.NoteListUiEvent
import sg.acktor.android.stickthosenotes.notelist.ui.NoteListUiState
import sg.acktor.android.stickthosenotes.notelist.usecases.GetNoteListUseCase
import sg.acktor.android.stickthosenotes.notelist.usecases.MoveNotesUseCase
import javax.inject.Inject

@HiltViewModel
class NoteListViewModel @Inject constructor(
    @ApplicationContext
    private val context: Context,
    private val getNoteListUseCase: GetNoteListUseCase,
    private val moveNotesUseCase: MoveNotesUseCase,
    private val noteListTypeStateProvider: NoteListTypeStateProvider,
) : ViewModel() {
    private val _noteListState = MutableStateFlow<NoteListUiState>(NoteListUiState.Loading)
    val noteListState get() = _noteListState.asStateFlow()

    private val _uiEventChannel = Channel<NoteListUiEvent?>()
    val uiEventChannel = _uiEventChannel.receiveAsFlow()

    private var isListUpdateEnabled = true

    private var listFetchJob: Job? = null

    init {
        noteListTypeStateProvider.state
            .onEach { listType ->
                val isHomeScreen = listType == NoteListType.Home

                listFetchJob?.cancel()
                listFetchJob = getNoteListUseCase.run(listType)
                    .filter { isListUpdateEnabled }
                    .onEach { uiModels ->
                        _noteListState.update {
                            if (uiModels.isEmpty()) {
                                NoteListUiState.Empty(
                                    emptyMessage = context.getString(
                                        when (listType) {
                                            NoteListType.Home -> R.string.message_empty_notes
                                            NoteListType.RecycleBin -> R.string.message_empty_deletedNotes
                                            is NoteListType.Search -> R.string.message_empty_search
                                        }
                                    ),
                                    isAddButtonVisible = isHomeScreen,
                                )
                            } else {
                                NoteListUiState.Loaded(
                                    notes = uiModels,
                                    isDraggingEnabled = isHomeScreen,
                                    isAddButtonVisible = isHomeScreen,
                                )
                            }
                        }
                    }
                    .catch { e ->
                        e.printStackTrace()
                        _noteListState.update {
                            NoteListUiState.Empty(
                                emptyMessage = context.getString(R.string.message_empty_notes),
                                isAddButtonVisible = isHomeScreen,
                            )
                        }
                        _uiEventChannel.send(
                            NoteListUiEvent.ShowSnackbarMessage(
                                context.getString(R.string.message_error_loadNotes)
                            )
                        )
                    }
                    .launchIn(viewModelScope)
            }
            .launchIn(viewModelScope)
    }

    override fun onCleared() {
        listFetchJob?.cancel()
    }

    fun processUiAction(action: NoteListUiAction) {
        when (action) {
            is NoteListUiAction.ChangeListType -> changeListType(action.listType)
            NoteListUiAction.CreateNoteSelected -> openCreateNoteDialog()
            is NoteListUiAction.NoteSelected -> openNote(action.noteId, action.contentType)
            NoteListUiAction.StartNoteMove -> disableNoteUpdate()
            is NoteListUiAction.MoveNote -> moveNote(action.fromPos, action.toPos)
            NoteListUiAction.EndNoteMove -> enableNoteUpdates()
        }
    }

    private fun changeListType(listType: NoteListType) {
        noteListTypeStateProvider.setState(listType)
    }

    private fun openCreateNoteDialog() {
        viewModelScope.launch {
            _uiEventChannel.send(NoteListUiEvent.OpenCreateNoteDialog)
        }
    }

    private fun openNote(noteId: Long, contentType: ContentType) {
        viewModelScope.launch {
            _uiEventChannel.send(NoteListUiEvent.OpenNote(noteId, contentType))
        }
    }

    private fun disableNoteUpdate() {
        isListUpdateEnabled = false
    }

    private fun moveNote(fromPos: Int, toPos: Int) {
        if (fromPos == toPos) return

        val uiModel = _noteListState.value as? NoteListUiState.Loaded ?: return
        val notes = uiModel.notes
        viewModelScope.launch {
            val sortedNotes = moveNotesUseCase.updatePositions(notes, fromPos, toPos)
            _noteListState.update { uiModel.copy(notes = sortedNotes) }
            moveNotesUseCase.updateRepo(sortedNotes)
        }
    }

    private fun enableNoteUpdates() {
        isListUpdateEnabled = true
    }
}
