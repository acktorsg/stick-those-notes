/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.ui

import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.common.models.ContentType

sealed class NoteListItemUiModel {
    abstract val id: Long
    abstract val title: String
    abstract val colorPalette: ColorPalette
    abstract val isPinned: Boolean
    abstract val contentType: ContentType

    data class Text(
        override val id: Long,
        override val title: String,
        override val colorPalette: ColorPalette,
        override val isPinned: Boolean,
        val content: String,
    ) : NoteListItemUiModel() {
        override val contentType get() = ContentType.TEXT
    }

    data class Checklist(
        override val id: Long,
        override val title: String,
        override val colorPalette: ColorPalette,
        override val isPinned: Boolean,
        val content: List<ChecklistItem>,
    ) : NoteListItemUiModel() {
        override val contentType get() = ContentType.CHECKLIST
    }

    data class ChecklistItem(val isChecked: Boolean, val content: String)
}
