/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.CheckBox
import androidx.compose.material.icons.rounded.CheckBoxOutlineBlank
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import sg.acktor.acktorsdk.ui.compose.Dimensions
import sg.acktor.android.stickthosenotes.common.LocalNoteThemes
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.notelist.api.R

private const val MAX_CHECKLIST_ITEMS = 8

@Composable
fun NoteListItem(
    modifier: Modifier = Modifier,
    noteUiModel: NoteListItemUiModel,
    openNoteDetailsScreen: () -> Unit
) {
    Box(
        modifier = modifier,
    ) {
        val theme = LocalNoteThemes.current[noteUiModel.colorPalette]

        ElevatedCard(
            modifier = Modifier
                .fillMaxWidth()
                .clickable(onClick = openNoteDetailsScreen),
            colors = CardDefaults.elevatedCardColors(
                containerColor = theme.darkBackground,
                contentColor = theme.onDarkBackground,
            )
        ) {
            Spacer(Modifier.height(Dimensions.spacingSmall))
            Text(
                noteUiModel.title,
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(horizontal = Dimensions.spacingDefault),
                style = MaterialTheme.typography.bodyLarge,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
            )
            Spacer(Modifier.height(Dimensions.spacingSmall))
            Card(
                modifier = Modifier.fillMaxWidth(),
                colors = CardDefaults.elevatedCardColors(
                    containerColor = theme.lightBackground,
                    contentColor = theme.onLightBackground,
                )
            ) {
                when (noteUiModel) {
                    is NoteListItemUiModel.Text -> {
                        Box(Modifier.padding(Dimensions.spacingDefault)) {
                            Text(
                                noteUiModel.content,
                                style = MaterialTheme.typography.bodyMedium,
                                maxLines = 10,
                                overflow = TextOverflow.Ellipsis,
                            )
                        }
                    }

                    is NoteListItemUiModel.Checklist -> {
                        Box(
                            Modifier.padding(
                                top = Dimensions.spacingDefault,
                                bottom = Dimensions.spacingSmall,
                                start = Dimensions.spacingDefault,
                                end = Dimensions.spacingDefault,
                            )
                        ) {
                            Column {
                                noteUiModel.content.take(MAX_CHECKLIST_ITEMS).map {
                                    ChecklistItem(it)
                                }

                                if (noteUiModel.content.size > MAX_CHECKLIST_ITEMS) {
                                    Text(
                                        stringResource(R.string.indicator_moreItems),
                                        modifier = Modifier
                                            .padding(bottom = Dimensions.spacingSmall)
                                            .align(Alignment.CenterHorizontally),
                                        style = MaterialTheme.typography.bodyLarge,
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }

        if (noteUiModel.isPinned) {
            Icon(
                painterResource(R.drawable.ic_pin_static),
                contentDescription = "Note is pinned",
                modifier = Modifier
                    .align(Alignment.TopEnd)
                    .rotate(45f),
                tint = Color.Unspecified,
            )
        }
    }
}

@Composable
private fun ChecklistItem(checklistItem: NoteListItemUiModel.ChecklistItem) {
    Row(
        Modifier.padding(bottom = Dimensions.spacingSmall),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            if (checklistItem.isChecked) {
                Icons.Rounded.CheckBox
            } else {
                Icons.Rounded.CheckBoxOutlineBlank
            },
            contentDescription = "Is item checked? ${checklistItem.isChecked}"
        )
        Spacer(Modifier.width(Dimensions.spacingSmall))
        Text(
            checklistItem.content,
            style = MaterialTheme.typography.bodyMedium,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
        )
    }
}

@Preview
@Composable
private fun TextNoteListItemPreview() {
    val note = NoteListItemUiModel.Text(
        id = 0,
        title = "An incredibly long title for client meeting notes that shouldn't span more " +
                "than 2 lines because we cannot keep it too long",
        colorPalette = ColorPalette.GREEN,
        isPinned = false,
        content = "We need to rethink how we handle user experience & security issues. " +
                "Geralt is going to circle back with Yennifer & chat about the latest " +
                "engineering estimates. Jaskier is working on a one-pager that will help us " +
                "understand what to do in the event of another lockdown.\n\nHere are more " +
                "lines to pad out the content so that it gets cut off after a certain " +
                "threshold. Had to cheat a little by adding some break lines."
    )

    StickThoseNotesTheme {
        NoteListItem(
            noteUiModel = note,
            openNoteDetailsScreen = {},
        )
    }
}

@Preview
@Composable
private fun ChecklistNoteListItemPreview() {
    val note = NoteListItemUiModel.Checklist(
        id = 0,
        title = "Shopping list",
        colorPalette = ColorPalette.YELLOW,
        isPinned = true,
        content = listOf(
            NoteListItemUiModel.ChecklistItem(false, "Apple"),
            NoteListItemUiModel.ChecklistItem(true, "Paper towels"),
            NoteListItemUiModel.ChecklistItem(true, "Skim milk"),
            NoteListItemUiModel.ChecklistItem(false, "Eggs"),
            NoteListItemUiModel.ChecklistItem(true, "French bread"),
            NoteListItemUiModel.ChecklistItem(false, "Banana"),
            NoteListItemUiModel.ChecklistItem(
                false,
                "This is a really long checklist item that should get cut off after " +
                        "the second line because we cannot make each item too long"
            ),
            NoteListItemUiModel.ChecklistItem(false, "Get budget approval"),
            NoteListItemUiModel.ChecklistItem(false, "Review market research"),
        )
    )

    StickThoseNotesTheme {
        NoteListItem(
            noteUiModel = note,
            openNoteDetailsScreen = {},
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun ChecklistItemPreview() {
    val checklistItem = NoteListItemUiModel.ChecklistItem(
        true,
        "Buy modmat @ store.gamersnexus.net"
    )
    StickThoseNotesTheme {
        ChecklistItem(checklistItem)
    }
}
