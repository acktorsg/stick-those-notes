/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.ui

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import dagger.hilt.android.components.ActivityComponent
import it.czerwinski.android.hilt.annotations.Bound
import sg.acktor.android.stickthosenotes.notelist.api.ui.NoteListUiProvider
import javax.inject.Inject

@Bound(ActivityComponent::class)
class NoteListUiProviderImpl @Inject constructor() : NoteListUiProvider {
    @Composable
    override fun NoteList(modifier: Modifier, showSnackbarMessage: (String) -> Unit) {
        NoteListScreen(modifier = modifier, showMessage = showSnackbarMessage)
    }
}
