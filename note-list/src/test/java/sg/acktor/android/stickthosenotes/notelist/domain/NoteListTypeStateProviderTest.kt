package sg.acktor.android.stickthosenotes.notelist.domain

import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.notelist.api.domain.NoteListTypeStateProvider
import sg.acktor.android.stickthosenotes.notelist.api.models.NoteListType
import kotlin.test.Test
import kotlin.test.assertEquals

class NoteListTypeStateProviderTest {
    private val sut: NoteListTypeStateProvider = NoteListTypeStateProviderImpl()

    @Test
    fun `verify state is updated`() = runTest {
        val expectedInitial = NoteListType.Home
        val resultInitial = sut.state.first()
        assertEquals(expectedInitial, resultInitial)

        val expectedUpdate = NoteListType.RecycleBin
        sut.setState(expectedUpdate)
        val resultUpdate = sut.state.first()
        assertEquals(expectedUpdate, resultUpdate)
    }
}
