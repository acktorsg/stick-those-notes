/*
 * Copyright (c) 2023. Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.data

import io.mockk.every
import io.mockk.mockk
import io.mockk.verifySequence
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItemRelation
import sg.acktor.android.stickthosenotes.notelist.data.models.NoteListItemApiModel
import sg.acktor.android.stickthosenotes.notelist.data.models.ChecklistNoteListRoom
import sg.acktor.android.stickthosenotes.notelist.data.room.NoteListDAO
import sg.acktor.android.stickthosenotes.notelist.data.models.TextNoteListRoom
import sg.acktor.android.stickthosenotes.notelist.mappers.NoteListItemApiModelMapper
import kotlin.test.Test
import kotlin.test.assertEquals

class NoteListRepositorySearchTest {
    private val checklistNotes = arrayOf(
        ChecklistNoteListRoom(
            id = 0,
            title = "Shopping list",
            colorPalette = ColorPalette.YELLOW,
            isPinned = true,
            position = 1,
            items = listOf(
                ChecklistItemRelation(
                    content = "Apple",
                    isChecked = false
                ),
                ChecklistItemRelation(
                    content = "Paper towels",
                    isChecked = true
                ),
            )
        ),
        ChecklistNoteListRoom(
            id = 2,
            title = "To-do",
            colorPalette = ColorPalette.GREEN,
            isPinned = false,
            position = 3,
            items = listOf(
                ChecklistItemRelation(
                    content = "Buy modmat @ store.gamersnexus.net",
                    isChecked = true
                ),
                ChecklistItemRelation(
                    content = "Call the vet",
                    isChecked = false
                ),
            )
        ),
    )

    private val dao = mockk<NoteListDAO> {
        every { searchTextNotes(any()) } returns flowOf(emptyArray())
        every { searchChecklistNotes(any()) } returns flowOf(checklistNotes)
    }

    private val sut = NoteListRepository(dao, NoteListItemApiModelMapper())

    @Test
    fun `query returns text and checklist notes`() {
        every { dao.searchTextNotes(any()) } returns flowOf(
            arrayOf(
                TextNoteListRoom(
                    id = 1,
                    title = "E-commerce store listing template",
                    colorPalette = ColorPalette.RED,
                    isPinned = false,
                    position = 2,
                    content = "[NOTE: Price is NOT negotiable]"
                ),
                TextNoteListRoom(
                    id = 3,
                    title = "Notes from client meeting",
                    colorPalette = ColorPalette.WHITE,
                    isPinned = true,
                    position = 4,
                    content = "We need to rethink how we handle user experience & security issues."
                )
            )
        )
        every { dao.searchChecklistItems(any()) } returns flowOf(emptyArray())
        every { dao.getChecklistNotes(emptyArray()) } returns flowOf(emptyArray())

        val expected = listOf(
            NoteListItemApiModel.Text(
                id = 1,
                title = "E-commerce store listing template",
                colorPalette = ColorPalette.RED,
                isPinned = false,
                position = 2,
                content = "[NOTE: Price is NOT negotiable]"
            ),
            NoteListItemApiModel.Text(
                id = 3,
                title = "Notes from client meeting",
                colorPalette = ColorPalette.WHITE,
                isPinned = true,
                position = 4,
                content = "We need to rethink how we handle user experience & security issues."
            ),
            NoteListItemApiModel.Checklist(
                id = 0,
                title = "Shopping list",
                colorPalette = ColorPalette.YELLOW,
                isPinned = true,
                position = 1,
                content = listOf(
                    NoteListItemApiModel.ChecklistItem(
                        content = "Apple",
                        isChecked = false
                    ),
                    NoteListItemApiModel.ChecklistItem(
                        content = "Paper towels",
                        isChecked = true
                    ),
                )
            ),
            NoteListItemApiModel.Checklist(
                id = 2,
                title = "To-do",
                colorPalette = ColorPalette.GREEN,
                isPinned = false,
                position = 3,
                content = listOf(
                    NoteListItemApiModel.ChecklistItem(
                        content = "Buy modmat @ store.gamersnexus.net",
                        isChecked = true
                    ),
                    NoteListItemApiModel.ChecklistItem(
                        content = "Call the vet",
                        isChecked = false
                    ),
                )
            ),
        )

        runTest {
            val result = sut.searchNotes("arc").first()
            assertEquals(expected, result)
        }

        verifySequence {
            dao.searchTextNotes(any())
            dao.searchChecklistNotes(any())
            dao.searchChecklistItems(any())
            dao.getChecklistNotes(emptyArray())
        }
    }

    @Test
    fun `query checklist notes and items with no duplicates`() {
        val noteIds = arrayOf(4L)

        every { dao.searchChecklistItems(any()) } returns flowOf(noteIds)
        every { dao.getChecklistNotes(noteIds) } returns flowOf(
            arrayOf(
                ChecklistNoteListRoom(
                    id = 4,
                    title = "Store opening plan",
                    colorPalette = ColorPalette.BLUE,
                    isPinned = false,
                    position = 5,
                    items = listOf(
                        ChecklistItemRelation(
                            content = "Get budget approval",
                            isChecked = false
                        ),
                        ChecklistItemRelation(
                            content = "Review market research",
                            isChecked = false
                        ),
                    )
                ),
            )
        )

        val expected = listOf(
            NoteListItemApiModel.Checklist(
                id = 0,
                title = "Shopping list",
                colorPalette = ColorPalette.YELLOW,
                isPinned = true,
                position = 1,
                content = listOf(
                    NoteListItemApiModel.ChecklistItem(
                        content = "Apple",
                        isChecked = false
                    ),
                    NoteListItemApiModel.ChecklistItem(
                        content = "Paper towels",
                        isChecked = true
                    ),
                )
            ),
            NoteListItemApiModel.Checklist(
                id = 2,
                title = "To-do",
                colorPalette = ColorPalette.GREEN,
                isPinned = false,
                position = 3,
                content = listOf(
                    NoteListItemApiModel.ChecklistItem(
                        content = "Buy modmat @ store.gamersnexus.net",
                        isChecked = true
                    ),
                    NoteListItemApiModel.ChecklistItem(
                        content = "Call the vet",
                        isChecked = false
                    ),
                )
            ),
            NoteListItemApiModel.Checklist(
                id = 4,
                title = "Store opening plan",
                colorPalette = ColorPalette.BLUE,
                isPinned = false,
                position = 5,
                content = listOf(
                    NoteListItemApiModel.ChecklistItem(
                        content = "Get budget approval",
                        isChecked = false
                    ),
                    NoteListItemApiModel.ChecklistItem(
                        content = "Review market research",
                        isChecked = false
                    ),
                )
            ),
        )

        runTest {
            val result = sut.searchNotes("arc").first()
            assertEquals(expected, result)
        }

        verifySequence {
            dao.searchTextNotes(any())
            dao.searchChecklistNotes(any())
            dao.searchChecklistItems(any())
            dao.getChecklistNotes(noteIds)
        }
    }

    @Test
    fun `query checklist notes and items with duplicates`() {
        val noteIds = arrayOf(0L)

        every { dao.searchChecklistItems(any()) } returns flowOf(noteIds)
        every { dao.getChecklistNotes(noteIds) } returns flowOf(
            arrayOf(
                ChecklistNoteListRoom(
                    id = 0,
                    title = "Shopping list",
                    colorPalette = ColorPalette.YELLOW,
                    isPinned = true,
                    position = 1,
                    items = listOf(
                        ChecklistItemRelation(
                            content = "Apple",
                            isChecked = false
                        ),
                        ChecklistItemRelation(
                            content = "Paper towels",
                            isChecked = true
                        ),
                    )
                ),
            )
        )

        val expected = listOf(
            NoteListItemApiModel.Checklist(
                id = 0,
                title = "Shopping list",
                colorPalette = ColorPalette.YELLOW,
                isPinned = true,
                position = 1,
                content = listOf(
                    NoteListItemApiModel.ChecklistItem(
                        content = "Apple",
                        isChecked = false
                    ),
                    NoteListItemApiModel.ChecklistItem(
                        content = "Paper towels",
                        isChecked = true
                    ),
                )
            ),
            NoteListItemApiModel.Checklist(
                id = 2,
                title = "To-do",
                colorPalette = ColorPalette.GREEN,
                isPinned = false,
                position = 3,
                content = listOf(
                    NoteListItemApiModel.ChecklistItem(
                        content = "Buy modmat @ store.gamersnexus.net",
                        isChecked = true
                    ),
                    NoteListItemApiModel.ChecklistItem(
                        content = "Call the vet",
                        isChecked = false
                    ),
                )
            ),
        )

        runTest {
            val result = sut.searchNotes("arc").first()
            assertEquals(expected, result)
        }

        verifySequence {
            dao.searchTextNotes(any())
            dao.searchChecklistNotes(any())
            dao.searchChecklistItems(any())
            dao.getChecklistNotes(noteIds)
        }
    }
}
