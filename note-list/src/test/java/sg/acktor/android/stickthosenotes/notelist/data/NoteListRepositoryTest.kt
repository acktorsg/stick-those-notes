/*
 * Copyright (c) 2023. Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.data

import io.mockk.Ordering
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.database.api.room.entities.ChecklistItemRelation
import sg.acktor.android.stickthosenotes.database.api.room.entities.NotePosition
import sg.acktor.android.stickthosenotes.notelist.data.models.ChecklistNoteListRoom
import sg.acktor.android.stickthosenotes.notelist.data.models.NoteListItemApiModel
import sg.acktor.android.stickthosenotes.notelist.data.models.NotePositionApiModel
import sg.acktor.android.stickthosenotes.notelist.data.models.TextNoteListRoom
import sg.acktor.android.stickthosenotes.notelist.data.room.NoteListDAO
import sg.acktor.android.stickthosenotes.notelist.mappers.NoteListItemApiModelMapper
import kotlin.test.Test
import kotlin.test.assertEquals

class NoteListRepositoryTest {
    private val dao = mockk<NoteListDAO>(relaxUnitFun = true)
    private val noteItemMapper = NoteListItemApiModelMapper()

    private val sut = NoteListRepository(dao, noteItemMapper)

    @Test
    fun `verify list is ordered by position`() {
        val textNotes = arrayOf(
            TextNoteListRoom(
                id = 1,
                title = "E-commerce store listing template",
                colorPalette = ColorPalette.RED,
                isPinned = false,
                position = 2,
                content = "[NOTE: Price is NOT negotiable]"
            ),
            TextNoteListRoom(
                id = 3,
                title = "Notes from client meeting",
                colorPalette = ColorPalette.WHITE,
                isPinned = true,
                position = 4,
                content = "We need to rethink how we handle user experience & security issues."
            )
        )

        val checklistNotes = arrayOf(
            ChecklistNoteListRoom(
                id = 0,
                title = "Shopping list",
                colorPalette = ColorPalette.YELLOW,
                isPinned = true,
                position = 1,
                items = listOf(
                    ChecklistItemRelation(
                        content = "Apple",
                        isChecked = false
                    ),
                    ChecklistItemRelation(
                        content = "Paper towels",
                        isChecked = true
                    ),
                )
            ),
            ChecklistNoteListRoom(
                id = 2,
                title = "To-do",
                colorPalette = ColorPalette.BLUE,
                isPinned = false,
                position = 3,
                items = listOf(
                    ChecklistItemRelation(
                        content = "Buy modmat @ store.gamersnexus.net",
                        isChecked = true
                    ),
                    ChecklistItemRelation(
                        content = "Call the vet",
                        isChecked = false
                    ),
                )
            ),
        )

        val expected = buildList {
            addAll(noteItemMapper.mapTextNotes(textNotes))
            addAll(noteItemMapper.mapChecklistNotes(checklistNotes))
            sortBy(NoteListItemApiModel::position)
        }

        every { dao.getAllTextNotes(false) } returns flowOf(textNotes)
        every { dao.getAllChecklistNotes(false) } returns flowOf(checklistNotes)

        runTest {
            val result = sut.getAllNotes(false).first()
            assertEquals(expected, result)
        }
    }

    @Test
    fun `verify note position update`() = runTest {
        val apiModels = listOf(NotePositionApiModel(2L, 3))
        val daoPositions = apiModels.map { NotePosition(it.id, it.position) }

        sut.updateNotePositions(apiModels)

        coVerify(Ordering.SEQUENCE) {
            dao.updateNotePositions(daoPositions)
        }
    }
}
