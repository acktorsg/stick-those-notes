package sg.acktor.android.stickthosenotes.notelist

import android.content.Context
import androidx.annotation.StringRes
import io.mockk.CapturingSlot
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.notelist.api.domain.NoteListTypeStateProvider
import sg.acktor.android.stickthosenotes.notelist.api.models.NoteListType
import sg.acktor.android.stickthosenotes.notelist.ui.NoteListItemUiModel
import sg.acktor.android.stickthosenotes.notelist.ui.NoteListUiAction
import sg.acktor.android.stickthosenotes.notelist.ui.NoteListUiEvent
import sg.acktor.android.stickthosenotes.notelist.ui.NoteListUiState
import sg.acktor.android.stickthosenotes.notelist.usecases.GetNoteListUseCase
import sg.acktor.android.stickthosenotes.notelist.usecases.MoveNotesUseCase
import sg.acktor.android.stickthosenotes.notelist.utils.mockChecklistNote
import sg.acktor.android.stickthosenotes.notelist.utils.mockTextNote
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs

private const val MOCK_STRING = "string resource"

@OptIn(ExperimentalCoroutinesApi::class)
class NoteListViewModelTest {
    companion object {
        @BeforeAll
        @JvmStatic
        fun setUpAll() {
            Dispatchers.setMain(StandardTestDispatcher())
        }

        @AfterAll
        @JvmStatic
        fun tearDownAll() {
            Dispatchers.resetMain()
        }
    }

    private val mockListFlow = MutableStateFlow(emptyList<NoteListItemUiModel>())
    private val mockTypeState = MutableStateFlow<NoteListType>(NoteListType.Home)

    private val context = mockk<Context> {
        every { getString(any()) } returns MOCK_STRING
    }
    private val getNoteListUseCase = mockk<GetNoteListUseCase> {
        every { run(any<NoteListType>()) } returns mockListFlow
    }
    private val moveNotesUseCase = mockk<MoveNotesUseCase>(relaxUnitFun = true)
    private val typeSlot = CapturingSlot<NoteListType>()
    private val stateProvider = mockk<NoteListTypeStateProvider> {
        every { state } returns mockTypeState
        every { setState(capture(typeSlot)) } answers {
            mockTypeState.update { typeSlot.captured }
        }
    }
    private val sut = NoteListViewModel(
        context, getNoteListUseCase, moveNotesUseCase, stateProvider,
    )

    @AfterEach
    fun tearDown() {
        mockTypeState.update { NoteListType.Home }
        mockListFlow.update { emptyList() }
    }

    @Test
    fun `verify create note dialog shown`() = runTest {
        val expected = NoteListUiEvent.OpenCreateNoteDialog
        sut.processUiAction(NoteListUiAction.CreateNoteSelected)
        val result = sut.uiEventChannel.first()
        assertEquals(expected, result)
    }

    @Test
    fun `verify note opened`() = runTest {
        val noteId = 4L
        val contentType = ContentType.TEXT
        val expected = NoteListUiEvent.OpenNote(noteId, contentType)

        sut.processUiAction(NoteListUiAction.NoteSelected(noteId, contentType))
        val result = sut.uiEventChannel.first()
        assertEquals(expected, result)
    }

    @Test
    fun `verify list updates disabled when moving notes`() = runTest {
        mockListFlow.update { listOf(mockTextNote()) }

        val initialUiState = sut.noteListState.first { it is NoteListUiState.Loaded }

        // Start moving notes
        sut.processUiAction(NoteListUiAction.StartNoteMove)
        advanceUntilIdle()
        // Simulate note list updating
        mockListFlow.update { listOf(mockTextNote()) }
        advanceUntilIdle()
        // Assert that UI state is not updated
        val updatedUiState = sut.noteListState.value
        assertEquals(initialUiState, updatedUiState)

        val noteList = listOf(mockChecklistNote())
        val expected = NoteListUiState.Loaded(
            notes = noteList,
            isDraggingEnabled = true,
            isAddButtonVisible = true,
        )

        // Stop moving notes
        sut.processUiAction(NoteListUiAction.EndNoteMove)
        advanceUntilIdle()
        // Simulate note list updating
        mockListFlow.update { noteList }
        advanceUntilIdle()
        // Assert that UI state is updated
        val finalUiState = sut.noteListState.first()
        assertEquals(expected, finalUiState)
    }

    @Test
    fun `verify list type change`() = runTest {
        val expected = listOf(mockTextNote())

        mockListFlow.update { expected }
        sut.processUiAction(NoteListUiAction.ChangeListType(NoteListType.RecycleBin))
        advanceUntilIdle()

        val uiState = sut.noteListState.first()
        assertIs<NoteListUiState.Loaded>(uiState)
        assertEquals(expected, uiState.notes)
    }

    @Test
    fun `verify moving notes`() = runTest {
        val note1 = mockTextNote()
        val note2 = mockChecklistNote()
        val notes = listOf(note1, note2)
        val expected = listOf(note2, note1)
        every { moveNotesUseCase.updatePositions(notes, 0, 1) } returns expected
        mockListFlow.update { listOf(note1, note2) }
        advanceUntilIdle()

        sut.processUiAction(NoteListUiAction.MoveNote(0, 1))
        advanceUntilIdle()

        val result = sut.noteListState.first()
        assertIs<NoteListUiState.Loaded>(result)
        assertEquals(expected, result.notes)
    }

    @Test
    fun `verify empty list & snackbar shown when note list load fails`() = runTest {
        val emptyMessage = "empty list"
        val snackbarMessage = "snackbar message"

        val expectedState = NoteListUiState.Empty(
            emptyMessage = emptyMessage,
            isAddButtonVisible = false,
        )

        val expectedEvent = NoteListUiEvent.ShowSnackbarMessage(message = snackbarMessage)

        every { context.getString(R.string.message_empty_notes) } returns emptyMessage
        every { context.getString(R.string.message_error_loadNotes) } returns snackbarMessage
        every { getNoteListUseCase.run(NoteListType.RecycleBin) } returns flow {
            throw Exception("rando exception")
        }

        sut.processUiAction(NoteListUiAction.ChangeListType(NoteListType.RecycleBin))
        advanceUntilIdle()

        val resultState = sut.noteListState.first()
        assertEquals(expectedState, resultState)

        val resultEvent = sut.uiEventChannel.first()
        assertEquals(expectedEvent, resultEvent)
    }

    @Test
    fun `verify moving notes stops if checks fail`() = runTest {
        sut.processUiAction(NoteListUiAction.MoveNote(0, 1))
        advanceUntilIdle()

        verify(exactly = 0) {
            moveNotesUseCase.updatePositions(any(), any(), any())
        }

        val note1 = mockTextNote()
        val note2 = mockChecklistNote()
        mockListFlow.update { listOf(note1, note2) }
        advanceUntilIdle()

        sut.processUiAction(NoteListUiAction.MoveNote(0, 0))
        advanceUntilIdle()

        verify(exactly = 0) {
            moveNotesUseCase.updatePositions(any(), any(), any())
        }
    }

    @Test
    fun `verify different message shown for different list types`() = runTest {
        verifyEmptyMessage(
            expectedMessage = "No notes in Recycle Bin",
            stringRes = R.string.message_empty_deletedNotes,
            listType = NoteListType.RecycleBin,
        )

        verifyEmptyMessage(
            expectedMessage = " ",
            stringRes = R.string.message_empty_search,
            listType = NoteListType.Search("some query"),
        )

        verifyEmptyMessage(
            expectedMessage = "No notes added",
            stringRes = R.string.message_empty_notes,
            listType = NoteListType.Home,
        )
    }

    private suspend fun TestScope.verifyEmptyMessage(
        expectedMessage: String,
        @StringRes stringRes: Int,
        listType: NoteListType,
    ) {
        every { context.getString(stringRes) } returns expectedMessage
        val expected = NoteListUiState.Empty(
            emptyMessage = expectedMessage,
            isAddButtonVisible = listType == NoteListType.Home,
        )

        sut.processUiAction(NoteListUiAction.ChangeListType(listType))
        advanceUntilIdle()
        val result = sut.noteListState.first()
        assertEquals(expected, result)
    }
}
