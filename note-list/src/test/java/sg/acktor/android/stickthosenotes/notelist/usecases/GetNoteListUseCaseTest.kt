/*
 * Copyright (c) 2023. Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.usecases

import io.mockk.Called
import io.mockk.Ordering
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.notelist.api.models.NoteListType
import sg.acktor.android.stickthosenotes.notelist.data.NoteListRepository
import sg.acktor.android.stickthosenotes.notelist.data.models.NoteListItemApiModel
import sg.acktor.android.stickthosenotes.notelist.mappers.NoteListItemUiModelMapper
import sg.acktor.android.stickthosenotes.notelist.ui.NoteListItemUiModel
import kotlin.test.Test
import kotlin.test.assertEquals

class GetNoteListUseCaseTest {
    private val repo = mockk<NoteListRepository> {
        every { searchNotes(any()) } returns flowOf(
            listOf(
                NoteListItemApiModel.Text(
                    id = 1,
                    title = "E-commerce store listing template",
                    colorPalette = ColorPalette.RED,
                    isPinned = false,
                    position = 2,
                    content = "[NOTE: Price is NOT negotiable]"
                ),
                NoteListItemApiModel.Checklist(
                    id = 0,
                    title = "Shopping list",
                    colorPalette = ColorPalette.YELLOW,
                    isPinned = true,
                    position = 1,
                    content = listOf(
                        NoteListItemApiModel.ChecklistItem(
                            content = "Apple",
                            isChecked = false
                        ),
                        NoteListItemApiModel.ChecklistItem(
                            content = "Paper towels",
                            isChecked = true
                        ),
                    )
                ),
            )
        )
    }

    private val sut = GetNoteListUseCaseImpl(repo, NoteListItemUiModelMapper())

    @Test
    fun `empty query returns blank without querying dao`() {
        runTest {
            val result = sut.run(NoteListType.Search("")).first()
            assertEquals(0, result.size)
        }

        verify {
            repo wasNot Called
        }
    }

    @Test
    fun `retrieve query results from repo and map to ui model`() {
        val expected = listOf(
            NoteListItemUiModel.Text(
                id = 1,
                title = "E-commerce store listing template",
                colorPalette = ColorPalette.RED,
                isPinned = false,
                content = "[NOTE: Price is NOT negotiable]"
            ),
            NoteListItemUiModel.Checklist(
                id = 0,
                title = "Shopping list",
                colorPalette = ColorPalette.YELLOW,
                isPinned = true,
                content = listOf(
                    NoteListItemUiModel.ChecklistItem(
                        content = "Apple",
                        isChecked = false
                    ),
                    NoteListItemUiModel.ChecklistItem(
                        content = "Paper towels",
                        isChecked = true
                    ),
                )
            ),
        )

        runTest {
            val result = sut.run(NoteListType.Search("arc")).first()
            assertEquals(expected, result)
        }
    }

    @Test
    fun `verify notes retrieved from repo`() = runTest {
        every { repo.getAllNotes(true) } returns flowOf()

        sut.run(NoteListType.RecycleBin)

        coVerify(ordering = Ordering.ORDERED) {
            repo.getAllNotes(true)
        }
    }
}
