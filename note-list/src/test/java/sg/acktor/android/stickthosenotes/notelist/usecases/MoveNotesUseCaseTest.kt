/*
 * Copyright (c) 2023. Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.notelist.usecases

import io.mockk.Ordering
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.common.models.ColorPalette
import sg.acktor.android.stickthosenotes.notelist.data.NoteListRepository
import sg.acktor.android.stickthosenotes.notelist.mappers.NoteListApiModelMapper
import sg.acktor.android.stickthosenotes.notelist.ui.NoteListItemUiModel
import kotlin.test.Test
import kotlin.test.assertEquals

class MoveNotesUseCaseTest {
    private val note1 = NoteListItemUiModel.Checklist(
        id = 0,
        title = "Shopping list",
        colorPalette = ColorPalette.YELLOW,
        isPinned = true,
        content = listOf(
            NoteListItemUiModel.ChecklistItem(
                isChecked = false,
                content = "Apple",
            ),
            NoteListItemUiModel.ChecklistItem(
                isChecked = true,
                content = "Paper towels",
            ),
        ),
    )

    private val note2 = NoteListItemUiModel.Text(
        id = 1,
        title = "E-commerce store listing template",
        colorPalette = ColorPalette.RED,
        isPinned = false,
        content = "[NOTE: Price is NOT negotiable]"
    )

    private val note3 = NoteListItemUiModel.Checklist(
        id = 2,
        title = "To-do",
        colorPalette = ColorPalette.BLUE,
        isPinned = false,
        content = listOf(
            NoteListItemUiModel.ChecklistItem(
                isChecked = true,
                content = "Buy modmat @ store.gamersnexus.net"
            ),
            NoteListItemUiModel.ChecklistItem(
                isChecked = false,
                content = "Call the vet"
            ),
        ),
    )

    private val notes = listOf(note1, note2, note3)

    private val repo = mockk<NoteListRepository>(relaxUnitFun = true)
    private val apiModelMapper = NoteListApiModelMapper()
    private val useCase = MoveNotesUseCaseImpl(repo, apiModelMapper)

    @Test
    fun `move from position 1 to position 3`() {
        performTest(
            fromPos = 0, toPos = 2,
            expected = listOf(note2, note3, note1)
        )
    }

    @Test
    fun `move from position 3 to position 1`() {
        performTest(
            fromPos = 2, toPos = 0,
            expected = listOf(note3, note1, note2)
        )
    }

    @Test
    fun `move from position 1 to position 2`() {
        performTest(
            fromPos = 0, toPos = 1,
            expected = listOf(note2, note1, note3)
        )
    }

    @Test
    fun `move from position 3 to position 2`() {
        performTest(
            fromPos = 2, toPos = 1,
            expected = listOf(note1, note3, note2)
        )
    }

    @Test
    fun `verify repo is updated`() = runTest {
        useCase.updateRepo(notes)

        coVerify(ordering = Ordering.ORDERED) {
            repo.updateNotePositions(apiModelMapper.mapToPosition(notes))
        }
    }

    private fun performTest(
        fromPos: Int, toPos: Int,
        expected: List<NoteListItemUiModel>,
    ) {
        val actual = useCase.updatePositions(notes, fromPos, toPos)
        assertEquals(expected, actual)
    }
}
