/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.home.ui

import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Menu
import androidx.compose.material.icons.rounded.MoreVert
import androidx.compose.material.icons.rounded.Search
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SnackbarResult
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import kotlinx.coroutines.launch
import sg.acktor.android.stickthosenotes.home.R
import sg.acktor.android.stickthosenotes.notelist.api.ui.NoteListUiProvider

@Composable
internal fun HomeContent(
    uiState: HomeUiModel,
    isDualPane: Boolean,
    noteListUiProvider: NoteListUiProvider,
    openDrawer: () -> Unit,
    sendUiAction: (HomeUiAction) -> Unit,
) {
    val scope = rememberCoroutineScope()

    val snackbarState = remember { SnackbarHostState() }

    LaunchedEffect(uiState.homeMessage) {
        val homeMessage = uiState.homeMessage
        if (homeMessage != null) {
            val result = snackbarState.showSnackbar(
                homeMessage.message,
                homeMessage.actionLabel,
                duration = SnackbarDuration.Short,
            )

            if (result == SnackbarResult.ActionPerformed) {
                homeMessage.action?.let {
                    sendUiAction(HomeUiAction.PerformHomeMessageAction(it))
                }
            }

            sendUiAction(HomeUiAction.SnackbarDismiss)
        }
    }

    Scaffold(
        topBar = {
            HomeTopBar(
                uiState = uiState.headerUiModel,
                showTitle = !isDualPane,
                showDrawerIcon = !isDualPane,
                openDrawer = openDrawer,
                sendUiAction = sendUiAction,
            )
        },
        snackbarHost = { SnackbarHost(snackbarState) }
    ) {
        noteListUiProvider.NoteList(
            modifier = Modifier.padding(top = it.calculateTopPadding()),
            showSnackbarMessage = { message ->
                scope.launch { snackbarState.showSnackbar(message) }
            },
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun HomeTopBar(
    uiState: HeaderUiModel,
    showTitle: Boolean,
    showDrawerIcon: Boolean,
    sendUiAction: (HomeUiAction) -> Unit,
    openDrawer: () -> Unit,
) {
    CenterAlignedTopAppBar(
        title = {
            if (showTitle) {
                Text(uiState.title)
            }
        },
        navigationIcon = {
            if (showDrawerIcon) {
                IconButton(onClick = openDrawer) {
                    Icon(
                        Icons.Rounded.Menu,
                        contentDescription = "Open navigation drawer"
                    )
                }
            }
        },
        actions = {
            when (uiState) {
                is HeaderUiModel.Home -> {
                    IconButton(onClick = { sendUiAction(HomeUiAction.SearchClicked) }) {
                        Icon(Icons.Rounded.Search, contentDescription = "Search")
                    }
                }

                is HeaderUiModel.RecycleBin -> {
                    var showOptions by remember { mutableStateOf(false) }

                    IconButton(onClick = { showOptions = true }) {
                        Icon(
                            Icons.Rounded.MoreVert,
                            contentDescription = "More options"
                        )
                    }

                    DropdownMenu(
                        expanded = showOptions,
                        onDismissRequest = { showOptions = false }
                    ) {
                        DropdownMenuItem(
                            text = { Text(stringResource(R.string.title_menu_emptyBin)) },
                            onClick = {
                                sendUiAction(HomeUiAction.ClearBinClicked)
                                showOptions = false
                            },
                            enabled = uiState.isEmptyBinEnabled,
                        )
                    }

                    if (uiState.showClearBinDialog) {
                        EmptyBinDialog(
                            clearBin = {
                                sendUiAction(HomeUiAction.ClearBinConfirm)
                            },
                            dismissDialog = { sendUiAction(HomeUiAction.ClearBinDialogDismiss) }
                        )
                    }
                }
            }
        }
    )
}

@Composable
private fun EmptyBinDialog(
    clearBin: () -> Unit,
    dismissDialog: () -> Unit
) {
    AlertDialog(
        title = { Text(stringResource(R.string.title_dialog_emptyBin)) },
        text = { Text(stringResource(R.string.message_dialog_emptyBin)) },
        dismissButton = {
            TextButton(onClick = dismissDialog) {
                Text(
                    stringResource(
                        sg.acktor.android.stickthosenotes.common.R.string.title_button_cancel
                    )
                )
            }
        },
        confirmButton = {
            TextButton(onClick = clearBin) {
                Text(stringResource(R.string.title_button_emptyBin))
            }
        },
        onDismissRequest = dismissDialog,
    )
}
