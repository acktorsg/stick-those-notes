/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.home.ui

import sg.acktor.android.stickthosenotes.home.api.HomeMessage

data class HomeUiModel(
    val headerUiModel: HeaderUiModel,
    val navDrawerUiModel: NavDrawerUiModel,
    val homeMessage: HomeMessage?,
)

sealed interface HeaderUiModel {
    val title: String

    data class Home(override val title: String) : HeaderUiModel

    data class RecycleBin(
        override val title: String,
        val isEmptyBinEnabled: Boolean,
        val showClearBinDialog: Boolean,
    ) : HeaderUiModel
}

data class NavDrawerUiModel(
    val isHomeListSelected: Boolean,
    val isRecycleBinSelected: Boolean,
)
