/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.home

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import sg.acktor.android.stickthosenotes.home.domain.HomeMessageProvider
import sg.acktor.android.stickthosenotes.home.domain.RecycleBinProvider
import sg.acktor.android.stickthosenotes.home.ui.HeaderUiModel
import sg.acktor.android.stickthosenotes.home.ui.HomeUiAction
import sg.acktor.android.stickthosenotes.home.ui.HomeUiEvent
import sg.acktor.android.stickthosenotes.home.ui.HomeUiModel
import sg.acktor.android.stickthosenotes.home.ui.NavDrawerUiModel
import sg.acktor.android.stickthosenotes.notelist.api.domain.NoteListTypeStateProvider
import sg.acktor.android.stickthosenotes.notelist.api.models.NoteListType
import javax.inject.Inject
import sg.acktor.android.stickthosenotes.common.R as R_common

@HiltViewModel
class HomeViewModel @Inject constructor(
    @ApplicationContext
    private val context: Context,
    private val recycleBinProvider: RecycleBinProvider,
    private val noteListTypeStateProvider: NoteListTypeStateProvider,
    private val homeMessageProvider: HomeMessageProvider,
) : ViewModel() {
    private val _homeUiState = MutableStateFlow<HomeUiModel>(
        HomeUiModel(
            headerUiModel = HeaderUiModel.Home(
                title = context.getString(R_common.string.app_name),
            ),
            navDrawerUiModel = NavDrawerUiModel(
                isHomeListSelected = true,
                isRecycleBinSelected = false,
            ),
            homeMessage = null,
        )
    )
    val homeUiState = _homeUiState.asStateFlow()

    private val _uiEventChannel = Channel<HomeUiEvent>()
    val uiEventChannel = _uiEventChannel.receiveAsFlow()

    init {
        combine(
            recycleBinProvider.recycleBinVisibilityState,
            noteListTypeStateProvider.state,
            homeMessageProvider.state,
        ) { isEmptyBinEnabled, listType, homeMessage ->
            _homeUiState.update {
                it.copy(
                    headerUiModel = if (listType == NoteListType.RecycleBin) {
                        HeaderUiModel.RecycleBin(
                            title = context.getString(R.string.title_screen_recycleBin),
                            isEmptyBinEnabled = isEmptyBinEnabled,
                            showClearBinDialog = false,
                        )
                    } else {
                        HeaderUiModel.Home(
                            title = context.getString(R_common.string.app_name),
                        )
                    },
                    navDrawerUiModel = it.navDrawerUiModel.copy(
                        isHomeListSelected = listType == NoteListType.Home,
                        isRecycleBinSelected = listType == NoteListType.RecycleBin,
                    ),
                    homeMessage = homeMessage,
                )
            }
        }
            .launchIn(viewModelScope)
    }

    fun processUiAction(event: HomeUiAction) {
        when (event) {
            is HomeUiAction.ChangeListType -> changeListType(event.listType)
            HomeUiAction.ClearBinClicked -> showClearBinConfirmation()
            HomeUiAction.ClearBinConfirm -> clearAllDeletedNotes()
            HomeUiAction.ClearBinDialogDismiss -> dismissClearBinConfirmation()
            HomeUiAction.SearchClicked -> goToSearchScreen()
            HomeUiAction.SnackbarDismiss -> clearSnackbar()
            is HomeUiAction.PerformHomeMessageAction -> runHomeMessageAction(event)
        }
    }

    private fun changeListType(listType: NoteListType) {
        noteListTypeStateProvider.setState(listType)
    }

    private fun goToSearchScreen() {
        noteListTypeStateProvider.setState(NoteListType.Search(""))

        viewModelScope.launch {
            _uiEventChannel.send(HomeUiEvent.OpenSearchScreen)
        }
    }

    private fun showClearBinConfirmation() {
        _homeUiState.update {
            val headerUiModel = (it.headerUiModel as? HeaderUiModel.RecycleBin) ?: return@update it
            it.copy(headerUiModel = headerUiModel.copy(showClearBinDialog = true))
        }
    }

    private fun dismissClearBinConfirmation() {
        _homeUiState.update {
            val headerUiModel = (it.headerUiModel as? HeaderUiModel.RecycleBin) ?: return@update it
            it.copy(headerUiModel = headerUiModel.copy(showClearBinDialog = false))
        }
    }

    private fun clearAllDeletedNotes() {
        dismissClearBinConfirmation()

        viewModelScope.launch(Dispatchers.IO) {
            recycleBinProvider.emptyBin()
        }
    }

    private fun clearSnackbar() {
        homeMessageProvider.clearMessage()
    }

    private fun runHomeMessageAction(event: HomeUiAction.PerformHomeMessageAction) {
        viewModelScope.launch {
            homeMessageProvider.performAction(event.action)
        }
    }
}
