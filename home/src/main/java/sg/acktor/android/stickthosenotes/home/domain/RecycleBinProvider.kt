/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.home.domain

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.Bound
import kotlinx.coroutines.flow.Flow
import sg.acktor.android.stickthosenotes.home.data.RecycleBinRepository
import javax.inject.Inject
import javax.inject.Singleton

interface RecycleBinProvider {
    val recycleBinVisibilityState: Flow<Boolean>

    suspend fun emptyBin()
}

@Bound(SingletonComponent::class)
@Singleton
class RecycleBinProviderImpl @Inject constructor(
    private val repo: RecycleBinRepository
) : RecycleBinProvider {
    override val recycleBinVisibilityState: Flow<Boolean>
        get() = repo.getRecycleBinState()

    override suspend fun emptyBin() {
        repo.clearAllDeletedNotes()
    }
}
