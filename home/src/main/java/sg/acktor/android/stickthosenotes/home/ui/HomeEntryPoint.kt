/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.home.ui

import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import kotlinx.serialization.Serializable
import sg.acktor.android.stickthosenotes.common.ui.EntryPoint
import sg.acktor.android.stickthosenotes.notelist.api.ui.NoteListUiProvider
import javax.inject.Inject

@Serializable
object HomeDestination

internal class HomeEntryPoint @Inject constructor(
    private val noteListUiProvider: NoteListUiProvider,
) : EntryPoint {
    override fun createDestination(navGraphBuilder: NavGraphBuilder) {
        navGraphBuilder.composable<HomeDestination> {
            HomeScreen(
                noteListUiProvider = noteListUiProvider,
            )
        }
    }
}
