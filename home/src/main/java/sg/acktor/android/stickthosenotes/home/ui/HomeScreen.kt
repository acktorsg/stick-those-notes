/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.home.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.displayCutoutPadding
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Info
import androidx.compose.material.icons.rounded.Settings
import androidx.compose.material.icons.rounded.Shield
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.PermanentDrawerSheet
import androidx.compose.material3.PermanentNavigationDrawer
import androidx.compose.material3.Text
import androidx.compose.material3.adaptive.currentWindowAdaptiveInfo
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.window.core.layout.WindowWidthSizeClass
import kotlinx.coroutines.launch
import sg.acktor.acktorsdk.ui.compose.Dimensions
import sg.acktor.android.stickthosenotes.common.LocalRootNavigator
import sg.acktor.android.stickthosenotes.common.RootNavigator
import sg.acktor.android.stickthosenotes.common.models.ContentType
import sg.acktor.android.stickthosenotes.common.theme.StickThoseNotesTheme
import sg.acktor.android.stickthosenotes.home.HomeViewModel
import sg.acktor.android.stickthosenotes.home.R
import sg.acktor.android.stickthosenotes.notelist.api.models.NoteListType
import sg.acktor.android.stickthosenotes.notelist.api.ui.NoteListUiProvider

@Composable
fun HomeScreen(
    homeViewModel: HomeViewModel = hiltViewModel(),
    noteListUiProvider: NoteListUiProvider,
) {
    val scope = rememberCoroutineScope()
    val rootNavigator = LocalRootNavigator.current
    val drawerState = rememberDrawerState(DrawerValue.Closed)

    val uiState by homeViewModel.homeUiState.collectAsStateWithLifecycle()

    val windowWidthClass = currentWindowAdaptiveInfo().windowSizeClass.windowWidthSizeClass
    val isDualPane = windowWidthClass == WindowWidthSizeClass.EXPANDED

    LaunchedEffect(Unit) {
        homeViewModel.uiEventChannel.collect { event ->
            when (event) {
                HomeUiEvent.OpenSearchScreen -> rootNavigator.showSearchScreen()
            }
        }
    }

    val drawerContent = @Composable {
        HomeDrawerContent(
            uiModel = uiState.navDrawerUiModel,
            setListType = { homeViewModel.processUiAction(HomeUiAction.ChangeListType(it)) },
            closeDrawer = {
                scope.launch {
                    drawerState.close()
                }
            },
        )
    }

    val homeContent = @Composable {
        HomeContent(
            uiState = uiState,
            isDualPane = isDualPane,
            noteListUiProvider = noteListUiProvider,
            openDrawer = {
                scope.launch {
                    drawerState.open()
                }
            },
            sendUiAction = homeViewModel::processUiAction,
        )
    }

    if (isDualPane) {
        PermanentNavigationDrawer(
            modifier = Modifier.displayCutoutPadding(),
            drawerContent = {
                PermanentDrawerSheet {
                    drawerContent()
                }
            },
            content = homeContent
        )
    } else {
        ModalNavigationDrawer(
            drawerContent = {
                ModalDrawerSheet {
                    drawerContent()
                }
            },
            drawerState = drawerState,
            content = homeContent
        )
    }
}

@Composable
private fun HomeDrawerContent(
    uiModel: NavDrawerUiModel,
    setListType: (NoteListType) -> Unit,
    closeDrawer: () -> Unit,
) {
    val rootNavigator = LocalRootNavigator.current
    val context = LocalContext.current

    Column(
        Modifier
            .padding(Dimensions.spacingDefault)
            .verticalScroll(rememberScrollState())
    ) {
        Text(
            stringResource(sg.acktor.android.stickthosenotes.common.R.string.app_name),
            style = MaterialTheme.typography.titleLarge,
            color = MaterialTheme.colorScheme.primary
        )
        Text(
            stringResource(R.string.subtitle_navDrawer),
            style = MaterialTheme.typography.titleMedium
        )
        Spacer(Modifier.height(Dimensions.spacingDefault))
        NavigationDrawerItem(
            label = { Text(stringResource(R.string.title_screen_noteList)) },
            selected = uiModel.isHomeListSelected,
            onClick = {
                setListType(NoteListType.Home)
                closeDrawer()
            }
        )
        NavigationDrawerItem(
            label = { Text(stringResource(R.string.title_screen_recycleBin)) },
            selected = uiModel.isRecycleBinSelected,
            onClick = {
                setListType(NoteListType.RecycleBin)
                closeDrawer()
            }
        )
        Spacer(Modifier.height(Dimensions.spacingSmall))
        HorizontalDivider()
        Spacer(Modifier.height(Dimensions.spacingSmall))
        NavigationDrawerItem(
            icon = { Icon(Icons.Rounded.Shield, contentDescription = "Settings") },
            label = { Text(stringResource(R.string.title_screen_privacyPolicy)) },
            selected = false,
            onClick = { openPrivacyPolicy(context) }
        )
        NavigationDrawerItem(
            icon = { Icon(Icons.Rounded.Settings, contentDescription = "Settings") },
            label = { Text(stringResource(R.string.title_screen_settings)) },
            selected = false,
            onClick = rootNavigator::showSettingsScreen
        )
        NavigationDrawerItem(
            icon = { Icon(Icons.Rounded.Info, contentDescription = "About this app") },
            label = { Text(stringResource(R.string.title_screen_about)) },
            selected = false,
            onClick = rootNavigator::showAboutScreen
        )
    }
}

private fun openPrivacyPolicy(context: Context) {
    context.startActivity(
        Intent.createChooser(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://stickthosenotes.acktorsg.info/privacypolicy.html")
            ),
            "Open with"
        )
    )
}

@Preview(showBackground = true)
@Composable
private fun HomeDrawerPreview() {
    val uiModel = NavDrawerUiModel(
        isHomeListSelected = true,
        isRecycleBinSelected = false,
    )

    StickThoseNotesTheme {
        Column(Modifier.fillMaxHeight()) {
            CompositionLocalProvider(
                LocalRootNavigator provides object : RootNavigator {
                    override fun showNoteDetailsScreen(
                        noteId: Long,
                        contentType: ContentType,
                        shouldReturnToHome: Boolean
                    ) {
                    }

                    override fun showCreateNoteScreen() {}

                    override fun showSearchScreen() {}

                    override fun showSettingsScreen() {}

                    override fun showAboutScreen() {}

                    override fun returnToHomeScreen() {}
                }
            ) {
                HomeDrawerContent(
                    uiModel = uiModel,
                    setListType = {},
                    closeDrawer = {},
                )
            }
        }
    }
}
