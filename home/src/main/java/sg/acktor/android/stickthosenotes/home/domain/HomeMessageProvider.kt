/*
 * Copyright (c) 2019-2024 Khairuddin Bin Ali
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package sg.acktor.android.stickthosenotes.home.domain

import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import it.czerwinski.android.hilt.annotations.BoundTo
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import sg.acktor.android.stickthosenotes.home.api.HomeMessage
import sg.acktor.android.stickthosenotes.home.api.HomeMessageAction
import sg.acktor.android.stickthosenotes.home.api.HomeMessageSender
import sg.acktor.android.stickthosenotes.notedetail.api.usecases.UpdateDeleteStateUseCase
import javax.inject.Inject

interface HomeMessageProvider : HomeMessageSender {
    val state: StateFlow<HomeMessage?>

    suspend fun performAction(action: HomeMessageAction)

    fun clearMessage()
}

@BoundTo(HomeMessageProvider::class, ActivityRetainedComponent::class)
@BoundTo(HomeMessageSender::class, ActivityRetainedComponent::class)
@ActivityRetainedScoped
class HomeMessageProviderImpl @Inject constructor(
    private val updateDeleteStateUseCase: UpdateDeleteStateUseCase,
) : HomeMessageProvider {
    private val _messageState = MutableStateFlow<HomeMessage?>(null)
    override val state = _messageState.asStateFlow()

    override fun sendMessage(message: HomeMessage) {
        _messageState.update { message }
    }

    override suspend fun performAction(action: HomeMessageAction) {
        when (action) {
            is HomeMessageAction.RestoreNote -> {
                updateDeleteStateUseCase.setDeleteState(action.noteId, false)
            }
        }
    }

    override fun clearMessage() {
        _messageState.update { null }
    }
}
