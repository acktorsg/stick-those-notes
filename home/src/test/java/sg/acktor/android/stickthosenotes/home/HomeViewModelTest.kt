package sg.acktor.android.stickthosenotes.home

import android.content.Context
import app.cash.turbine.test
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import sg.acktor.android.stickthosenotes.home.api.HomeMessage
import sg.acktor.android.stickthosenotes.home.api.HomeMessageAction
import sg.acktor.android.stickthosenotes.home.domain.HomeMessageProvider
import sg.acktor.android.stickthosenotes.home.domain.RecycleBinProvider
import sg.acktor.android.stickthosenotes.home.ui.HeaderUiModel
import sg.acktor.android.stickthosenotes.home.ui.HomeUiAction
import sg.acktor.android.stickthosenotes.home.ui.HomeUiEvent
import sg.acktor.android.stickthosenotes.home.ui.HomeUiModel
import sg.acktor.android.stickthosenotes.home.ui.NavDrawerUiModel
import sg.acktor.android.stickthosenotes.notelist.api.domain.NoteListTypeStateProvider
import sg.acktor.android.stickthosenotes.notelist.api.models.NoteListType
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs
import sg.acktor.android.stickthosenotes.common.R as R_common

private const val HOME_TITLE = "Stick Those Notes!"
private const val RECYCLE_BIN_TITLE = "Recycle Bin"

@OptIn(ExperimentalCoroutinesApi::class)
class HomeViewModelTest {
    companion object {
        @BeforeAll
        @JvmStatic
        fun setUpAll() {
            Dispatchers.setMain(StandardTestDispatcher())
        }

        @AfterAll
        @JvmStatic
        fun tearDownAll() {
            Dispatchers.resetMain()
        }
    }

    private val showBinState = MutableStateFlow(false)
    private val listTypeState = MutableStateFlow<NoteListType>(NoteListType.Home)
    private val homeMessageState = MutableStateFlow<HomeMessage?>(null)

    private val context = mockk<Context> {
        every { getString(R_common.string.app_name) } returns HOME_TITLE
        every { getString(R.string.title_screen_recycleBin) } returns RECYCLE_BIN_TITLE
    }
    private val recycleBinProvider = mockk<RecycleBinProvider>(relaxUnitFun = true) {
        every { recycleBinVisibilityState } returns showBinState
    }
    private val noteListTypeStateProvider = mockk<NoteListTypeStateProvider>(relaxUnitFun = true) {
        every { state } returns listTypeState
    }
    private val homeMessageProvider = mockk<HomeMessageProvider>(relaxUnitFun = true) {
        every { state } returns homeMessageState
    }

    private val sut = HomeViewModel(
        context, recycleBinProvider, noteListTypeStateProvider, homeMessageProvider
    )

    @AfterEach
    fun tearDown() {
        showBinState.update { false }
        listTypeState.update { NoteListType.Home }
        homeMessageState.update { null }
    }

    @Test
    fun `verify initial home state`() = runTest {
        val expected = HomeUiModel(
            headerUiModel = HeaderUiModel.Home(title = HOME_TITLE),
            navDrawerUiModel = NavDrawerUiModel(
                isHomeListSelected = true,
                isRecycleBinSelected = false,
            ),
            homeMessage = null,
        )

        val result = sut.homeUiState.first()
        assertEquals(expected, result)
    }

    @Test
    fun `verify list type update`() = runTest {
        val expected = HomeUiModel(
            headerUiModel = HeaderUiModel.RecycleBin(
                title = RECYCLE_BIN_TITLE,
                isEmptyBinEnabled = false,
                showClearBinDialog = false,
            ),
            navDrawerUiModel = NavDrawerUiModel(
                isHomeListSelected = false,
                isRecycleBinSelected = true,
            ),
            homeMessage = null,
        )

        sut.homeUiState.test {
            skipItems(1)
            listTypeState.update { NoteListType.RecycleBin }

            val result = awaitItem()
            assertEquals(expected, result)
        }
    }

    @Test
    fun `verify show bin update`() = runTest {
        val expected = true

        sut.homeUiState.test {
            skipItems(1)

            listTypeState.update { NoteListType.RecycleBin }
            skipItems(1)

            showBinState.update { true }

            val headerUiModel = awaitItem().headerUiModel
            assertIs<HeaderUiModel.RecycleBin>(headerUiModel)
            assertEquals(expected, headerUiModel.isEmptyBinEnabled)
        }
    }

    @Test
    fun `verify home message update`() = runTest {
        val expected = HomeMessage("some message")

        sut.homeUiState.test {
            skipItems(1)

            homeMessageState.update { expected }

            val uiState = awaitItem()
            assertEquals(expected, uiState.homeMessage)
        }
    }

    @Test
    fun `verify clear home message`() = runTest {
        val expected = HomeMessage("some message")

        sut.homeUiState.test {
            skipItems(1)

            homeMessageState.update { expected }
            skipItems(1)

            sut.processUiAction(HomeUiAction.SnackbarDismiss)

            verify {
                homeMessageProvider.clearMessage()
            }
        }
    }

    @Test
    fun `verify change list type`() = runTest {
        val listType = NoteListType.RecycleBin
        sut.processUiAction(HomeUiAction.ChangeListType(listType))

        verify {
            noteListTypeStateProvider.setState(listType)
        }
    }

    @Test
    fun `verify search button click`() = runTest {
        val expected = HomeUiEvent.OpenSearchScreen
        sut.processUiAction(HomeUiAction.SearchClicked)
        val result = sut.uiEventChannel.first()
        assertEquals(expected, result)

        verify {
            noteListTypeStateProvider.setState(NoteListType.Search(""))
        }
    }

    @Test
    fun `verify show clear bin dialog`() = runTest {
        val expected = true

        sut.homeUiState.test {
            skipItems(1)

            listTypeState.update { NoteListType.RecycleBin }
            skipItems(1)

            sut.processUiAction(HomeUiAction.ClearBinClicked)

            val headerUiModel = awaitItem().headerUiModel
            assertIs<HeaderUiModel.RecycleBin>(headerUiModel)
            assertEquals(expected, headerUiModel.showClearBinDialog)
        }
    }

    @Test
    fun `verify dismiss clear bin dialog`() = runTest {
        val expected = false

        sut.homeUiState.test {
            skipItems(1)

            listTypeState.update { NoteListType.RecycleBin }
            skipItems(1)

            sut.processUiAction(HomeUiAction.ClearBinClicked)
            skipItems(1)

            sut.processUiAction(HomeUiAction.ClearBinDialogDismiss)

            val headerUiModel = awaitItem().headerUiModel
            assertIs<HeaderUiModel.RecycleBin>(headerUiModel)
            assertEquals(expected, headerUiModel.showClearBinDialog)
        }
    }

    @Test
    fun `verify clear all notes`() = runTest {
        val expected = false

        sut.homeUiState.test {
            skipItems(1)

            listTypeState.update { NoteListType.RecycleBin }
            skipItems(1)

            sut.processUiAction(HomeUiAction.ClearBinClicked)
            skipItems(1)

            sut.processUiAction(HomeUiAction.ClearBinConfirm)

            val headerUiModel = awaitItem().headerUiModel
            assertIs<HeaderUiModel.RecycleBin>(headerUiModel)
            assertEquals(expected, headerUiModel.showClearBinDialog)
        }

        coVerify {
            recycleBinProvider.emptyBin()
        }
    }

    @Test
    fun `verify home message performed`() = runTest {
        val action = HomeMessageAction.RestoreNote(2L)
        sut.processUiAction(HomeUiAction.PerformHomeMessageAction(action))
        advanceUntilIdle()
        coVerify {
            homeMessageProvider.performAction(action)
        }
    }
}
