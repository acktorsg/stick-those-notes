package sg.acktor.android.stickthosenotes.home.domain

import app.cash.turbine.test
import io.mockk.Ordering
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.home.api.HomeMessage
import sg.acktor.android.stickthosenotes.home.api.HomeMessageAction
import sg.acktor.android.stickthosenotes.notedetail.api.usecases.UpdateDeleteStateUseCase
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

class HomeMessageProviderTest {
    private val updateDeleteStateUseCase = mockk<UpdateDeleteStateUseCase>(relaxUnitFun = true)

    private val sut: HomeMessageProvider = HomeMessageProviderImpl(updateDeleteStateUseCase)

    @Test
    fun `verify send message`() = runTest {
        val expected = HomeMessage("some home message")

        sut.state.test {
            skipItems(1)
            sut.sendMessage(expected)

            val result = awaitItem()
            assertEquals(expected, result)
        }
    }

    @Test
    fun `verify clear message`() = runTest {
        sut.state.test {
            sut.sendMessage(HomeMessage("some home message"))
            skipItems(2)

            sut.clearMessage()

            val result = awaitItem()
            assertNull(result)
        }
    }

    @Test
    fun `verify restore note action`() = runTest {
        val noteId = 3L

        sut.performAction(HomeMessageAction.RestoreNote(noteId))

        coVerify(Ordering.ORDERED) {
            updateDeleteStateUseCase.setDeleteState(noteId, false)
        }
    }
}
