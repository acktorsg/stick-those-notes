package sg.acktor.android.stickthosenotes.home.data

import io.mockk.Ordering
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runTest
import sg.acktor.android.stickthosenotes.home.data.room.RecycleBinDAO
import kotlin.test.Test
import kotlin.test.assertContentEquals

class RecycleBinRepositoryTest {
    private val dao = mockk<RecycleBinDAO>(relaxUnitFun = true)

    private val sut: RecycleBinRepository = RecycleBinRepositoryImpl(dao)

    @Test
    fun `verify clear deleted notes operation`() = runTest {
        sut.clearAllDeletedNotes()

        coVerify(Ordering.SEQUENCE) {
            dao.clearAllDeletedNotes()
        }
    }

    @Test
    fun `verify recycle bin state`() = runTest {
        val expected = listOf(true, false)

        every { dao.hasDeletedNotes() } returns flowOf(true, true, false)

        val result = sut.getRecycleBinState().toList()
        assertContentEquals(expected, result)

        verify(Ordering.ORDERED) {
            dao.hasDeletedNotes()
        }
    }
}
