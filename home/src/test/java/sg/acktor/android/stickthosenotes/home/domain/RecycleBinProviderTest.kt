package sg.acktor.android.stickthosenotes.home.domain

import io.mockk.Ordering
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Test
import sg.acktor.android.stickthosenotes.home.data.RecycleBinRepository
import kotlin.test.assertContentEquals

class RecycleBinProviderTest {
    private val repo = mockk<RecycleBinRepository>(relaxUnitFun = true)

    private val sut: RecycleBinProvider = RecycleBinProviderImpl(repo)

    @Test
    fun `verify empty bin operation`() = runTest {
        sut.emptyBin()

        coVerify(Ordering.SEQUENCE) {
            repo.clearAllDeletedNotes()
        }
    }

    @Test
    fun `verify visibility state source`() = runTest {
        val expected = listOf(true, false)

        every { repo.getRecycleBinState() } returns flowOf(*expected.toTypedArray())

        val result = sut.recycleBinVisibilityState.toList()
        assertContentEquals(expected, result)

        coVerify(Ordering.SEQUENCE) {
            repo.getRecycleBinState()
        }
    }
}
